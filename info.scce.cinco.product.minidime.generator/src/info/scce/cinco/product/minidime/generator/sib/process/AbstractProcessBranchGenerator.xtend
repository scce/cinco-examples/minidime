package info.scce.cinco.product.minidime.generator.sib.process

import info.scce.cinco.product.minidime.generator.MiniDIMEXtendTemplate

import static info.scce.cinco.product.minidime.generator.helper.NameExtension.*

/**
 * @author Fabian Storek
 */
class AbstractProcessBranchGenerator extends MiniDIMEXtendTemplate {
	
	override packageName() {
		processBranchPackage
	}
	
	override className() {
		processBranchClass
	}
	
	override classTemplate() '''		
		abstract class «processBranchClass» extends «branchFqn.shorten» {
			// Intentionally left blank
		}
	'''
	
	
}

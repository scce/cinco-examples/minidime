package info.scce.cinco.product.minidime.generator.common

import info.scce.cinco.product.minidime.generator.MiniDIMEXtendTemplate

import static extension info.scce.cinco.product.minidime.generator.helper.NameExtension.*

/**
 * @author Fabian Storek
 */
class DoubleExtensionGenerator extends MiniDIMEXtendTemplate {
	
	override packageName() {
		commonExtensionPackage
	}
	
	override className() {
		'Double'.commonExtensionClass
	}
	
	override classTemplate() '''
		class «className» {
			
			def static void print(Double d) {
				println(d)
			}
			
			def static Long toLong(Double d) {
				if (d === null) return null
				d.longValue
			}
			
			def static Boolean toBoolean(Double d) {
				if (d === null) return null
				d != 0
			}
			
			def static String toString(Double d) {
				d?.toString
			}
			
			def static Boolean equal(Double d1, Double d2) {
				d1 == d2
			}
			
			def static Boolean notEqual(Double d1, Double d2) {
				d1 != d2
			}
			
			def static Boolean less(Double d1, Double d2) {
				d1 < d2
			}
			
			def static Boolean lessOrEqual(Double d1, Double d2) {
				d1 <= d2
			}
			
			def static Boolean greater(Double d1, Double d2) {
				d1 > d2
			}
			
			def static Boolean greaterOrEqual(Double d1, Double d2) {
				d1 >= d2
			}
			
			def static Double add(Double d1, Double d2) {
				d1 + d2
			}
			
			def static Double subtract(Double d1, Double d2) {
				d1 - d2
			}
			
			def static Double multiply(Double d1, Double d2) {
				d1 * d2
			}
			
			def static Double divide(Double d1, Double d2) {
				d1 / d2
			}
			
		}
	'''
	
}

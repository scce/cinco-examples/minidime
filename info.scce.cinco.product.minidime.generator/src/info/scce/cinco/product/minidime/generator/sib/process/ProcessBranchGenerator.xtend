package info.scce.cinco.product.minidime.generator.sib.process

import info.scce.cinco.product.minidime.generator.MiniDIMEXtendTemplate

import static extension info.scce.cinco.product.minidime.generator.helper.NameExtension.*
import info.scce.cinco.product.minidime.process.MiniDIMEProcess

/**
 * @author Fabian Storek
 */
class ProcessBranchGenerator extends MiniDIMEXtendTemplate {
	
	val MiniDIMEProcess sib
	
	new(MiniDIMEProcess sib) {
		this.sib = sib
	}
	
	override packageName() {
		sib.processBranchPackage
	}
	
	override className() {
		sib.processBranchClass
	}
	
	override classTemplate() '''		
		abstract class «sib.processBranchClass» extends «processBranchFqn.shorten» {
			// Intentionally left blank
		}
		«FOR endSib : sib.endSIBs»
			
			«dataAnnotation»
			class «endSib.processBranchClass» extends «sib.processBranchFqn.shorten» {
				«IF endSib.inputs.nullOrEmpty»
					// Intentionally left blank
				«ELSE»
					
					// Variables
					«FOR port : endSib.inputs»
						val «port.variableFqn.shorten» «port.variableName»
					«ENDFOR»
					
				«ENDIF»
			}
		«ENDFOR»
	'''
	
}

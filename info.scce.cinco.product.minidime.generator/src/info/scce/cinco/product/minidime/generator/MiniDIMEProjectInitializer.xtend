package info.scce.cinco.product.minidime.generator

import de.jabc.cinco.meta.plugin.template.ProjectTemplate
import info.scce.cinco.product.minidime.generator.common.BooleanExtensionGenerator
import info.scce.cinco.product.minidime.generator.common.BooleanSibsGenerator
import info.scce.cinco.product.minidime.generator.common.DoubleExtensionGenerator
import info.scce.cinco.product.minidime.generator.common.IntegerSibsGenerator
import info.scce.cinco.product.minidime.generator.common.ListExtensionGenerator
import info.scce.cinco.product.minidime.generator.common.LongExtensionGenerator
import info.scce.cinco.product.minidime.generator.common.RealSibsGenerator
import info.scce.cinco.product.minidime.generator.common.StringExtensionGenerator
import info.scce.cinco.product.minidime.generator.common.TextSibsGenerator
import info.scce.cinco.product.minidime.generator.tuple.TupleGenerator

import static info.scce.cinco.product.minidime.generator.helper.NameExtension.*

import static extension de.jabc.cinco.meta.core.utils.projects.ProjectCreator.getProject
import info.scce.cinco.product.minidime.process.MiniDIMEProcess

/**
 * @author Fabian Storek
 */
class MiniDIMEProjectInitializer extends ProjectTemplate {
	
	val String projectName
	val MiniDIMEProcess process
		
	new(String projectName) {
		this.projectName = projectName
		this.process = null
	}
	
	new(MiniDIMEProcess process) {
		this.process = process
		this.projectName = null
	}
	
	override projectDescription() {
		
		project(projectName ?: process.eResource.project.name) [
			
			deleteIfExistent = false
			
			natures = #['org.eclipse.xtext.ui.shared.xtextNature']
			
			requiredBundles = #[
				'com.google.guava',
				'org.eclipse.xtext.xbase.lib',
				'org.eclipse.xtend.lib',
				'org.eclipse.xtend.lib.macro'
			]
			
			folder(commonSibPackage) [
				deleteIfExistent = true
				isSourceFolder = false
				file(new IntegerSibsGenerator)
				file(new RealSibsGenerator)
				file(new BooleanSibsGenerator)
				file(new TextSibsGenerator)
			]
			
			folder('src') [
				deleteIfExistent = false
				isSourceFolder = true
			]
			
			folder('src-gen') [
				deleteIfExistent = true
				isSourceFolder = true
				pkg(tuplePackage) [
					file(new TupleGenerator(10), true)
				]
				pkg(commonExtensionPackage) [
					file(new LongExtensionGenerator)
					file(new DoubleExtensionGenerator)
					file(new BooleanExtensionGenerator)
					file(new StringExtensionGenerator)
					file(new ListExtensionGenerator)
				]
			]
			
			folder('models') [
				deleteIfExistent = false
				isSourceFolder = false
			]
			
		]
		
	}
	
	override projectSuffix() {
		throw new UnsupportedOperationException("TODO: auto-generated method stub")
	}
	
}

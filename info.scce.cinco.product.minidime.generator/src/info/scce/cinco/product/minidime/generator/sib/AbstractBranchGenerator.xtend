package info.scce.cinco.product.minidime.generator.sib

import info.scce.cinco.product.minidime.generator.MiniDIMEXtendTemplate

import static info.scce.cinco.product.minidime.generator.helper.NameExtension.*

/**
 * @author Fabian Storek
 */
class AbstractBranchGenerator extends MiniDIMEXtendTemplate {
	
	override packageName() {
		branchPackage
	}
	
	override className() {
		branchClass
	}
	
	override classTemplate() '''		
		abstract class «branchClass» extends «elementFqn.shorten» {
			// Intentionally left blank
		}
	'''
	
}

package info.scce.cinco.product.minidime.generator

import org.eclipse.core.resources.ResourcesPlugin
import org.eclipse.jface.viewers.IStructuredSelection
import org.eclipse.jface.wizard.Wizard
import org.eclipse.jface.wizard.WizardPage
import org.eclipse.swt.events.KeyEvent
import org.eclipse.swt.events.KeyListener
import org.eclipse.swt.layout.GridData
import org.eclipse.swt.layout.GridLayout
import org.eclipse.swt.widgets.Composite
import org.eclipse.swt.widgets.Label
import org.eclipse.swt.widgets.Text
import org.eclipse.ui.IWorkbench
import org.eclipse.ui.IWorkbenchWizard

import static org.eclipse.swt.SWT.*
import org.eclipse.ui.PlatformUI
import org.eclipse.ui.WorkbenchException

/**
 * @author Fabian Storek
 */
class MiniDIMEProjectWizard extends Wizard implements IWorkbenchWizard {
	
	val static TITLE = 'New Mini DIME Project'
	
	var MiniDIMEProjectWizardPage wizardPage
	
	override init(IWorkbench workbench, IStructuredSelection selection) {
		windowTitle = TITLE
		wizardPage = new MiniDIMEProjectWizardPage
	}
	
	override addPages() {
		wizardPage.addPage
		super.addPages
	}
	
	override canFinish() {
		wizardPage.pageComplete
	}

	override performFinish() {
		new MiniDIMEProjectInitializer(projectName).createProject
		val workbench = PlatformUI.workbench
		try workbench.showPerspective('info.scce.cinco.product.minidime.perspectives.MiniDIMEPerspective', workbench.activeWorkbenchWindow)
		catch (WorkbenchException e) e.printStackTrace
		true
	}
	
	def getProjectName() {
		wizardPage?.projectName
	}

	static class MiniDIMEProjectWizardPage extends WizardPage {
		
		var Text projectNameText
		
		new() {
			super(TITLE)
			title = TITLE
			description = 'Initialize an empty Mini DIME project and switch to the correct perspective.'
			pageComplete = false
		}
		
		override createControl(Composite parent) {
			
			val composite = new Composite(parent, NONE)
			composite.layout = new GridLayout(2, false)
			control = composite
			
			val projectNameLabel = new Label(composite, NONE)
			projectNameLabel.layoutData = new GridData(LEFT, CENTER, false, false, 1, 1)
			projectNameLabel.text = '&Project name:'
			
			projectNameText = new Text(composite, BORDER)
			projectNameText.layoutData = new GridData(FILL, CENTER, true, false, 1, 1)
			projectNameText.text = 'MyMiniDIMEProject'
			
			projectNameText.addKeyListener(new KeyListener {
				override keyReleased(KeyEvent e) {
					validate
				}
				override keyPressed(KeyEvent e) {
					// Intentionally left blank
				}
			})
	
			validate
			
		}
		
		def getProjectName() {
			projectNameText?.text
		}
		
		def validate() {
			if (projectName.nullOrEmpty) {
				errorMessage = 'The project name must not be empty.'
				pageComplete = false
			}
			else if (!projectName.matches('''^[a-zA-Z].*$''')) {
				errorMessage = 'The project name must start with a letter.'
				pageComplete = false
			}
			else if (!projectName.matches('''^[\w\.-]+$''')) {
				errorMessage = 'The project name may only contain letters, digits, periods (.), hyphens (-) and underscores (_).'
				pageComplete = false
			}
			else if (ResourcesPlugin.workspace.root.projects.exists[name == projectName]) {
				errorMessage = '''The project "«projectName»" already exists.'''
				pageComplete = false
			}
			else {
				errorMessage = null
				pageComplete = true
			}
		}
		
	}
	
}

package info.scce.cinco.product.minidime.generator.sib

import info.scce.cinco.product.minidime.generator.MiniDIMEXtendTemplate

import static info.scce.cinco.product.minidime.generator.helper.NameExtension.*

/**
 * @author Fabian Storek
 */
class AbstractSibGenerator extends MiniDIMEXtendTemplate {
	
	override packageName() {
		sibPackage
	}
	
	override className() {
		sibClass
	}
	
	override classTemplate() '''		
		abstract class «sibClass» extends «elementFqn.shorten» {
			// Intentionally left blank
		}
	'''
	
}

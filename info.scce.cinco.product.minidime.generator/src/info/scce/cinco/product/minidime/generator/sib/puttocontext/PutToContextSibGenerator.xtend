package info.scce.cinco.product.minidime.generator.sib.puttocontext

import info.scce.cinco.product.minidime.generator.MiniDIMEXtendTemplate
import info.scce.cinco.product.minidime.process.PutToContextSIB

import static extension info.scce.cinco.product.minidime.generator.helper.NameExtension.*

/**
 * @author Fabian Storek
 */
class PutToContextSibGenerator extends MiniDIMEXtendTemplate {
	
	val PutToContextSIB sib
	
	new(PutToContextSIB sib) {
		this.sib = sib
	}
	
	override packageName() {
		sib.putToContextSibPackage
	}
	
	override className() {
		sib.putToContextSibClass
	}
	
	override classTemplate() '''		
		class «sib.putToContextSibClass» extends «putToContextSibFqn.shorten» {
			
			// Execute method
			def «sib.putToContextBranchFqn.shorten» execute(«FOR port : sib.inputs SEPARATOR ', '»«port.variableFqn.shorten» «port.variableName»«ENDFOR») {
				return new «sib.successPutToContextBranch.putToContextBranchFqn.shorten»(«FOR input : sib.inputs SEPARATOR ', '»«input.variableNameNullSafeList»«ENDFOR»)
			}
			
		}
	'''
	
}

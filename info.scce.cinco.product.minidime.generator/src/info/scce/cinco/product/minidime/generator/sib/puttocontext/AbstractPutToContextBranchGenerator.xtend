package info.scce.cinco.product.minidime.generator.sib.puttocontext

import info.scce.cinco.product.minidime.generator.MiniDIMEXtendTemplate

import static info.scce.cinco.product.minidime.generator.helper.NameExtension.*

/**
 * @author Fabian Storek
 */
class AbstractPutToContextBranchGenerator extends MiniDIMEXtendTemplate {
	
	override packageName() {
		putToContextBranchPackage
	}
	
	override className() {
		putToContextBranchClass
	}
	
	override classTemplate() '''		
		abstract class «putToContextBranchClass» extends «branchFqn.shorten» {
			// Intentionally left blank
		}
	'''
	
}

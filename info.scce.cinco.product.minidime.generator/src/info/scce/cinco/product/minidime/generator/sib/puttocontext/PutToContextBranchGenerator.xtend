package info.scce.cinco.product.minidime.generator.sib.puttocontext

import info.scce.cinco.product.minidime.generator.MiniDIMEXtendTemplate
import info.scce.cinco.product.minidime.process.PutToContextSIB

import static extension info.scce.cinco.product.minidime.generator.helper.NameExtension.*

/**
 * @author Fabian Storek
 */
class PutToContextBranchGenerator extends MiniDIMEXtendTemplate {
	
	val PutToContextSIB sib
	
	new(PutToContextSIB sib) {
		this.sib = sib
	}
	
	override packageName() {
		sib.putToContextBranchPackage
	}
	
	override className() {
		sib.putToContextBranchClass
	}
	
	override classTemplate() '''		
		abstract class «sib.putToContextBranchClass» extends «putToContextBranchFqn.shorten» {
			// Intentionally left blank
		}
		«FOR branch : sib.branchSuccessors»
			
			«dataAnnotation»
			class «branch.putToContextBranchClass» extends «sib.putToContextBranchFqn.shorten» {
				«IF branch.outputPorts.nullOrEmpty»
					// Intentionally left blank
				«ELSE»
					
					// Variables
					«FOR port : branch.outputPorts»
						val «port.variableFqn.shorten» «port.variableName»
					«ENDFOR»
					
				«ENDIF»
			}
		«ENDFOR»
	'''
	
}

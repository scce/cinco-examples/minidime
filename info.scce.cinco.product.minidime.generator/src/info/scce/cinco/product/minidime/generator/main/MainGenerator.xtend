package info.scce.cinco.product.minidime.generator.main

import info.scce.cinco.product.minidime.generator.MiniDIMEXtendTemplate

import static extension info.scce.cinco.product.minidime.generator.helper.NameExtension.*
import info.scce.cinco.product.minidime.process.MiniDIMEProcess

/**
 * @author Fabian Storek
 */
class MainGenerator extends MiniDIMEXtendTemplate {
	
	val MiniDIMEProcess process
	
	new(MiniDIMEProcess process) {
		this.process = process
	}
	
	override packageName() {
		mainPackage
	}
	
	override className() {
		mainClass
	}
	
	override classTemplate() '''
		/**
		 * If you switched your root process or made changes to its input / output ports
		 * and wish to re-generate this class, rename or delete this class and generate again.
		 */
		class «mainClass» {
			
			def static void main(String[] args) {
				
				val rootProcess = new «process.processSibFqn.shorten»
				
				switch «IF process.endSIBs.exists[!inputs.nullOrEmpty]»result : «ENDIF»rootProcess.execute«FOR port : process.startSIBs.head.outputPorts BEFORE '(' SEPARATOR ', ' AFTER ')'»null«ENDFOR» {
					
					«FOR branch : process.endSIBs»
						«branch.processBranchFqn.shorten»: println(«"'''"»
							================================
							Result: «branch.processBranchName»«FOR port : branch.inputs BEFORE '(' SEPARATOR ', ' AFTER ')'»«port.name»«ENDFOR»
							«IF !branch.inputs.nullOrEmpty»
								--------------------------------
								«FOR port : branch.inputs»
									«port.name»: «'«'»result.«port.variableName»«'»'»
								«ENDFOR»
							«ENDIF»
							================================
						«"'''"»)
						
					«ENDFOR»
				}
			
			}
			
		}
	'''
	
}

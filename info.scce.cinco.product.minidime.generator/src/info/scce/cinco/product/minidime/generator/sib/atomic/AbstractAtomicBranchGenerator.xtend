package info.scce.cinco.product.minidime.generator.sib.atomic

import info.scce.cinco.product.minidime.generator.MiniDIMEXtendTemplate

import static info.scce.cinco.product.minidime.generator.helper.NameExtension.*

/**
 * @author Fabian Storek
 */
class AbstractAtomicBranchGenerator extends MiniDIMEXtendTemplate {
	
	override packageName() {
		atomicBranchPackage
	}
	
	override className() {
		atomicBranchClass
	}
	
	override classTemplate() '''		
		abstract class «atomicBranchClass» extends «branchFqn.shorten» {
			// Intentionally left blank
		}
	'''
	
}

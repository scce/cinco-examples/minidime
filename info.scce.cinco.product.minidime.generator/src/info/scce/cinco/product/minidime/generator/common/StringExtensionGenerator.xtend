package info.scce.cinco.product.minidime.generator.common

import info.scce.cinco.product.minidime.generator.MiniDIMEXtendTemplate

import static extension info.scce.cinco.product.minidime.generator.helper.NameExtension.*

/**
 * @author Fabian Storek
 */
class StringExtensionGenerator extends MiniDIMEXtendTemplate {
	
	override packageName() {
		commonExtensionPackage
	}
	
	override className() {
		'String'.commonExtensionClass
	}
	
	override classTemplate() '''
		import java.util.List
		
		import static extension java.lang.Double.parseDouble
		import static extension java.lang.Long.parseLong
		
		class «className» {
			
			def static void print(String s) {
				println(s)
			}
			
			def static Long toLong(String s) {
				if (s === null) return null
				s.parseLong
			}
			
			def static Double toDouble(String s) {
				if (s === null) return null
				s.parseDouble
			}
			
			def static Boolean toBoolean(String s) {
				if (s === null) return null
				val normalizedString = s.trim.toLowerCase
				if (#['true', 'yes'].contains(normalizedString)) {
					return true
				}
				if (#['false', 'no'].contains(normalizedString)) {
					return false
				}
				try {
					return normalizedString.parseDouble != 0D
				}
				catch (NumberFormatException e) { }
				try {
					return normalizedString.parseLong != 0L
				}
				catch (NumberFormatException e) { }
				throw new BooleanFormatException
			}
			
			def static Boolean equal(String s1, String s2) {
				s1 == s2
			}
			
			def static Boolean notEqual(String s1, String s2) {
				s1 != s2
			}
			
			def static String append(String s1, String s2) {
				s1 + s2
			}
			
			def static List<String> listOfCharacters(String s) {
				s.toCharArray.map[toString]
			}
			
			def static List<String> listOfWords(String s) {
				s.split('\\W').filter[!nullOrEmpty].toList // \W = Non-word characters
			}
			
		}
		
		class BooleanFormatException extends IllegalArgumentException { }
	'''
	
}

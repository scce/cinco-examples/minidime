package info.scce.cinco.product.minidime.generator.helper

import info.scce.cinco.product.minidime.process.AbstractContainsSIB
import info.scce.cinco.product.minidime.process.AbstractIterateSIB
import info.scce.cinco.product.minidime.process.AtomicSIB
import info.scce.cinco.product.minidime.process.BooleanInputStatic
import info.scce.cinco.product.minidime.process.Branch
import info.scce.cinco.product.minidime.process.EndSIB
import info.scce.cinco.product.minidime.process.Input
import info.scce.cinco.product.minidime.process.InputStatic
import info.scce.cinco.product.minidime.process.IntegerInputStatic
import info.scce.cinco.product.minidime.process.JavaNativeInputPort
import info.scce.cinco.product.minidime.process.JavaNativeOutputPort
import info.scce.cinco.product.minidime.process.JavaNativeVariable
import info.scce.cinco.product.minidime.process.Output
import info.scce.cinco.product.minidime.process.PrimitiveInputPort
import info.scce.cinco.product.minidime.process.PrimitiveOutputPort
import info.scce.cinco.product.minidime.process.PrimitiveVariable
import info.scce.cinco.product.minidime.process.ProcessSIB
import info.scce.cinco.product.minidime.process.PutToContextSIB
import info.scce.cinco.product.minidime.process.RealInputStatic
import info.scce.cinco.product.minidime.process.TextInputStatic
import info.scce.cinco.product.minidime.process.Variable
import info.scce.cinco.product.minidime.process.helper.ProcessExtension.SwitchException
import info.scce.cinco.product.minidime.siblibrary.JavaType
import info.scce.cinco.product.minidime.siblibrary.JavaTypePort
import info.scce.cinco.product.minidime.siblibrary.Port
import info.scce.cinco.product.minidime.siblibrary.PrimitivePort
import info.scce.cinco.product.minidime.siblibrary.PrimitiveType
import info.scce.cinco.product.minidime.siblibrary.SIB
import info.scce.cinco.product.minidime.siblibrary.SIBLibrary
import java.util.List
import java.util.Map

import static info.scce.cinco.product.minidime.process.PrimitiveType.*
import static info.scce.cinco.product.minidime.siblibrary.PrimitiveType.*

import static extension info.scce.cinco.product.minidime.generator.helper.GeneratorExtension.*
import static extension org.eclipse.xtext.EcoreUtil2.getContainerOfType
import info.scce.cinco.product.minidime.process.MiniDIMEProcess

/**
 * @author Fabian Storek
 */
class NameExtension {
	
	/*** Literals ***/
	
	val static main			= 'Main'
	val static tuple		= 'Tuple'
	val static common       = 'Common'
	val static element		= 'Element'
	val static sib			= 'Sib'
	val static branch		= 'Branch'
	val static process		= 'Process'
	val static atomic		= 'Atomic'
	val static putToContext	= 'PutToContext'
	val static contains     = 'Contains'
	val static iterate      = 'Iterate'
	
	val static text         = 'String'
	val static textFqn      = 'java.lang.String'
	val static integer      = 'Long'
	val static integerFqn   = 'java.lang.Long'
	val static real         = 'Double'
	val static realFqn      = 'java.lang.Double'
	val static bool         = 'Boolean'
	val static boolFqn      = 'java.lang.Boolean'
	val static list         = 'List'
	val static listFqn      = 'java.util.List'
	
	
	
	/*** Unnamed Sibs ***/
	
	var static Map<Object, Integer> indices = newHashMap
	
	def private static int getIndex(Object o) {
		if (!indices.containsKey(o)) indices.put(o, indices.size)
		indices.get(o)
	}
	
	def private static String getNonEmptyName(info.scce.cinco.product.minidime.process.SIB it) {
		if (name.nullOrEmpty) 'Unnamed' + index else name
	}
	
	def static void reset() {
		indices = newHashMap
	}
	
	
	/*** Main classes ***/
	
	// Main
	def static String getMainPackage()		{ main.toLowerCase }
	def static String getMainName()			{ main }
	def static String getMainClass()		{ mainName.escapeClassName }
	def static String getMainFqn()			{ mainPackage + '.' + mainClass }
		
	
	
	/*** Tuple classes ****/
	
	// Tuple
	def static String getTuplePackage()			{ tuple.toLowerCase }
	def static String getTupleName()			{ tuple }
	def static String getTupleClass()			{ tupleName.escapeClassName }
	def static String getTupleFqn()				{ tuplePackage + '.' + tupleClass }
	
	def static String getTuplePackage(int i)	{ tuplePackage }
	def static String getTupleName(int i)		{ if (i > 0) tupleName + i else tupleName }
	def static String getTupleClass(int i)		{ i.tupleName.escapeClassName }
	def static String getTupleFqn(int i)		{ i.tuplePackage + '.' + i.tupleClass }
	
	
	
	/*** Common Sibs classes ****/
	
	// Common Sibs
	def static String getCommonSibPackage()						{ common.toFirstLower }
//	def static String getCommonSibName()						{ '' }
//	def static String getCommonSibClass()						{ '' }
//	def static String getCommonSibFqn()							{ '' }
	
	def static String getCommonSibPackage(String type)			{ commonSibPackage + '.' + type.commonSibName.toFirstLower }
	def static String getCommonSibName(String type)				{ type + sib }
//	def static String getCommonSibClass(String type)			{ '' }
//	def static String getCommonSibFqn(String type)				{ '' }
	
	// Common extensions
	def static String getCommonExtensionPackage()				{ common.toFirstLower }
	def static String getCommonExtensionName()					{ 'Extension' }
//	def static String getCommonExtensionClass()					{ '' }
//	def static String getCommonExtensionFqn()					{ '' }
	
	def static String getCommonExtensionPackage(String type)	{ commonExtensionPackage }
	def static String getCommonExtensionName(String type)		{ type + commonExtensionName }
	def static String getCommonExtensionClass(String type)		{ type.commonExtensionName.escapeClassName }
	def static String getCommonExtensionFqn(String type)		{ type.commonExtensionPackage + '.' + type.commonExtensionClass }
	
	
	
	/*** Abstract classes ***/
	
	// Abstract Element
	def static String getElementPackage()	{ sibPackage }
	def static String getElementName()		{ element }
	def static String getElementClass()		{ elementName.escapeClassName }
	def static String getElementFqn()		{ elementPackage + '.' + elementClass }
	
	// Abstract Sib
	def static String getSibPackage()		{ sib.toLowerCase }
	def static String getSibName()			{ sib }
	def static String getSibClass()			{ sibName.escapeClassName }
	def static String getSibFqn()			{ sibPackage + '.' + sibClass }
	
	// Abstract Branch
	def static String getBranchPackage()	{ sibPackage }
	def static String getBranchName()		{ branch }
	def static String getBranchClass()		{ branchName.escapeClassName }
	def static String getBranchFqn()		{ branchPackage + '.' + branchClass }
	
	// Abstract AtomicSib
	def static String getAtomicSibPackage()	{ sibPackage + '.' + atomic.toLowerCase }
	def static String getAtomicSibName()	{ atomic + sibName }
	def static String getAtomicSibClass()	{ atomicSibName.escapeClassName }
	def static String getAtomicSibFqn()		{ atomicSibPackage + '.' + atomicSibClass }
	
	// Abstract AtomicBranch
	def static String getAtomicBranchPackage()	{ atomicSibPackage }
	def static String getAtomicBranchName()		{ atomic + branchName }
	def static String getAtomicBranchClass()	{ atomicBranchName.escapeClassName }
	def static String getAtomicBranchFqn()		{ atomicBranchPackage + '.' + atomicBranchClass }
	
	// Abstract ProcessSib
	def static String getProcessSibPackage()	{ sibPackage + '.' + process.toLowerCase }
	def static String getProcessSibName()		{ process + sibName }
	def static String getProcessSibClass()		{ processSibName.escapeClassName }
	def static String getProcessSibFqn()		{ processSibPackage + '.' + processSibClass }
	
	// Abstract ProcessBranch
	def static String getProcessBranchPackage()	{ processSibPackage }
	def static String getProcessBranchName()	{ process + branchName }
	def static String getProcessBranchClass()	{ processBranchName.escapeClassName }
	def static String getProcessBranchFqn()		{ processBranchPackage + '.' + processBranchClass }
	
	// Abstract PutToContextSib
	def static String getPutToContextSibPackage()	{ sibPackage + '.' + putToContext.toLowerCase }
	def static String getPutToContextSibName()		{ putToContext + sibName }
	def static String getPutToContextSibClass()		{ putToContextSibName.escapeClassName }
	def static String getPutToContextSibFqn()		{ putToContextSibPackage + '.' + putToContextSibClass }
	
	// Abstract PutToContextBranch
	def static String getPutToContextBranchPackage()	{ putToContextSibPackage }
	def static String getPutToContextBranchName()		{ putToContext + branchName }
	def static String getPutToContextBranchClass()		{ putToContextBranchName.escapeClassName }
	def static String getPutToContextBranchFqn()		{ putToContextBranchPackage + '.' + putToContextBranchClass }
	
	// Abstract ContainsSib
	def static String getContainsSibPackage()			{ sibPackage + '.' + contains.toLowerCase }
	def static String getContainsSibName()				{ contains + sibName }
	def static String getContainsSibClass()				{ containsSibName.escapeClassName }
	def static String getContainsSibFqn()				{ containsSibPackage + '.' + containsSibClass }
	
	// Abstract ContainsBranch
	def static String getContainsBranchPackage()		{ containsSibPackage }
	def static String getContainsBranchName()			{ contains + branchName }
	def static String getContainsBranchClass()			{ containsBranchName.escapeClassName }
	def static String getContainsBranchFqn()			{ containsBranchPackage + '.' + containsBranchClass }
	
	// Abstract IterateSib
	def static String getIterateSibPackage()			{ sibPackage + '.' + iterate.toLowerCase }
	def static String getIterateSibName()				{ iterate + sibName }
	def static String getIterateSibClass()				{ iterateSibName.escapeClassName }
	def static String getIterateSibFqn()				{ iterateSibPackage + '.' + iterateSibClass }
	
	// Abstract IterateBranch
	def static String getIterateBranchPackage()			{ iterateSibPackage }
	def static String getIterateBranchName()			{ iterate + branchName }
	def static String getIterateBranchClass()			{ iterateBranchName.escapeClassName }
	def static String getIterateBranchFqn()				{ iterateBranchPackage + '.' + iterateBranchClass }
	
	
	
	/*** Concrete classes: Atomic ***/
	
	// Concrete AtomicSib
	def static String getAtomicSibPackage(SIB it)		{ atomicSibPackage }
	def static String getAtomicSibName(SIB it)			{ getContainerOfType(SIBLibrary).package.name.split('''\.''').map[toFirstUpper].join + it.name }
	def static String getAtomicSibClass(SIB it)			{ it.atomicSibName.escapeClassName + atomicSibClass }
	def static String getAtomicSibFqn(SIB it)			{ it.atomicSibPackage + '.' + it.atomicSibClass }
	
	// Abstract AtomicBranch of concrete AtomicSib
	def static String getAtomicBranchPackage(SIB it)	{ it.atomicSibPackage }
	def static String getAtomicBranchName(SIB it)		{ it.atomicSibName }
	def static String getAtomicBranchClass(SIB it)		{ it.atomicBranchName.escapeClassName + atomicBranchClass }
	def static String getAtomicBranchFqn(SIB it)		{ it.atomicBranchPackage + '.' + it.atomicBranchClass }
	
	// Concrete AtomicBranch of concrete AtomicSib
	def static String getAtomicBranchPackage(info.scce.cinco.product.minidime.siblibrary.Branch it)	{ it.atomicSib.atomicBranchPackage }
	def static String getAtomicBranchName(info.scce.cinco.product.minidime.siblibrary.Branch it)	{ it.name }
	def static String getAtomicBranchClass(info.scce.cinco.product.minidime.siblibrary.Branch it)	{ it.atomicBranchName.escapeClassName + it.atomicSib.atomicBranchClass }
	def static String getAtomicBranchFqn(info.scce.cinco.product.minidime.siblibrary.Branch it)		{ it.atomicBranchPackage + '.' + it.atomicBranchClass }
	
	// Utils
	def static SIB getAtomicSib(AtomicSIB sib) {
		return sib.sib as SIB
	}
	
	def static SIB getAtomicSib(info.scce.cinco.product.minidime.siblibrary.Branch branch) {
		return branch.eContainer as SIB
	}
	
	def static SIB getAtomicSib(Branch branch) {
		val atomicSib = branch.incomingBranchConnectors.head.sourceElement as AtomicSIB
		return atomicSib.sib as SIB
	}
	
	def static info.scce.cinco.product.minidime.siblibrary.Branch getAtomicBranch(Branch branch) {
		val sib = branch.atomicSib
		return sib.branches.findFirst[name == branch.name]
	}
	
	def static info.scce.cinco.product.minidime.siblibrary.Branch getAtomicBranch(SIB sib, String branchName) {
		return sib.branches.findFirst[name.toLowerCase == branchName.toLowerCase]
	}
	
	def static info.scce.cinco.product.minidime.siblibrary.Branch getSuccessAtomicBranch(SIB sib) {
		return sib.getAtomicBranch('success')
	}
	
	def static info.scce.cinco.product.minidime.siblibrary.Branch getNoresultAtomicBranch(SIB sib) {
		return sib.getAtomicBranch('noresult')
	}
	
	def static info.scce.cinco.product.minidime.siblibrary.Branch getFailureAtomicBranch(SIB sib) {
		return sib.getAtomicBranch('failure')
	}
	
	def static info.scce.cinco.product.minidime.siblibrary.Branch getTrueAtomicBranch(SIB sib) {
		return sib.getAtomicBranch('true')
	}
	
	def static info.scce.cinco.product.minidime.siblibrary.Branch getFalseAtomicBranch(SIB sib) {
		return sib.getAtomicBranch('false')
	}
	
	
	
	
	/*** Concrete classes: Process ***/
	
	// Concrete ProcessSib
	def static String getProcessSibPackage(MiniDIMEProcess it)		{ processSibPackage }
	def static String getProcessSibName(MiniDIMEProcess it)			{ it.getModelName }
	def static String getProcessSibClass(MiniDIMEProcess it)		{ it.processSibName.escapeClassName + processSibClass }
	def static String getProcessSibFqn(MiniDIMEProcess it)			{ it.processSibPackage + '.' + it.processSibClass }
	
	// Abstract ProcessBranch of concrete ProcessSib
	def static String getProcessBranchPackage(MiniDIMEProcess it)	{ it.processSibPackage }
	def static String getProcessBranchName(MiniDIMEProcess it)		{ it.processSibName }
	def static String getProcessBranchClass(MiniDIMEProcess it)		{ it.processBranchName.escapeClassName + processBranchClass }
	def static String getProcessBranchFqn(MiniDIMEProcess it)		{ it.processBranchPackage + '.' + it.processBranchClass }
	
	// Concrete ProcessBranch of concrete ProcessSib
	def static String getProcessBranchPackage(EndSIB it)	{ it.processSib.processBranchPackage }
	def static String getProcessBranchName(EndSIB it)		{ it.branchName }
	def static String getProcessBranchClass(EndSIB it)		{ it.processBranchName.escapeClassName + it.processSib.processBranchClass }
	def static String getProcessBranchFqn(EndSIB it)		{ it.processBranchPackage + '.' + it.processBranchClass }
	
	// Utils
	def static MiniDIMEProcess getProcessSib(ProcessSIB sib) {
		return sib.proMod
	}
	
	def static EndSIB getProcessBranch(Branch branch) {
		val sib = branch.processSib
		return sib.endSIBs.findFirst[branchName == branch.name]
	}
	
	def static MiniDIMEProcess getProcessSib(EndSIB branch) {
		return branch.rootElement
	}
	
	def static MiniDIMEProcess getProcessSib(Branch branch) {
		val processSib = branch.incomingBranchConnectors.head.sourceElement as ProcessSIB
		return processSib.proMod
	}
	
	
	
	/*** Concrete classes: PutToContext ***/
	
	// Concrete PutToContextSib
	def static String getPutToContextSibPackage(PutToContextSIB it)		{ putToContextSibPackage }
	def static String getPutToContextSibName(PutToContextSIB it)		{ it.nonEmptyName }
	def static String getPutToContextSibClass(PutToContextSIB it)		{ it.putToContextSibName.escapeClassName + putToContextSibClass }
	def static String getPutToContextSibFqn(PutToContextSIB it)			{ it.putToContextSibPackage + '.' + it.putToContextSibClass }
	
	// Abstract PutToContextBranch of concrete PutToContextSib
	def static String getPutToContextBranchPackage(PutToContextSIB it)	{ it.putToContextSibPackage }
	def static String getPutToContextBranchName(PutToContextSIB it)		{ it.putToContextSibName }
	def static String getPutToContextBranchClass(PutToContextSIB it)	{ it.putToContextBranchName.escapeClassName + putToContextBranchClass }
	def static String getPutToContextBranchFqn(PutToContextSIB it)		{ it.putToContextBranchPackage + '.' + it.putToContextBranchClass }
	
	// Concrete PutToContextBranch of concrete PutToContextSib
	def static String getPutToContextBranchPackage(Branch it)			{ it.putToContextSib.putToContextBranchPackage }
	def static String getPutToContextBranchName(Branch it)				{ it.name }
	def static String getPutToContextBranchClass(Branch it)				{ it.putToContextBranchName.escapeClassName + it.putToContextSib.putToContextBranchClass }
	def static String getPutToContextBranchFqn(Branch it)				{ it.putToContextBranchPackage + '.' + it.putToContextBranchClass }
	
	// Utils
	def static PutToContextSIB getPutToContextSib(Branch branch) {
		return branch.incomingBranchConnectors.head.sourceElement as PutToContextSIB
	}
	
	def static Branch getPutToContextBranch(PutToContextSIB sib, String branchName) {
		return sib.branchSuccessors.findFirst[name.toLowerCase == branchName.toLowerCase]
	}
	
	def static Branch getSuccessPutToContextBranch(PutToContextSIB sib) {
		return sib.getPutToContextBranch('success')
	}
	
	
	
	/*** Concrete classes: Contains ***/
	
	// Concrete ContainsSib
	def static String getContainsSibPackage(AbstractContainsSIB it)		{ containsSibPackage }
	def static String getContainsSibName(AbstractContainsSIB it)		{ it.nonEmptyName }
	def static String getContainsSibClass(AbstractContainsSIB it)		{ it.containsSibName.escapeClassName + containsSibClass }
	def static String getContainsSibFqn(AbstractContainsSIB it)			{ it.containsSibPackage + '.' + it.containsSibClass }

	// Abstract ContainsBranch of concrete ContainsSib
	def static String getContainsBranchPackage(AbstractContainsSIB it)	{ it.containsSibPackage }
	def static String getContainsBranchName(AbstractContainsSIB it)		{ it.containsSibName }
	def static String getContainsBranchClass(AbstractContainsSIB it)	{ it.containsBranchName.escapeClassName + containsBranchClass }
	def static String getContainsBranchFqn(AbstractContainsSIB it)		{ it.containsBranchPackage + '.' + it.containsBranchClass }

	// Concrete ContainsBranch of concrete ContainsSib
	def static String getContainsBranchPackage(Branch it)				{ it.containsSib.containsBranchPackage }
	def static String getContainsBranchName(Branch it)					{ it.name }
	def static String getContainsBranchClass(Branch it)					{ it.containsBranchName.escapeClassName + it.containsSib.containsBranchClass }
	def static String getContainsBranchFqn(Branch it)					{ it.containsBranchPackage + '.' + it.containsBranchClass }
	
	// Utils
	def static AbstractContainsSIB getContainsSib(Branch branch) {
		return branch.incomingBranchConnectors.head.sourceElement as AbstractContainsSIB
	}
	
	def static Branch getContainsBranch(AbstractContainsSIB sib, String branchName) {
		return sib.branchSuccessors.findFirst[name.toLowerCase == branchName.toLowerCase]
	}
	
	def static Branch getYesContainsBranch(AbstractContainsSIB sib) {
		return sib.getContainsBranch('yes')
	}
	
	def static Branch getNoContainsBranch(AbstractContainsSIB sib) {
		return sib.getContainsBranch('no')
	}
	
	
	
	/*** Concrete classes: Iterate ***/
	
	// Concrete IterateSib
	def static String getIterateSibPackage(AbstractIterateSIB it)		{ iterateSibPackage }
	def static String getIterateSibName(AbstractIterateSIB it)			{ it.nonEmptyName }
	def static String getIterateSibClass(AbstractIterateSIB it)			{ it.iterateSibName.escapeClassName + iterateSibClass }
	def static String getIterateSibFqn(AbstractIterateSIB it)			{ it.iterateSibPackage + '.' + it.iterateSibClass }

	// Abstract IterateBranch of concrete IterateSib
	def static String getIterateBranchPackage(AbstractIterateSIB it)	{ it.iterateSibPackage }
	def static String getIterateBranchName(AbstractIterateSIB it)		{ it.iterateSibName }
	def static String getIterateBranchClass(AbstractIterateSIB it)		{ it.iterateBranchName.escapeClassName + iterateBranchClass }
	def static String getIterateBranchFqn(AbstractIterateSIB it)		{ it.iterateBranchPackage + '.' + it.iterateBranchClass }
	
	// Concrete IterateBranch of concrete IterateSib
	def static String getIterateBranchPackage(Branch it)				{ it.iterateSib.iterateBranchPackage }
	def static String getIterateBranchName(Branch it)					{ it.name }
	def static String getIterateBranchClass(Branch it)					{ it.iterateBranchName.escapeClassName + it.iterateSib.iterateBranchClass }
	def static String getIterateBranchFqn(Branch it)					{ it.iterateBranchPackage + '.' + it.iterateBranchClass }
	
	// Utils
	def static AbstractIterateSIB getIterateSib(Branch branch) {
		return branch.incomingBranchConnectors.head.sourceElement as AbstractIterateSIB
	}
	
	def static Branch getIterateBranch(AbstractIterateSIB sib, String branchName) {
		return sib.branchSuccessors.findFirst[name.toLowerCase == branchName.toLowerCase]
	}
	
	def static Branch getNextIterateBranch(AbstractIterateSIB sib) {
		return sib.getIterateBranch('next')
	}
	
	def static Branch getExitIterateBranch(AbstractIterateSIB sib) {
		return sib.getIterateBranch('exit')
	}
	
	
	
	/*** Variables and ports ***/
	
	/**
	 * @return The name of the provided variable as a String.
	 * @author Fabian Storek
	 */
	def static String getVariableName(Object variable) {
		switch it : variable {
			Variable: name.escapeVariableName
			Input:    name.escapeVariableName
			Output:   name.escapeVariableName
			Port:     name.escapeVariableName
			default:  throw new SwitchException(it)
		}
	}
	
	/**
	 * @return The name of the provided variable as a String. If the variable
	 *         is a {@link NameExtension#isList(Object) list}, an additional
	 *         {@code null} check will will return a list.
	 * @author Fabian Storek
	 */
	def static String getVariableNameNullSafeList(Object variable) {
		'''«variable.variableName»«IF variable.isList»?: newArrayList«ENDIF»'''
	}
	
	/**
	 * @return The value of the provided static input as a String.
	 * @author Fabian Storek
	 */
	def static String getVariableValue(InputStatic variableOrType) {
		switch it : variableOrType {
			TextInputStatic:    "'" + value + "'"
			IntegerInputStatic: value.toString + 'L'
			RealInputStatic:    value.toString
			BooleanInputStatic: value.toString
			default:            throw new SwitchException(it)
		}
	}
	
	/**
	 * @return The value of the provided static input as a String, or the name
	 *         of the variable, if it is not a static input.
	 * @author Fabian Storek
	 */
	def static String getVariableValueOrName(Object variableOrType) {
		switch it : variableOrType {
			InputStatic: variableValue
			default:     variableName
		}
	}
	
	/**
	 * @return The class name of the provided variable or type as a String.
	 * @author Fabian Storek
	 */
	def static String getVariableClass(Object variableOrType) {
		
		switch it : variableOrType {
			
			// Process MGL
			JavaNativeVariable:         dataType.variableClass
			PrimitiveVariable:          dataType.variableClass
			
			PrimitiveInputPort:         dataType.variableClass
			JavaNativeInputPort:        dataType.variableClass
			
			PrimitiveOutputPort:        dataType.variableClass
			JavaNativeOutputPort:       dataType.variableClass
			
			TextInputStatic:            text
			IntegerInputStatic:         integer
			RealInputStatic:            real
			BooleanInputStatic:         bool
			
			info.scce.cinco.product.minidime.process.PrimitiveType case TEXT:    text
			info.scce.cinco.product.minidime.process.PrimitiveType case INTEGER: integer
			info.scce.cinco.product.minidime.process.PrimitiveType case REAL:    real
			info.scce.cinco.product.minidime.process.PrimitiveType case BOOLEAN: bool
			
			// SIB Library
			PrimitivePort:              type.variableClass
			JavaTypePort:               type.variableClass

			JavaType:                   fqn.split('\\.').last
			
			PrimitiveType case TEXT:    text
			PrimitiveType case INTEGER: integer
			PrimitiveType case REAL:    real
			PrimitiveType case BOOLEAN: bool
			
			// Default
			default:                    throw new SwitchException(it)
			
		}
		
	}
	
	/**
	 * @return The class name of the provided variable or type as a String.
	 * @author Fabian Storek
	 */
	def static String getVariableClassIncludeList(Object variableOrType) {
		if (variableOrType.isList) {
			'''«list»<«variableOrType.variableClass»>'''
		}
		else {
			variableOrType.variableClass
		}
	}
	
	/**
	 * @return The FQN(s) of the provided variable or type as a String.
	 * @author Fabian Storek
	 */
	def static List<String> getVariableFqn(Object variableOrType, boolean includeList) {
		
		val fqn = switch it : variableOrType {
			
			// Process MGL
			JavaNativeVariable:         dataType.variableFqn.head
			PrimitiveVariable:          dataType.variableFqn.head
			
			PrimitiveInputPort:         dataType.variableFqn.head
			JavaNativeInputPort:        dataType.variableFqn.head
			
			PrimitiveOutputPort:        dataType.variableFqn.head
			JavaNativeOutputPort:       dataType.variableFqn.head
			
			TextInputStatic:            textFqn
			IntegerInputStatic:         integerFqn
			RealInputStatic:            realFqn
			BooleanInputStatic:         boolFqn
			
			info.scce.cinco.product.minidime.process.PrimitiveType case TEXT:    textFqn
			info.scce.cinco.product.minidime.process.PrimitiveType case INTEGER: integerFqn
			info.scce.cinco.product.minidime.process.PrimitiveType case REAL:    realFqn
			info.scce.cinco.product.minidime.process.PrimitiveType case BOOLEAN: boolFqn
			
			// SIB Library
			PrimitivePort:              type.variableFqn.head
			JavaTypePort:               type.variableFqn.head

			JavaType:                   fqn
			
			PrimitiveType case TEXT:    textFqn
			PrimitiveType case INTEGER: integerFqn
			PrimitiveType case REAL:    realFqn
			PrimitiveType case BOOLEAN: boolFqn
			
			// Default
			default:                    throw new SwitchException(it)
			
		}
		
		if (includeList && variableOrType.isList) {
			#[fqn, listFqn]
		}
		else {
			#[fqn]
		}
		
	}
	
	/**
	 * @return The FQN(s) of the provided variable or type as a String.
	 * @author Fabian Storek
	 */
	def static List<String> getVariableFqn(Object variableOrType) {
		variableOrType.getVariableFqn(true)
	}
	
	/**
	 * @author Fabian Storek
	 */
	def static String wrapped(String className) {
		switch className {
			case 'byte':    'Byte'
			case 'short':   'Short'
			case 'int':     'Integer'
			case 'long':    'Long'
			case 'float':   'Float'
			case 'double':  'Double'
			case 'boolean': 'Boolean'
			case 'char':    'Character'
			default:        className
		}
	}
	
	/**
	 * @author Fabian Storek
	 */
	def static String unwrapped(String className) {
		switch className {
			case 'Byte':      'byte'
			case 'Short':     'short'
			case 'Integer':   'int'
			case 'Long':      'long'
			case 'Float':     'float'
			case 'Double':    'double'
			case 'Boolean':   'boolean'
			case 'Character': 'char'
			default:          className
		}
	}
	
	/**
	 * @author Fabian Storek
	 */
	def static boolean isJavaLangFqn(String fqn) {
		return fqn.startsWith('java.lang.')
	}
	
	/**
	 * @author Fabian Storek
	 */
	def static boolean isList(Object variableOrType) {
		switch it : variableOrType {
			
			// Process MGL
			JavaNativeVariable:         isList
			PrimitiveVariable:          isList
			
			PrimitiveInputPort:         isList
			JavaNativeInputPort:        isList
			
			PrimitiveOutputPort:        isList
			JavaNativeOutputPort:       isList
			
			TextInputStatic:            false
			IntegerInputStatic:         false
			RealInputStatic:            false
			BooleanInputStatic:         false
			
			info.scce.cinco.product.minidime.process.PrimitiveType case TEXT:    false
			info.scce.cinco.product.minidime.process.PrimitiveType case INTEGER: false
			info.scce.cinco.product.minidime.process.PrimitiveType case REAL:    false
			info.scce.cinco.product.minidime.process.PrimitiveType case BOOLEAN: false
			
			// SIB Library
			PrimitivePort:              isList
			JavaTypePort:               isList

			JavaType:                   false
			
			PrimitiveType case TEXT:    false
			PrimitiveType case INTEGER: false
			PrimitiveType case REAL:    false
			PrimitiveType case BOOLEAN: false
			
			// Default
			default:                    throw new SwitchException(it)
			
		}
	}
	
	
	
	/*** Simplification ***/
	
	// Concrete Sibs
	def static String getSibPackage(SIB sib) {
		sib.atomicSibPackage
	}
	
	def static String getSibPackage(MiniDIMEProcess sib) {
		sib.processSibPackage
	}
	
	def static String getSibPackage(info.scce.cinco.product.minidime.process.SIB sib) {
		switch sib {
			AtomicSIB:				sib.atomicSib.atomicSibPackage
			ProcessSIB:				sib.processSib.processSibPackage
			PutToContextSIB:		sib.putToContextSibPackage
			AbstractContainsSIB:	sib.containsSibPackage
			AbstractIterateSIB:		sib.iterateSibPackage
			default:				throw new SwitchException(sib)
		}
	}
	
	def static String getSibName(SIB sib) {
		sib.atomicSibName
	}
	
	def static String getSibName(MiniDIMEProcess sib) {
		sib.processBranchName
	}
	
	def static String getSibName(info.scce.cinco.product.minidime.process.SIB sib) {
		switch sib {
			AtomicSIB:				sib.atomicSib.atomicSibName
			ProcessSIB:				sib.processSib.processSibName
			PutToContextSIB:		sib.putToContextSibName
			AbstractContainsSIB:	sib.containsSibName
			AbstractIterateSIB:		sib.iterateSibName
			default:				throw new SwitchException(sib)
		}
	}
	
	def static String getSibClass(SIB sib) {
		sib.atomicSibClass
	}
	
	def static String getSibClass(MiniDIMEProcess sib) {
		sib.processSibClass
	}
	
	def static String getSibClass(info.scce.cinco.product.minidime.process.SIB sib) {
		switch sib {
			AtomicSIB:				sib.atomicSib.atomicSibClass
			ProcessSIB:				sib.processSib.processSibClass
			PutToContextSIB:		sib.putToContextSibClass
			AbstractContainsSIB:	sib.containsSibClass
			AbstractIterateSIB:		sib.iterateSibClass
			default:				throw new SwitchException(sib)
		}
	}
	
	def static String getSibFqn(SIB sib) {
		sib.atomicSibFqn
	}
	
	def static String getSibFqn(MiniDIMEProcess sib) {
		sib.processSibFqn
	}
	
	def static String getSibFqn(info.scce.cinco.product.minidime.process.SIB sib) {
		switch sib {
			AtomicSIB:				sib.atomicSib.atomicSibFqn
			ProcessSIB:				sib.processSib.processSibFqn
			PutToContextSIB:		sib.putToContextSibFqn
			AbstractContainsSIB:	sib.containsSibFqn
			AbstractIterateSIB:		sib.iterateSibFqn
			default:				throw new SwitchException(sib)
		}
	}
	
	// Abstract Branches of concrete Sibs
	def static String getBranchPackage(SIB sib) {
		sib.atomicBranchPackage
	}
	
	def static String getBranchPackage(MiniDIMEProcess sib) {
		sib.processBranchPackage
	}
	
	def static String getBranchPackage(info.scce.cinco.product.minidime.process.SIB sib) {
		switch sib {
			AtomicSIB:				sib.atomicSib.atomicBranchPackage
			ProcessSIB:				sib.processSib.processBranchPackage
			PutToContextSIB:		sib.putToContextBranchPackage
			AbstractContainsSIB:	sib.containsBranchPackage
			AbstractIterateSIB:		sib.iterateBranchPackage
			default:				throw new SwitchException(sib)
		}
	}
	
	def static String getBranchName(SIB sib) {
		sib.atomicBranchName
	}
	
	def static String getBranchName(MiniDIMEProcess sib) {
		sib.processBranchName
	}
	
	def static String getBranchName(info.scce.cinco.product.minidime.process.SIB sib) {
		switch sib {
			AtomicSIB:				sib.atomicSib.atomicBranchName
			ProcessSIB:				sib.processSib.processBranchName
			PutToContextSIB:		sib.putToContextBranchName
			AbstractContainsSIB:	sib.containsBranchName
			AbstractIterateSIB:		sib.iterateBranchName
			default:				throw new SwitchException(sib)
		}
	}
	
	def static String getBranchClass(SIB sib) {
		sib.atomicBranchClass
	}
	
	def static String getBranchClass(MiniDIMEProcess sib) {
		sib.processBranchClass
	}
	
	def static String getBranchClass(info.scce.cinco.product.minidime.process.SIB sib) {
		switch sib {
			AtomicSIB:				sib.atomicSib.atomicBranchClass
			ProcessSIB:				sib.processSib.processBranchClass
			PutToContextSIB:		sib.putToContextBranchClass
			AbstractContainsSIB:	sib.containsBranchClass
			AbstractIterateSIB:		sib.iterateBranchClass
			default:				throw new SwitchException(sib)
		}
	}
	
	def static String getBranchFqn(SIB sib) {
		sib.atomicBranchFqn
	}
	
	def static String getBranchFqn(MiniDIMEProcess sib) {
		sib.processBranchFqn
	}
	
	def static String getBranchFqn(info.scce.cinco.product.minidime.process.SIB sib) {
		switch sib {
			AtomicSIB:				sib.atomicSib.atomicBranchFqn
			ProcessSIB:				sib.processSib.processBranchFqn
			PutToContextSIB:		sib.putToContextBranchFqn
			AbstractContainsSIB:	sib.containsBranchFqn
			AbstractIterateSIB:		sib.iterateBranchFqn
			default:				throw new SwitchException(sib)
		}
	}
	
	// Concrete Branches of concrete Sibs
	def static String getBranchPackage(info.scce.cinco.product.minidime.siblibrary.Branch branch) {
		branch.atomicBranchPackage
	}
	
	def static String getBranchPackage(EndSIB branch) {
		branch.processBranchPackage
	}
	
	def static String getBranchPackage(Branch branch) {
		switch branch.predecessors.head {
			AtomicSIB:				branch.atomicBranch.atomicBranchPackage
			ProcessSIB:				branch.processBranch.processBranchPackage
			PutToContextSIB:		branch.putToContextBranchPackage
			AbstractContainsSIB:	branch.containsBranchPackage
			AbstractIterateSIB:		branch.iterateBranchPackage
			default:				throw new SwitchException(branch)
		}
	}
	
	def static String getBranchName(info.scce.cinco.product.minidime.siblibrary.Branch branch) {
		branch.atomicBranchName
	}
	
	def static String getBranchName(EndSIB branch) {
		branch.processBranchName
	}
	
	def static String getBranchName(Branch branch) {
		switch branch.predecessors.head {
			AtomicSIB:				branch.atomicBranch.atomicBranchName
			ProcessSIB:				branch.processBranch.processBranchName
			PutToContextSIB:		branch.putToContextBranchName
			AbstractContainsSIB:	branch.containsBranchName
			AbstractIterateSIB:		branch.iterateBranchName
			default:				throw new SwitchException(branch)
		}
	}
	
	def static String getBranchClass(info.scce.cinco.product.minidime.siblibrary.Branch branch) {
		branch.atomicBranchClass
	}
	
	def static String getBranchClass(EndSIB branch) {
		branch.processBranchClass
	}
	
	def static String getBranchClass(Branch branch) {
		switch branch.predecessors.head {
			AtomicSIB:				branch.atomicBranch.atomicBranchClass
			ProcessSIB:				branch.processBranch.processBranchClass
			PutToContextSIB:		branch.putToContextBranchClass
			AbstractContainsSIB:	branch.containsBranchClass
			AbstractIterateSIB:		branch.iterateBranchClass
			default:				throw new SwitchException(branch)
		}
	}
	
	def static String getBranchFqn(info.scce.cinco.product.minidime.siblibrary.Branch branch) {
		branch.atomicBranchFqn
	}
	
	def static String getBranchFqn(EndSIB branch) {
		branch.processBranchFqn
	}
	
	def static String getBranchFqn(Branch branch) {
		switch branch.predecessors.head {
			AtomicSIB:				branch.atomicBranch.atomicBranchFqn
			ProcessSIB:				branch.processBranch.processBranchFqn
			PutToContextSIB:		branch.putToContextBranchFqn
			AbstractContainsSIB:	branch.containsBranchFqn
			AbstractIterateSIB:		branch.iterateBranchFqn
			default:				throw new SwitchException(branch)
		}
	}
	
}
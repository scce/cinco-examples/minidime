package info.scce.cinco.product.minidime.generator.sib.iterate

import info.scce.cinco.product.minidime.generator.MiniDIMEXtendTemplate
import info.scce.cinco.product.minidime.process.AbstractIterateSIB

import static extension info.scce.cinco.product.minidime.generator.helper.NameExtension.*

/**
 * @author Fabian Storek
 */
class IterateBranchGenerator extends MiniDIMEXtendTemplate {
	
	val AbstractIterateSIB sib
	
	new(AbstractIterateSIB sib) {
		this.sib = sib
	}
	
	override packageName() {
		sib.iterateBranchPackage
	}
	
	override className() {
		sib.iterateBranchClass
	}
	
	override classTemplate() '''		
		abstract class «sib.iterateBranchFqn.shorten» extends «iterateBranchFqn.shorten» {
			// Intentionally left blank
		}
		«FOR branch : sib.branchSuccessors»
			
			«dataAnnotation»
			class «branch.iterateBranchClass» extends «sib.iterateBranchFqn.shorten» {
				«IF branch.outputPorts.nullOrEmpty»
					// Intentionally left blank
				«ELSE»
					
					// Variables
					«FOR port : branch.outputPorts»
						val «port.variableFqn.shorten» «port.variableName»
					«ENDFOR»
					
				«ENDIF»
			}
		«ENDFOR»
	'''
	
}

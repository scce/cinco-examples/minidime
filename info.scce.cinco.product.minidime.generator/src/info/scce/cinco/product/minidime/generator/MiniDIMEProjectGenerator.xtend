package info.scce.cinco.product.minidime.generator

import de.jabc.cinco.meta.plugin.template.ProjectTemplate
import info.scce.cinco.product.minidime.generator.common.BooleanExtensionGenerator
import info.scce.cinco.product.minidime.generator.common.BooleanSibsGenerator
import info.scce.cinco.product.minidime.generator.common.DoubleExtensionGenerator
import info.scce.cinco.product.minidime.generator.common.IntegerSibsGenerator
import info.scce.cinco.product.minidime.generator.common.ListExtensionGenerator
import info.scce.cinco.product.minidime.generator.common.LongExtensionGenerator
import info.scce.cinco.product.minidime.generator.common.RealSibsGenerator
import info.scce.cinco.product.minidime.generator.common.StringExtensionGenerator
import info.scce.cinco.product.minidime.generator.common.TextSibsGenerator
import info.scce.cinco.product.minidime.generator.main.MainGenerator
import info.scce.cinco.product.minidime.generator.sib.AbstractBranchGenerator
import info.scce.cinco.product.minidime.generator.sib.AbstractElementGenerator
import info.scce.cinco.product.minidime.generator.sib.AbstractSibGenerator
import info.scce.cinco.product.minidime.generator.sib.atomic.AbstractAtomicBranchGenerator
import info.scce.cinco.product.minidime.generator.sib.atomic.AbstractAtomicSibGenerator
import info.scce.cinco.product.minidime.generator.sib.atomic.AtomicBranchGenerator
import info.scce.cinco.product.minidime.generator.sib.atomic.AtomicSibGenerator
import info.scce.cinco.product.minidime.generator.sib.contains.AbstractContainsBranchGenerator
import info.scce.cinco.product.minidime.generator.sib.contains.AbstractContainsSibGenerator
import info.scce.cinco.product.minidime.generator.sib.contains.ContainsBranchGenerator
import info.scce.cinco.product.minidime.generator.sib.contains.ContainsSibGenerator
import info.scce.cinco.product.minidime.generator.sib.iterate.AbstractIterateBranchGenerator
import info.scce.cinco.product.minidime.generator.sib.iterate.AbstractIterateSibGenerator
import info.scce.cinco.product.minidime.generator.sib.iterate.IterateBranchGenerator
import info.scce.cinco.product.minidime.generator.sib.iterate.IterateSibGenerator
import info.scce.cinco.product.minidime.generator.sib.process.AbstractProcessBranchGenerator
import info.scce.cinco.product.minidime.generator.sib.process.AbstractProcessSibGenerator
import info.scce.cinco.product.minidime.generator.sib.process.ProcessBranchGenerator
import info.scce.cinco.product.minidime.generator.sib.process.ProcessSibGenerator
import info.scce.cinco.product.minidime.generator.sib.puttocontext.AbstractPutToContextBranchGenerator
import info.scce.cinco.product.minidime.generator.sib.puttocontext.AbstractPutToContextSibGenerator
import info.scce.cinco.product.minidime.generator.sib.puttocontext.PutToContextBranchGenerator
import info.scce.cinco.product.minidime.generator.sib.puttocontext.PutToContextSibGenerator
import info.scce.cinco.product.minidime.generator.tuple.TupleGenerator
import info.scce.cinco.product.minidime.process.AbstractContainsSIB
import info.scce.cinco.product.minidime.process.AbstractIterateSIB
import info.scce.cinco.product.minidime.process.PutToContextSIB
import info.scce.cinco.product.minidime.siblibrary.SIB
import java.util.Set

import static info.scce.cinco.product.minidime.generator.helper.NameExtension.*

import static extension de.jabc.cinco.meta.core.utils.projects.ProjectCreator.getProject
import static extension info.scce.cinco.product.minidime.generator.helper.GeneratorExtension.getProcessDescendants
import info.scce.cinco.product.minidime.process.MiniDIMEProcess

/**
 * @author Fabian Storek
 */
class MiniDIMEProjectGenerator extends ProjectTemplate {
	
	val MiniDIMEProcess						process
	val Set<MiniDIMEProcess>				allProcessSibs
	val Set<SIB>					allAtomicSibs
	val Set<PutToContextSIB>		allPutToContextSibs
	val Set<AbstractContainsSIB>	allContainsSibs
	val Set<AbstractIterateSIB>		allIterateSibs
	val int							largestTuple
		
	new(MiniDIMEProcess process) {
		this.process				= process
		this.allProcessSibs			= process.processDescendants
		this.allAtomicSibs			= allProcessSibs.map[atomicSIBs].flatten.map[sib as SIB].toSet
		this.allPutToContextSibs		= allProcessSibs.map[putToContextSIBs].flatten.toSet
		this.allContainsSibs		= allProcessSibs.map[abstractContainsSIBs].flatten.toSet
		this.allIterateSibs			= allProcessSibs.map[abstractIterateSIBs].flatten.toSet
		this.largestTuple			= (allAtomicSibs.map[branches].flatten.filter[name == 'success'].map[outputPorts.size] + #[10]).max
	}
	
	override projectDescription() {
		
		project(process.eResource.project.name) [
			
			deleteIfExistent = false
			
			natures = #['org.eclipse.xtext.ui.shared.xtextNature']
			
			requiredBundles = #[
				'com.google.guava',
				'org.eclipse.xtext.xbase.lib',
				'org.eclipse.xtend.lib',
				'org.eclipse.xtend.lib.macro'
			]
			
			folder(commonSibPackage) [
				deleteIfExistent = false
				isSourceFolder = false
				file(new IntegerSibsGenerator, false)
				file(new RealSibsGenerator, false)
				file(new BooleanSibsGenerator, false)
				file(new TextSibsGenerator, false)
			]
			
			folder('src') [
				deleteIfExistent = false
				isSourceFolder = true
				
				pkg(mainPackage) [
					file(new MainGenerator(process), false)
				]
				
			]
			
			folder('src-gen') [
				deleteIfExistent = true
				isSourceFolder = true
				
				pkg(tuplePackage) [
					file(new TupleGenerator(largestTuple))
				]
				
				pkg(commonExtensionPackage) [
					file(new LongExtensionGenerator)
					file(new DoubleExtensionGenerator)
					file(new BooleanExtensionGenerator)
					file(new StringExtensionGenerator)
					file(new ListExtensionGenerator)
				]
				
				pkg(sibPackage) [
					file(new AbstractElementGenerator)
					file(new AbstractSibGenerator)
					file(new AbstractBranchGenerator)
				]
				
				pkg(atomicSibPackage) [
					file(new AbstractAtomicSibGenerator)
					file(new AbstractAtomicBranchGenerator)
					forEachOf(allAtomicSibs) [sib |
						file(new AtomicSibGenerator(sib))
						file(new AtomicBranchGenerator(sib))
					]
				]
				
				pkg(processSibPackage) [
					file(new AbstractProcessSibGenerator)
					file(new AbstractProcessBranchGenerator)
					forEachOf(allProcessSibs) [sib |
						file(new ProcessSibGenerator(sib))
						file(new ProcessBranchGenerator(sib))
					]
				]
				
				pkg(putToContextSibPackage) [
					file(new AbstractPutToContextSibGenerator)
					file(new AbstractPutToContextBranchGenerator)
					forEachOf(allPutToContextSibs) [sib |
						file(new PutToContextSibGenerator(sib))
						file(new PutToContextBranchGenerator(sib))
					]
				]
				
				pkg(containsSibPackage) [
					file(new AbstractContainsSibGenerator)
					file(new AbstractContainsBranchGenerator)
					forEachOf(allContainsSibs) [sib |
						file(new ContainsSibGenerator(sib))
						file(new ContainsBranchGenerator(sib))
					]
				]
				
				pkg(iterateSibPackage) [
					file(new AbstractIterateSibGenerator)
					file(new AbstractIterateBranchGenerator)
					forEachOf(allIterateSibs) [sib |
						file(new IterateSibGenerator(sib))
						file(new IterateBranchGenerator(sib))
					]
				]
				
			]
			
		]
		
	}
	
	override projectSuffix() {
		throw new UnsupportedOperationException("TODO: auto-generated method stub")
	}
	
}

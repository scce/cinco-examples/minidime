package info.scce.cinco.product.minidime.generator

import static extension info.scce.cinco.product.minidime.generator.helper.GeneratorExtension.*

/**
 * @author Fabian Storek
 */
abstract class MiniDIMESibsTemplate extends MiniDIMEFileTemplate {
	
	override final fileName() {
		'''«libraryName.escapeClassName».sibs'''
	}
	
	override final fileTemplate() '''
		package «packageName.escapePackageName»
		
		«libraryTemplate»
	'''
	
	def String packageName()
	
	def String libraryName()
	
	def String libraryTemplate()
	
}

package info.scce.cinco.product.minidime.generator.sib.contains

import info.scce.cinco.product.minidime.generator.MiniDIMEXtendTemplate

import static info.scce.cinco.product.minidime.generator.helper.NameExtension.*

/**
 * @author Fabian Storek
 */
class AbstractContainsSibGenerator extends MiniDIMEXtendTemplate {
	
	override packageName() {
		containsSibPackage
	}
	
	override className() {
		containsSibClass
	}
	
	override classTemplate() '''		
		abstract class «containsSibClass» extends «sibFqn.shorten» {
			// Intentionally left blank
		}
	'''
	
}

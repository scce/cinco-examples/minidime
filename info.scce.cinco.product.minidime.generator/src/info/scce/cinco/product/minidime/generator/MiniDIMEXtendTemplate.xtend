package info.scce.cinco.product.minidime.generator

import java.util.List
import org.eclipse.xtend.lib.annotations.AccessorType
import org.eclipse.xtend.lib.annotations.Accessors
import org.eclipse.xtend.lib.annotations.Data

import static extension info.scce.cinco.product.minidime.generator.helper.NameExtension.*

/**
 * @author Fabian Storek
 */
abstract class MiniDIMEXtendTemplate extends MiniDIMEFileTemplate {
	
	var List<String> importFqns = newLinkedList
	
	override final fileName() {
		'''«className».xtend'''
	}
	
	override final fileTemplate() {
		// Run classTemplate() first, in case more FQNs will be added to the
		// import list
		val classTemplateResult = classTemplate
		'''
			«IF !packageName.nullOrEmpty»
				package «packageName»
			«ENDIF»
			«IF !importFqns.nullOrEmpty»
				
				«FOR fqn : importFqns.sort»
					import «fqn»
				«ENDFOR»
			«ENDIF»
			
			«classTemplateResult»
		'''
	}
	
	/**
	 * Checks, if this FQN is the package of the generated class.
	 */
	private def boolean isInPackage(String fqn) {
		fqn.startsWith(packageName + '.') &&
		!fqn.replace(packageName + '.', '').contains('.')
	}
	
	/**
	 * Extracts the class name from an FQN.
	 */
	private def String getClassName(String fqn) {
		fqn.replaceAll('''[0-9a-zA-Z_]+\.''', '')
	}
	
	/**
	 * Extracts the package name from an FQN.
	 */
	private def String getPackageName(String fqn) {
		fqn.replaceAll('''\.«fqn.className»$''', '')
	}
	
	/**
	 * Checks, if this FQN would result in an import conflict.
	 * @returns {@code true}, if there is an FQN in the import list, which has
	 *          the same class name as {@code fqn}.
	 */
	private def boolean isImportConflict(String fqn) {
		val className = fqn.className
		val packageName = fqn.packageName
		importFqns.exists[other | other.className == className && other.packageName != packageName]
	}
	
	/** 
	 * Adds all provided FQN(s) to the import list. Duplicates will be removed.
	 * {@code java.lang} types will be removed. The FQNs will be sorted.
	 * @returns {@code true}, if all FQNs have been added.
	 */
	final def boolean addImport(String ... fqns) {
		if (fqns.nullOrEmpty) {
			return false
		}
		var error = false
		for (fqn : fqns) {
			if (importFqns.contains(fqn) || fqn.isJavaLangFqn || fqn.isInPackage) {
				// No conflict and do not add FQN
			}
			else if (fqn.nullOrEmpty || fqn.importConflict) {
				error = true
			}
			else {
				importFqns.add(fqn)
			}
		}
		return !error
	}
	
	/** 
	 * Adds all provided FQN(s) to the import list. Duplicates will be removed.
	 * {@code java.lang} types will be removed. The FQNs will be sorted.
	 * @returns {@code true}, if all FQNs have been added.
	 */
	final def boolean addImport(String fqn) {
		#[fqn].addImport
	}
	
	/** 
	 * If there is no conflict with the import list, this method shortens the
	 * FQN to the class name and adds the FQN to the import list.
	 * @returns the class name (without package), if there is no conflict or
	 *          the whole FQN, if there is already an import with the same
	 *          class name.
	 */
	final def String shorten(String fqn) {
		if (fqn.nullOrEmpty) {
			return null
		}
		else if (fqn.addImport) {
			return fqn.className
		}
		else {
			return fqn
		}
	}
	
	/** 
	 * If there is no conflict with the import list, this method shortens the
	 * FQN to the class name and adds the FQN to the import list.
	 * @returns the class name (without package), if there is no conflict or
	 *          the whole FQN, if there is already an import with the same
	 *          class name.
	 */
	final def String shorten(List<String> fqns) {
		if (fqns === null) throw new IllegalArgumentException('FQN list is null')
		switch fqns.size {
			case 0:
				throw new IllegalArgumentException('FQN list is empty')
			case 1:
				return fqns.head.shorten
			case 2: {
				val listPattern = '''^java\.util\.[0-9a-zA-Z_]*List$'''
				val listFqn = fqns.findFirst[ matches(listPattern)]
				val typeFqn = fqns.findFirst[!matches(listPattern)]
				if (listFqn === null) throw new IllegalArgumentException('FQN list only contains List types')
				if (typeFqn === null) throw new IllegalArgumentException('FQN list does not contain a List type')
				return '''«listFqn.shorten»<«typeFqn.shorten»>'''
			}
			default:
				throw new IllegalArgumentException('FQN list does contain more than 2 elements')
		}
	}
	
	/**
	 * @returns the {@link Data @Data} annotation.
	 */
	final def String dataAnnotation() {
		'''@«shorten('org.eclipse.xtend.lib.annotations.Data')»'''
	}
	
	/**
	 * @returns the {@link Accessors @Accessors}
	 * annotation with the provided {@link AccessorType}.
	 */
	final def String accessorsAnnotation(AccessorType ... accessorTypes) {
		'''@«shorten('org.eclipse.xtend.lib.annotations.Accessors')»«FOR at : accessorTypes BEFORE '(' SEPARATOR ', ' AFTER ')'»«at»«ENDFOR»'''
	}
	
	/**
	 * This method must return the package name.
	 */
	def String packageName()
	
	/**
	 * This method must return the class name. The generated file will be named
	 * {@code <className>.xtend}.
	 */
	def String className()
	
	/**
	 * This template method must return the class definition and class body.
	 * Imports can be added via {@link #addImportFqns(String[]) addImportFqns(String ...)}.
	 */
	def String classTemplate()
	
}

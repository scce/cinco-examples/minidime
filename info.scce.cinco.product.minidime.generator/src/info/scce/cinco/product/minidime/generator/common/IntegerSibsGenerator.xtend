package info.scce.cinco.product.minidime.generator.common

import info.scce.cinco.product.minidime.generator.MiniDIMESibsTemplate

import static extension info.scce.cinco.product.minidime.generator.helper.NameExtension.*

/**
 * @author Fabian Storek
 */
class IntegerSibsGenerator extends MiniDIMESibsTemplate {
	
	val fqn = 'Long'.commonExtensionFqn
	
	override String packageName() {
		libraryName.commonSibPackage
	}
	
	override libraryName() {
		'Integer'
	}
	
	override libraryTemplate() '''
		sib Print: «fqn»#print
			input: integer
			-> success
		
		sib ToReal: «fqn»#toDouble
			input: integer
			-> success
				result: real
		
		sib ToBoolean: «fqn»#toBoolean
			input: integer
			-> success
				result: boolean
		
		sib ToText: «fqn»#toString
			input: integer
			-> success
				result: text
		
		sib Equal: «fqn»#equal
			input1: integer
			input2: integer
			-> success
				result: boolean
		
		sib NotEqual: «fqn»#notEqual
			input1: integer
			input2: integer
			-> success
				result: boolean
		
		sib Less: «fqn»#less
			input1: integer
			input2: integer
			-> success
				result: boolean
		
		sib LessOrEqual: «fqn»#lessOrEqual
			input1: integer
			input2: integer
			-> success
				result: boolean
		
		sib Greater: «fqn»#greater
			input1: integer
			input2: integer
			-> success
				result: boolean
		
		sib GreaterOrEqual: «fqn»#greaterOrEqual
			input1: integer
			input2: integer
			-> success
				result: boolean
		
		sib Add: «fqn»#add
			input1: integer
			input2: integer
			-> success
				result: integer
		
		sib Subtract: «fqn»#subtract
			input1: integer
			input2: integer
			-> success
				result: integer
		
		sib Multiply: «fqn»#multiply
			input1: integer
			input2: integer
			-> success
				result: integer
		
		sib Divide: «fqn»#divide
			input1: integer
			input2: integer
			-> success
				result: integer
	'''
	
}

package info.scce.cinco.product.minidime.generator.sib.iterate

import info.scce.cinco.product.minidime.generator.MiniDIMEXtendTemplate

import static info.scce.cinco.product.minidime.generator.helper.NameExtension.*

/**
 * @author Fabian Storek
 */
class AbstractIterateBranchGenerator extends MiniDIMEXtendTemplate {
	
	override packageName() {
		iterateBranchPackage
	}
	
	override className() {
		iterateBranchClass
	}
	
	override classTemplate() '''		
		abstract class «iterateBranchClass» extends «branchFqn.shorten» {
			// Intentionally left blank
		}
	'''
	
}

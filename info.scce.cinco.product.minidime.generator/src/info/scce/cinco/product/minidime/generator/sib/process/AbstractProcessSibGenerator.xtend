package info.scce.cinco.product.minidime.generator.sib.process

import info.scce.cinco.product.minidime.generator.MiniDIMEXtendTemplate

import static info.scce.cinco.product.minidime.generator.helper.NameExtension.*

/**
 * @author Fabian Storek
 */
class AbstractProcessSibGenerator extends MiniDIMEXtendTemplate {
	
	override packageName() {
		processSibPackage
	}
	
	override className() {
		processSibClass
	}
	
	override classTemplate() '''		
		abstract class «processSibClass» extends «sibFqn.shorten» {
			// Intentionally left blank
		}
	'''
	
}

package info.scce.cinco.product.minidime.generator.common

import info.scce.cinco.product.minidime.generator.MiniDIMEXtendTemplate

import static extension info.scce.cinco.product.minidime.generator.helper.NameExtension.*

/**
 * @author Fabian Storek
 */
class ListExtensionGenerator extends MiniDIMEXtendTemplate {
	
	override packageName() {
		commonExtensionPackage
	}
	
	override className() {
		'List'.commonExtensionClass
	}
	
	override classTemplate() '''
		import java.util.List
		import «2.tupleFqn»
		
		class ListExtension {
			
			def static <T> void print(List<T> l) {
				println(l)
			}
			
			def static <T> Boolean equal(List<T> l1, List<T> l2) {
				l1 === l2 || l1.equals(l2)
			}
			
			def static <T> Boolean notEqual(List<T> l1, List<T> l2) {
				!l1.equal(l2)
			}
			
			def static <T> T getElement(List<T> l, Long i) {
				l.get(i.intValue)
			}
			
			def static <T> void setElement(List<T> l, Long i, T e) {
				l.set(i.intValue, e)
			}
			
			def static <T> void removeElement(List<T> l, T e) {
				l.remove(e)
			}
			
			def static <T> void insertAtIndex(List<T> l, Long i, T e) {
				l.add(i.intValue, e)
			}
			
			def static <T> T removeAtIndex(List<T> l, Long i) {
				l.remove(i.intValue)
			}
			
			def static <T> «2.tupleClass»<T, List<T>> splitFirst(List<T> l) {
				new «2.tupleClass»(l.head, l.tail)
			}
			
			def static <T> «2.tupleClass»<List<T>, T> splitLast(List<T> l) {
				new «2.tupleClass»(l.reverse.tail.toList.reverse, l.last)
			}
			
			def static <T> List<T> prepend(List<T> l, T e) {
				(#[e] + l).toList
			}
			
			def static <T> List<T> append(List<T> l, T e) {
				(l + #[e]).toList
			}
			
			def static <T> List<T> reverseOrder(List<T> l) {
				l.reverse
			}
			
		}
	'''
	
}

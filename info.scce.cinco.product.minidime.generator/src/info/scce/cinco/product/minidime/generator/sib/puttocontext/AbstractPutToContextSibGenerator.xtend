package info.scce.cinco.product.minidime.generator.sib.puttocontext

import info.scce.cinco.product.minidime.generator.MiniDIMEXtendTemplate

import static info.scce.cinco.product.minidime.generator.helper.NameExtension.*

/**
 * @author Fabian Storek
 */
class AbstractPutToContextSibGenerator extends MiniDIMEXtendTemplate {
	
	override packageName() {
		putToContextSibPackage
	}
	
	override className() {
		putToContextSibClass
	}
	
	override classTemplate() '''		
		abstract class «putToContextSibClass» extends «sibFqn.shorten» {
			// Intentionally left blank
		}
	'''
	
}

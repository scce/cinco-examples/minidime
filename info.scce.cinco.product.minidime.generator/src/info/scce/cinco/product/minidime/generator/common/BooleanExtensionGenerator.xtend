package info.scce.cinco.product.minidime.generator.common

import info.scce.cinco.product.minidime.generator.MiniDIMEXtendTemplate

import static extension info.scce.cinco.product.minidime.generator.helper.NameExtension.*

/**
 * @author Fabian Storek
 */
class BooleanExtensionGenerator extends MiniDIMEXtendTemplate {
	
	override packageName() {
		commonExtensionPackage
	}
	
	override className() {
		'Boolean'.commonExtensionClass
	}
	
	override classTemplate() '''
		class «className» {
			
			def static void print(Boolean b) {
				println(b)
			}
			
			def static Long toLong(Boolean b) {
				if (b === null) return null
				if (b) 1L else 0L
			}
			
			def static Double toDouble(Boolean b) {
				if (b === null) return null
				if (b) 1D else 0D
			}
			
			def static String toString(Boolean b) {
				b?.toString
			}
			
			def static Boolean not(Boolean b) {
				!b
			}
			
			def static Boolean equal(Boolean b1, Boolean b2) {
				b1 == b2
			}
			
			def static Boolean notEqual(Boolean b1, Boolean b2) {
				b1 != b2
			}
			
			def static Boolean and(Boolean b1, Boolean b2) {
				b1 && b2
			}
			
			def static Boolean or(Boolean b1, Boolean b2) {
				b1 || b2
			}
			
			def static Boolean ifSwitch(Boolean b) {
				b
			}
			
		}
	'''
	
}

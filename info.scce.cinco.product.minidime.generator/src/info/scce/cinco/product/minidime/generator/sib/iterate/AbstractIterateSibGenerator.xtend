package info.scce.cinco.product.minidime.generator.sib.iterate

import info.scce.cinco.product.minidime.generator.MiniDIMEXtendTemplate

import static info.scce.cinco.product.minidime.generator.helper.NameExtension.*

/**
 * @author Fabian Storek
 */
class AbstractIterateSibGenerator extends MiniDIMEXtendTemplate {
	
	override packageName() {
		iterateSibPackage
	}
	
	override className() {
		iterateSibClass
	}
	
	override classTemplate() '''		
		abstract class «iterateSibClass» extends «sibFqn.shorten» {
			// Intentionally left blank
		}
	'''
	
}

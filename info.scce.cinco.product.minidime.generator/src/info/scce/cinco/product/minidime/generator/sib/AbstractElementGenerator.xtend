package info.scce.cinco.product.minidime.generator.sib

import info.scce.cinco.product.minidime.generator.MiniDIMEXtendTemplate

import static info.scce.cinco.product.minidime.generator.helper.NameExtension.*

/**
 * @author Fabian Storek
 */
class AbstractElementGenerator extends MiniDIMEXtendTemplate {
	
	override packageName() {
		elementPackage
	}
	
	override className() {
		elementClass
	}
	
	override classTemplate() '''
		abstract class «elementClass» {
			// Intentionally left blank
		}
	'''
	
}
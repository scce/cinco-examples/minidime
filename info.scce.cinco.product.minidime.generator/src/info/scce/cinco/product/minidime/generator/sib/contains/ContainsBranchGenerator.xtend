package info.scce.cinco.product.minidime.generator.sib.contains

import info.scce.cinco.product.minidime.generator.MiniDIMEXtendTemplate
import info.scce.cinco.product.minidime.process.AbstractContainsSIB

import static extension info.scce.cinco.product.minidime.generator.helper.NameExtension.*

/**
 * @author Fabian Storek
 */
class ContainsBranchGenerator extends MiniDIMEXtendTemplate {
	
	val AbstractContainsSIB sib
	
	new(AbstractContainsSIB sib) {
		this.sib = sib
	}
	
	override packageName() {
		sib.containsBranchPackage
	}
	
	override className() {
		sib.containsBranchClass
	}
	
	override classTemplate() '''		
		abstract class «sib.containsBranchClass» extends «containsBranchFqn.shorten» {
			// Intentionally left blank
		}
		«FOR branch : sib.branchSuccessors»
			
			class «branch.containsBranchClass» extends «sib.containsBranchFqn.shorten» {
				// Intentionally left blank
			}
		«ENDFOR»
	'''
	
}

package info.scce.cinco.product.minidime.generator.common

import info.scce.cinco.product.minidime.generator.MiniDIMESibsTemplate

import static extension info.scce.cinco.product.minidime.generator.helper.NameExtension.*

/**
 * @author Fabian Storek
 */
class BooleanSibsGenerator extends MiniDIMESibsTemplate {
	
	val fqn = 'Boolean'.commonExtensionFqn
	
	override String packageName() {
		libraryName.commonSibPackage
	}
	
	override libraryName() {
		'Boolean'
	}
	
	override libraryTemplate() '''
		sib Print: «fqn»#print
			input: boolean
			-> success
		
		sib ToInteger: «fqn»#toLong
			input: boolean
			-> success
				result: integer
		
		sib ToReal: «fqn»#toDouble
			input: boolean
			-> success
				result: real
		
		sib ToString: «fqn»#toString
			input: boolean
			-> success
				result: text
		
		sib Not: «fqn»#not
			input: boolean
			-> success
				result: boolean
		
		sib Equal: «fqn»#equal
			input1: boolean
			input2: boolean
			-> success
				result: boolean
		
		sib NotEqual: «fqn»#notEqual
			input1: boolean
			input2: boolean
			-> success
				result: boolean
		
		sib And: «fqn»#and
			input1: boolean
			input2: boolean
			-> success
				result: boolean
		
		sib Or: «fqn»#or
			input1: boolean
			input2: boolean
			-> success
				result: boolean
		
		sib If: «fqn»#ifSwitch
			input: boolean
			-> true
			-> false
	'''
	
}

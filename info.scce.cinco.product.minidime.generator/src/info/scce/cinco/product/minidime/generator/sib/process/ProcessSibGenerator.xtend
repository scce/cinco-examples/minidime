package info.scce.cinco.product.minidime.generator.sib.process

import info.scce.cinco.product.minidime.generator.MiniDIMEXtendTemplate
import info.scce.cinco.product.minidime.process.Branch
import info.scce.cinco.product.minidime.process.DataFlowTarget
import info.scce.cinco.product.minidime.process.EndSIB
import info.scce.cinco.product.minidime.process.InputPort
import info.scce.cinco.product.minidime.process.InputStatic
import info.scce.cinco.product.minidime.process.OutputPort
import info.scce.cinco.product.minidime.process.SIB
import info.scce.cinco.product.minidime.process.StartSIB
import info.scce.cinco.product.minidime.process.Variable
import info.scce.cinco.product.minidime.process.helper.ProcessExtension.SwitchException
import java.util.List
import java.util.Map

import static extension info.scce.cinco.product.minidime.generator.helper.GeneratorExtension.*
import static extension info.scce.cinco.product.minidime.generator.helper.NameExtension.*
import info.scce.cinco.product.minidime.process.PrimitiveVariable
import info.scce.cinco.product.minidime.process.JavaNativeVariable
import info.scce.cinco.product.minidime.process.MiniDIMEProcess

/**
 * @author Fabian Storek
 */
class ProcessSibGenerator extends MiniDIMEXtendTemplate {
	
	val MiniDIMEProcess sib
	val List<OutputPort> ports
	val List<Variable> variables
	val List<Branch> branches
	val List<SIB> sibs
	val Map<Object, Integer> indices
	val DataFlowTarget firstSib
	
	new(MiniDIMEProcess sib) {
		this.sib		= sib
		this.ports		= sib.startSIBs.head.outputPorts
		this.variables	= sib.dataContexts.map[dc | dc.variables].flatten.toList
		this.branches	= sib.branchs.toList
		this.sibs		= sib.SIBs
		this.indices	= newHashMap
		this.firstSib	= sib.startSIBs.head.successors.head
	}
	
	override packageName() {
		sib.processSibPackage
	}
	
	override className() {
		sib.processSibClass
	}
	
	override classTemplate() {
		var Object successor
		'''
		class «sib.processSibClass» extends «processSibFqn.shorten» {
			«IF !ports.nullOrEmpty»
				
				// Ports
				«FOR port : ports»
					var «port.variableFqn.shorten» «port.localName»
				«ENDFOR»
			«ENDIF»
			«IF !variables.nullOrEmpty»
				
				// Variables
				«FOR variable : variables»
					var «variable.variableFqn.shorten» «variable.localName»
				«ENDFOR»
			«ENDIF»
			«IF !sibs.nullOrEmpty»
				
				// Sibs
				«FOR sib : sibs»
					var «sib.sibFqn.shorten» «sib.localName»
				«ENDFOR»
			«ENDIF»
			«IF !branches.nullOrEmpty»
				
				// Branches
				«FOR branch : branches»
					var «branch.branchFqn.shorten» «branch.localName»
				«ENDFOR»
			«ENDIF»
			
			// Execute method
			def «sib.processBranchFqn.shorten» execute(«FOR port : ports SEPARATOR ', '»«port.variableFqn.shorten» «port.variableName»«ENDFOR») {
				«IF !ports.nullOrEmpty»
					
					// Save ports
					«FOR port : ports»
						«port.localName» = «port.variableNameNullSafeList»
					«ENDFOR»
				«ENDIF»
				«IF !variables.nullOrEmpty»
					
					// Set initial variable values
					«FOR variable : variables»
						«IF variable.isList && !ports.exists[!isList && successors.contains(variable)]»
							«variable.assign»
						«ENDIF»
						«FOR port : ports.filter[successors.contains(variable)]»
							«variable.assign(port)»
						«ENDFOR»
					«ENDFOR»
				«ENDIF»
				
				// Execution loop
				var «elementFqn.shorten» current = «IF firstSib instanceof SIB»«firstSib.localName» = new «firstSib.sibFqn.shorten»«ELSEIF firstSib instanceof EndSIB»new «firstSib.branchFqn.shorten»«FOR input : firstSib.inputs BEFORE '(' SEPARATOR ', ' AFTER ')'»«input.localName»«ENDFOR»«ELSE»null«ENDIF»
				while (current instanceof «sibFqn.shorten») {
					current = current.step
				}
				return current as «sib.processBranchClass»
				
			}
			
			def «elementClass» step(«sibClass» sib) {
					
				switch sib {
					
					«FOR containedSib : sibs»
						«containedSib.sibFqn.shorten» case «containedSib.localName»: {
							switch branch : sib.execute«FOR input : containedSib.inputs BEFORE '(' SEPARATOR ', ' AFTER ')'»«input.localName»«ENDFOR» {
								«FOR branch : containedSib.branchSuccessors»
									«branch.branchFqn.shorten»: {
										«branch.localName» = branch
										«FOR port : branch.outputPorts»
											«FOR variable : port.successors.filter(Variable)»
												«variable.assign(port)»
											«ENDFOR»
										«ENDFOR»
										«{successor = branch.successors.head; ''}»
										«IF successor === null»
											throw new RuntimeException('The «branchClass» "«branch.branchName»" of «containedSib.sibClass» "«containedSib.sibName»" in Process "«sib.processSibName»" has no successor!')
										«ELSEIF successor instanceof SIB»
											if («successor.localName» === null) {
												«successor.localName» = new «successor.sibFqn.shorten»
											}
											return «successor.localName»
										«ELSEIF successor instanceof EndSIB»
											return new «successor.branchFqn.shorten»«FOR input : successor.inputs BEFORE '(' SEPARATOR ', ' AFTER ')'»«input.localName»«ENDFOR»
										«ENDIF»
									}
								«ENDFOR»
							}
						}
						
					«ENDFOR»
				}
				
			}
			
		}
		'''
	}
	
	def private dispatch String getLocalName(Branch branch) {
		'''branch«branch.index»_«branch.branchClass.toFirstLower»'''
	}
	
	def private dispatch String getLocalName(OutputPort port) {
		switch container : port.container {
			StartSIB:	'''port«port.index»_«port.variableName»'''
			Branch:		container.localName + '?.' + port.variableName
			default:	throw new SwitchException(container)
		}
	}
	
	def private dispatch String getLocalName(Variable variable) {
		'''var«variable.index»_«variable.variableName»'''
	}
	
	def private dispatch String getLocalName(InputStatic input) {
		input.variableValue
	}
	
	def private dispatch String getLocalName(InputPort input) {
		switch predecessor : input.predecessors.head {
			Variable:	predecessor.localName
			OutputPort:	predecessor.localName
			case null:	'null'
			default:	throw new SwitchException(predecessor)
		}
	}
	
	def private dispatch String getLocalName(SIB sib) {
		'''sib«sib.index»_«sib.sibClass.toFirstLower»'''.toString.escapeVariableName
	}
	
	def private int getIndex(Object o) {
		if (!indices.containsKey(o)) {
			indices.put(o, indices.size)
		}
		return indices.get(o)
	}
	
	def private String assign(Variable target) {
		target.assign(null)
	}

	def private String assign(Variable target, OutputPort source) {
		if (source === null) {
			'''«target.localName» = «target.defaultValue»'''
		}
		else if (target.isList && source.isList) {
			'''«target.localName» = «source.localName»'''
		}
		else if (target.isList /* && !source.isList */) {
			'''
				if («source.localName» !== null) {
					«target.localName» += «source.localName»
				}
			'''
		}
		else {
			'''«target.localName» = «source.localName»'''
		}
	}
	
	def private dispatch String defaultValue(PrimitiveVariable it) {
		if (isList) return 'newArrayList'
		switch dataType {
			case BOOLEAN: 'false'
			case INTEGER: '0L'
			case REAL:    '0.0D'
			case TEXT:    "''"
		}
	}
	
	def private dispatch String defaultValue(JavaNativeVariable it) {
		if (isList) return 'newArrayList'
		'''new «variableFqn.shorten»'''
	}
	
}

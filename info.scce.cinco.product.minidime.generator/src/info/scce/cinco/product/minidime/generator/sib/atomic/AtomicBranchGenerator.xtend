package info.scce.cinco.product.minidime.generator.sib.atomic

import info.scce.cinco.product.minidime.generator.MiniDIMEXtendTemplate
import info.scce.cinco.product.minidime.siblibrary.SIB

import static extension info.scce.cinco.product.minidime.generator.helper.NameExtension.*

/**
 * @author Fabian Storek
 */
class AtomicBranchGenerator extends MiniDIMEXtendTemplate {
	
	val SIB sib
	
	new(SIB sib) {
		this.sib = sib
	}
	
	override packageName() {
		sib.atomicBranchPackage
	}
	
	override className() {
		sib.atomicBranchClass
	}
	
	override classTemplate() '''		
		abstract class «sib.atomicBranchClass» extends «atomicBranchFqn.shorten» {
			// Intentionally left blank
		}
		«FOR branch : sib.branches»
			
			«dataAnnotation»
			class «branch.atomicBranchClass» extends «sib.atomicBranchFqn.shorten» {
				«IF branch.outputPorts.nullOrEmpty»
					// Intentionally left blank
				«ELSE»
					
					// Variables
					«FOR port : branch.outputPorts»
						val «port.variableFqn.shorten» «port.variableName»
					«ENDFOR»
					
					// Constructors
					new(«FOR port : branch.outputPorts SEPARATOR ', '»«port.variableFqn.shorten» «port.variableName»«ENDFOR») {
						«FOR port : branch.outputPorts»
							this.«port.variableName» = «port.variableName»
						«ENDFOR»
					}
					
					new(«branch.outputPorts.size.tupleFqn.shorten»«FOR port : branch.outputPorts BEFORE '<' SEPARATOR ', ' AFTER '>'»«port.variableFqn.shorten»«ENDFOR» «tupleClass.toFirstLower») {
						«FOR i : 0 ..< branch.outputPorts.size»
							this.«branch.outputPorts.get(i).variableName» = «tupleClass.toFirstLower».element«i»
						«ENDFOR»
					}
					
				«ENDIF»
			}
		«ENDFOR»
	'''
	
}

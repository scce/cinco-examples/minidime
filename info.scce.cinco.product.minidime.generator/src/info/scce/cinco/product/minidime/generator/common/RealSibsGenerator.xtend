package info.scce.cinco.product.minidime.generator.common

import info.scce.cinco.product.minidime.generator.MiniDIMESibsTemplate

import static extension info.scce.cinco.product.minidime.generator.helper.NameExtension.*

/**
 * @author Fabian Storek
 */
class RealSibsGenerator extends MiniDIMESibsTemplate {
	
	val fqn = 'Double'.commonExtensionFqn
	
	override String packageName() {
		libraryName.commonSibPackage
	}
	
	override libraryName() {
		'Real'
	}
	
	override libraryTemplate() '''
		sib Print: «fqn»#print
			input: real
			-> success
		
		sib ToInteger: «fqn»#toLong
			input: real
			-> success
				result: integer
		
		sib ToBoolean: «fqn»#toBoolean
			input: real
			-> success
				result: boolean
		
		sib ToText: «fqn»#toString
			input: real
			-> success
				result: text
		
		sib Equal: «fqn»#equal
			input1: real
			input2: real
			-> success
				result: boolean
		
		sib NotEqual: «fqn»#notEqual
			input1: real
			input2: real
			-> success
				result: boolean
		
		sib Less: «fqn»#less
			input1: real
			input2: real
			-> success
				result: boolean
		
		sib LessOrEqual: «fqn»#lessOrEqual
			input1: real
			input2: real
			-> success
				result: boolean
		
		sib Greater: «fqn»#greater
			input1: real
			input2: real
			-> success
				result: boolean
		
		sib GreaterOrEqual: «fqn»#greaterOrEqual
			input1: real
			input2: real
			-> success
				result: boolean
		
		sib Add: «fqn»#add
			input1: real
			input2: real
			-> success
				result: real
		
		sib Subtract: «fqn»#subtract
			input1: real
			input2: real
			-> success
				result: real
		
		sib Multiply: «fqn»#multiply
			input1: real
			input2: real
			-> success
				result: real
		
		sib Divide: «fqn»#divide
			input1: real
			input2: real
			-> success
				result: real
	'''
	
}

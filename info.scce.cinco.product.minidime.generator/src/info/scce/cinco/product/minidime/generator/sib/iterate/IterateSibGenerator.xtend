package info.scce.cinco.product.minidime.generator.sib.iterate

import info.scce.cinco.product.minidime.generator.MiniDIMEXtendTemplate
import info.scce.cinco.product.minidime.process.AbstractIterateSIB
import info.scce.cinco.product.minidime.process.Input

import static extension info.scce.cinco.product.minidime.generator.helper.NameExtension.*

/**
 * @author Fabian Storek
 */
class IterateSibGenerator extends MiniDIMEXtendTemplate {
	
	val AbstractIterateSIB sib
	val Input list
	
	new(AbstractIterateSIB sib) {
		this.sib  = sib
		this.list = sib.inputs.head
	}
	
	override packageName() {
		sib.iterateSibPackage
	}
	
	override className() {
		sib.iterateSibClass
	}
	
	override classTemplate() '''		
		class «sib.iterateSibClass» extends «iterateSibFqn.shorten» {
			
			// Variables
			var «shorten('java.util.Iterator')»<«list.variableClass»> iterator
			
			// Execute method
			def «sib.iterateBranchFqn.shorten» execute(«list.variableFqn.shorten» «list.variableName») {
				if (iterator === null) {
					if («list.variableName».nullOrEmpty) return new «sib.exitIterateBranch.iterateBranchFqn.shorten»
					iterator = «list.variableName».iterator
				}
				if (iterator.hasNext) {
					return new «sib.nextIterateBranch.iterateBranchFqn.shorten»(iterator.next)
				}
				else {
					iterator = null
					return new «sib.exitIterateBranch.iterateBranchFqn.shorten»
				}
			}
			
		}
	'''
	
}

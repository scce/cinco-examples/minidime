package info.scce.cinco.product.minidime.generator

import de.jabc.cinco.meta.plugin.template.FileTemplate

/**
 * @author Fabian Storek
 */
abstract class MiniDIMEFileTemplate extends FileTemplate {
	
	override final getTargetFileName() {
		fileName
	}
	
	override final template() '''
		// Generated by «this.class.name»
			
		«fileTemplate»
	'''
	
	def String fileName()
	
	def String fileTemplate()
	
}

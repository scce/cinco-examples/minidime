package info.scce.cinco.product.minidime.generator

import de.jabc.cinco.meta.plugin.generator.runtime.IGenerator
import info.scce.cinco.product.minidime.siblibrary.helper.ClassLoaderExtension
import org.eclipse.core.runtime.IPath
import org.eclipse.core.runtime.IProgressMonitor

import static extension info.scce.cinco.product.minidime.generator.helper.NameExtension.*
import info.scce.cinco.product.minidime.process.MiniDIMEProcess

/**
 * @author Fabian Storek
 */
class MiniDIMEGenerator implements IGenerator<MiniDIMEProcess> {
		
	extension ClassLoaderExtension = new ClassLoaderExtension
	
	override void generate(MiniDIMEProcess process, IPath outlet, IProgressMonitor monitor) {
		
		// For debugging:
		// MiniDIMEProjectCleanup deletes the contents of the src-gen and common folders.
		// This allows the following if-statement to run the MiniDIMEProjectInitializer.
//		new MiniDIMEProjectCleanup(process).createProject
//		Thread.sleep(3000)
		
		val fqns = #[tupleFqn] + #['Boolean', 'Long', 'Double', 'String'].map[commonExtensionFqn]
		refreshClassLoaders
		
		if (fqns.exists[!classExists]) {
			
			new MiniDIMEProjectInitializer(process).createProject
			
			// Sleeping for 0.2 sec, so the compiler can compile the newly generated sources.
			// If we would not wait here, the Xtext validator of the SIBLibrary language
			// starts validating before the necessary classes are compiled. This results in
			// unnecessary error messages in SIBLibrary sources.
			Thread.sleep(200)
			
		}
		
		new MiniDIMEProjectGenerator(process).createProject
		
	}
	
}

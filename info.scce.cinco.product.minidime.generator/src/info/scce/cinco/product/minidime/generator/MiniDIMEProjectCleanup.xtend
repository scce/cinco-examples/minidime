package info.scce.cinco.product.minidime.generator

import de.jabc.cinco.meta.plugin.template.ProjectTemplate

import static info.scce.cinco.product.minidime.generator.helper.NameExtension.*

import static extension de.jabc.cinco.meta.core.utils.projects.ProjectCreator.getProject
import info.scce.cinco.product.minidime.process.MiniDIMEProcess

/**
 * @author Fabian Storek
 */
class MiniDIMEProjectCleanup extends ProjectTemplate {
	
	val MiniDIMEProcess process
		
	new(MiniDIMEProcess process) {
		this.process = process
	}
	
	override projectDescription() {
		
		project(process.eResource.project.name) [
			
			deleteIfExistent = false
			
			natures = #['org.eclipse.xtext.ui.shared.xtextNature']
			
			requiredBundles = #[
				'com.google.guava',
				'org.eclipse.xtext.xbase.lib',
				'org.eclipse.xtend.lib',
				'org.eclipse.xtend.lib.macro'
			]
			
			folder(commonSibPackage) [
				deleteIfExistent = true
				isSourceFolder = false
			]
			
			folder('src') [
				deleteIfExistent = false
				isSourceFolder = true
			]
			
			folder('src-gen') [
				deleteIfExistent = true
				isSourceFolder = true
			]
			
		]
		
	}
	
	override projectSuffix() {
		throw new UnsupportedOperationException("TODO: auto-generated method stub")
	}
	
}

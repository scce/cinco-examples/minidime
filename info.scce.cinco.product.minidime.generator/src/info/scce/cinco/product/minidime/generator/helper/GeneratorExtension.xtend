package info.scce.cinco.product.minidime.generator.helper

import java.util.HashSet

import static extension java.lang.Character.getName
import static extension java.lang.Character.isJavaIdentifierPart
import static extension java.lang.Character.isWhitespace
import info.scce.cinco.product.minidime.process.MiniDIMEProcess

/**
 * @author Fabian Storek
 */
class GeneratorExtension {
		
	/**
	 * Copied from info.scce.dime.generator.util.JavaIdentifierUtils#javaKeywords - Complete
	 */
	val static javaKeywords = #{
		"abstract", "assert", "boolean", "break", "byte", "case", "catch",
		"char", "class", "const", "continue", "default", "do", "double",
		"else", "enum", "extends", "false", "final", "finally", "float", "for",
		"goto", "if", "implements", "import","var", "instanceof", "int",
		"interface", "long", "native", "new", "null", "package", "private",
		"protected", "public", "return", "short", "static", "strictfp",
		"super", "switch", "synchronized", "this", "throw", "throws",
		"transient", "true", "try", "void", "volatile", "while"
	}
	
	/**
	 * Copied from info.scce.dime.generator.util.JavaIdentifierUtils#pkgEscape(String) - Complete
	 */
	def static String escapePackage(String source) {
		if (source === null) return null
		source.split('\\.').map[escapeJava].join('.')
	}
	
	/**
	 * Copied from info.scce.dime.generator.util.JavaIdentifierUtils#escapeJava(String) - Complete
	 */
	def static String escapeJava(String source) {
		if (source === null) return null
		if (source.empty) return ''
		if (javaKeywords.contains(source)) {
			return '''_«source»'''
		}
		else {
			val sourceAsArray = source.toCharArray
			val result = new StringBuilder
			if (!Character.isJavaIdentifierStart(sourceAsArray.head)) {
				result.append('_')
			}
			for (c : sourceAsArray) {
				result.append(
					switch c {
						case c.isJavaIdentifierPart: c
						case c.isWhitespace:         '_'
						case c == 45:                '__HYPHEN_MINUS__'
						default:                     '''__«escapeJava(c.name)»__'''
					}
				)
			}
			return result.toString
		}
	}
	
	/**
	 * Copied from info.scce.dime.generator.util.JavaIdentifierUtils#escapeString(String) - Complete
	 */
	def static String escapeString(String source) {
		if (source === null) return null
		return source.replace('\\', '\\\\')	// Backslashes
		             .replace('"',  '\\"')	// Quotes
	}
	
	/**
	 * Copied from info.scce.dime.generator.util.JavaIdentifierUtils#trimNL(String) - Complete
	 */
	def static String trimNL(String source) {
		if (source === null) return null
		return source.replaceAll('\n', '')
	}
	
	
	
	/*** Utils ***/
	
	/**
	 * @author Fabian Storek
	 */
	def static String escapeXtendString(String source) {
		if (source === null) return null
		return source.replace('\\', '\\\\')	// Backslashes
		             .replace("'",  "\\'")	// Quotes
	}
	
	/**
	 * @author Fabian Storek
	 */
	def static String escapePackageName(String name) {
		if (name === null) return null
		return name.trimNL.trim.toFirstLower.escapePackage
	}
	
	/**
	 * @author Fabian Storek
	 */
	def static String escapeClassName(String name) {
		if (name === null) return null
		return name.trimNL.trim.toFirstUpper.escapeJava
	}
	
	/**
	 * @author Fabian Storek
	 */
	def static String escapeVariableName(String name) {
		if (name === null) return null
		return name.trimNL.trim.toFirstLower.escapeJava
	}
	
	/**
	 * @return A set of all descendant processes and the {@code rootProcess} itself.
	 * @author Fabian Storek
	 */
	def static HashSet<MiniDIMEProcess> getProcessDescendants(MiniDIMEProcess rootProcess) {
		val descendants = new HashSet<MiniDIMEProcess>
		rootProcess.getProcessDescendantsRecursive(descendants)
		return descendants
	}
	
	def private static void getProcessDescendantsRecursive(MiniDIMEProcess rootProcess, HashSet<MiniDIMEProcess> alreadyFound) {
		alreadyFound.add(rootProcess)
		rootProcess.processSIBs
		           .map[proMod]
		           .filter[!alreadyFound.contains(it)]
		           .forEach[getProcessDescendantsRecursive(alreadyFound)]
	}
	
}

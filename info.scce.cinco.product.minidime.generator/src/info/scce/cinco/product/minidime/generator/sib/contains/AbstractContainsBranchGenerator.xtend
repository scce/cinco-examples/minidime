package info.scce.cinco.product.minidime.generator.sib.contains

import info.scce.cinco.product.minidime.generator.MiniDIMEXtendTemplate

import static info.scce.cinco.product.minidime.generator.helper.NameExtension.*

/**
 * @author Fabian Storek
 */
class AbstractContainsBranchGenerator extends MiniDIMEXtendTemplate {
	
	override packageName() {
		containsBranchPackage
	}
	
	override className() {
		containsBranchClass
	}
	
	override classTemplate() '''		
		abstract class «containsBranchClass» extends «branchFqn.shorten» {
			// Intentionally left blank
		}
	'''
	
}

package info.scce.cinco.product.minidime.generator.sib.atomic

import info.scce.cinco.product.minidime.generator.MiniDIMEXtendTemplate
import info.scce.cinco.product.minidime.siblibrary.Branch
import info.scce.cinco.product.minidime.siblibrary.Port
import info.scce.cinco.product.minidime.siblibrary.SIB
import java.util.List

import static extension info.scce.cinco.product.minidime.generator.helper.NameExtension.*

/**
 * @author Fabian Storek
 */
class AtomicSibGenerator extends MiniDIMEXtendTemplate {
	
	val static printStackTrace = false
	
	val SIB sib
	
	val Branch successBranch
	val Branch failureBranch
	val Branch noresultBranch
	val Branch trueBranch
	val Branch falseBranch
	
	val List<Port> successBranchPorts
	
	val boolean returnsVoid
	val boolean returnsValue
	val boolean returnsTuple
	val boolean returnsBoolean
	val boolean returnsNoResult
	val boolean returnsFailure
	
	new(SIB sib) {
		
		this.sib = sib
		
		this.successBranch      = sib.successAtomicBranch
		this.failureBranch      = sib.failureAtomicBranch
		this.noresultBranch     = sib.noresultAtomicBranch
		this.trueBranch         = sib.trueAtomicBranch
		this.falseBranch        = sib.falseAtomicBranch
		
		this.successBranchPorts = successBranch?.outputPorts?.toList
		
		this.returnsVoid        = successBranch  !== null && successBranchPorts.nullOrEmpty
		this.returnsValue       = successBranch  !== null && successBranchPorts.size == 1
		this.returnsTuple       = successBranch  !== null && successBranchPorts.size > 1
		this.returnsBoolean     = successBranch  === null && trueBranch !== null && falseBranch !== null
		this.returnsNoResult    = noresultBranch !== null
		this.returnsFailure     = failureBranch  !== null
		
	}
	
	override packageName() {
		sib.atomicSibPackage
	}
	
	override className() {
		sib.atomicSibClass
	}
	
	override classTemplate() '''		
		class «sib.atomicSibClass» extends «atomicSibFqn.shorten» {
			
			// Execute method
			def «sib.atomicBranchFqn.shorten» execute(«FOR port : sib.inputPorts SEPARATOR ', '»«port.variableFqn.shorten» «port.variableName»«ENDFOR») {
				«IF returnsVoid»
					«IF returnsFailure»
						try «executorCall»
						catch (Exception e) {
							«IF printStackTrace»e.printStackTrace«ENDIF»
							return new «failureBranch.atomicBranchFqn.shorten»
						}
					«ELSE»
						«executorCall»
					«ENDIF»
					return new «successBranch.atomicBranchFqn.shorten»
				«ELSE»
					«IF returnsFailure»
						val result = try «executorCall»
						catch (Exception e) {
							«IF printStackTrace»e.printStackTrace«ENDIF»
							return new «failureBranch.atomicBranchFqn.shorten»
						}
					«ELSE»
						val result = «executorCall»
					«ENDIF»
					«IF returnsNoResult»
						if (result as Object === null) { // Cast to Object in case result is a primitive Java type
							return new «noresultBranch.atomicBranchFqn.shorten»
						}
					«ENDIF»
					«IF returnsValue»
						return new «successBranch.atomicBranchFqn.shorten»(result)
					«ELSEIF returnsTuple»
						return new «successBranch.atomicBranchFqn.shorten»«FOR i : 0 ..< successBranchPorts.size BEFORE '(\n\t' SEPARATOR ',\n\t' AFTER '\n)'»result.get(«i») as «successBranchPorts.get(i).variableFqn.shorten»«ENDFOR»
					«ELSEIF returnsBoolean»
						if (result) {
							return new «trueBranch.atomicBranchFqn.shorten»
						}
						else {
							return new «falseBranch.atomicBranchFqn.shorten»
						}
					«ENDIF»
				«ENDIF»
			}
			
		}
	'''
	
	def executorCall() {
		'''«sib.executorClass.shorten».«sib.executorMethod»«FOR port : sib.inputPorts BEFORE '(' SEPARATOR ', ' AFTER ')'»«port.variableNameNullSafeList»«ENDFOR»'''
	}
	
}

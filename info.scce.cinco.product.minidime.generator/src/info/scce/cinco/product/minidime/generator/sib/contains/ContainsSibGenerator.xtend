package info.scce.cinco.product.minidime.generator.sib.contains

import info.scce.cinco.product.minidime.generator.MiniDIMEXtendTemplate
import info.scce.cinco.product.minidime.process.AbstractContainsSIB
import info.scce.cinco.product.minidime.process.Input

import static extension info.scce.cinco.product.minidime.generator.helper.NameExtension.*

/**
 * @author Fabian Storek
 */
class ContainsSibGenerator extends MiniDIMEXtendTemplate {
	
	val AbstractContainsSIB sib
	val Input list
	val Input element
	
	new(AbstractContainsSIB sib) {
		this.sib = sib
		this.list = sib.inputs.findFirst[name == 'list']
		this.element = sib.inputs.findFirst[name == 'element']
	}
	
	override packageName() {
		sib.containsSibPackage
	}
	
	override className() {
		sib.containsSibClass
	}
	
	override classTemplate() '''		
		class «sib.containsSibClass» extends «containsSibFqn.shorten» {
			
			// Execute method
			def «sib.containsBranchFqn.shorten» execute(«list.variableFqn.shorten» «list.variableName», «element.variableFqn.shorten» «element.variableName») {
				if (!«list.variableName».nullOrEmpty && «element.variableName» !== null && «list.variableName».contains(«element.variableName»)) {
					return new «sib.yesContainsBranch.containsBranchFqn.shorten»
				}
				else {
					return new «sib.noContainsBranch.containsBranchFqn.shorten»
				}
			}
			
		}
	'''
	
}

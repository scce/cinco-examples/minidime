package info.scce.cinco.product.minidime.generator.common

import info.scce.cinco.product.minidime.generator.MiniDIMESibsTemplate

import static extension info.scce.cinco.product.minidime.generator.helper.NameExtension.*

/**
 * @author Fabian Storek
 */
class TextSibsGenerator extends MiniDIMESibsTemplate {
	
	val fqn = 'String'.commonExtensionFqn
	
	override String packageName() {
		libraryName.commonSibPackage
	}
	
	override libraryName() {
		'Text'
	}
	
	override libraryTemplate() '''
		sib Print: «fqn»#print
			input: text
			-> success
		
		sib ToInteger: «fqn»#toLong
			input: text
			-> success
				result: integer
			-> failure
		
		sib ToReal: «fqn»#toDouble
			input: text
			-> success
				result: real
			-> failure
		
		sib ToBoolean: «fqn»#toBoolean
			input: text
			-> success
				result: boolean
			-> failure
		
		sib Equal: «fqn»#equal
			input1: text
			input2: text
			-> success
				result: boolean
		
		sib NotEqual: «fqn»#notEqual
			input1: text
			input2: text
			-> success
				result: boolean
		
		sib Append: «fqn»#append
			input1: text
			input2: text
			-> success
				result: text
		
		sib ListOfCharacters: «fqn»#listOfCharacters
			input: text
			-> success
				result: [text]
		
		sib ListOfWords: «fqn»#listOfWords
			input: text
			-> success
				result: [text]
	'''
	
}

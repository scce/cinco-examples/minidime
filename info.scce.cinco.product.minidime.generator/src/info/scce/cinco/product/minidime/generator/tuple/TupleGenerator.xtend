package info.scce.cinco.product.minidime.generator.tuple

import info.scce.cinco.product.minidime.generator.MiniDIMEXtendTemplate

import static extension info.scce.cinco.product.minidime.generator.helper.NameExtension.*

/**
 * @author Fabian Storek
 */
class TupleGenerator extends MiniDIMEXtendTemplate {
	
	val int maxDim
	
	new(int maxDim) {
		this.maxDim = maxDim
	}
	
	override packageName() {
		tuplePackage
	}
	
	override className() {
		tupleClass
	}
	
	override classTemplate() '''
		/**
		 * An abstract super class for tuples. A tuple is a finite ordered list of elements.
		 * <p>
		 * The largest tuple is {@linkplain «maxDim.tupleClass»} with a dimensionality of «maxDim».
		 * Larger tuples will be generated as needed. (Depends on the largest number of ports in an
		 * {@linkplain «atomicSibFqn» «atomicSibClass»} or its {@linkplain «atomicBranchFqn» branches}.)
		 */
		abstract class «tupleClass» implements Iterable<Object> {
			
			/**
			 * Returns the dimensionality of this tuple.
			 */
			def int getDim()
			
			/**
			 * Returns the element at the provided <code>index</code>.
			 * @throws IndexOutOfBoundsException if the <code>index</code> is not in
			 *         the range <code>0 ..< </code>{@link #getDim() getDim()}.
			 */
			def get(int index) {
				switch it : this {
					«FOR i : 1 .. maxDim»
						«i.tupleClass»«i.csvAngle['?']» case index == «i - 1»: element«i - 1»
					«ENDFOR»
					default: throw new IndexOutOfBoundsException
				}
			}
			
			/**
			 * Indicates whether some other tuple is "equal to" this one. A tuple is
			 * equal to another tuple, iff all of their elements are pairwise equal to
			 * each other.
			 * @see Object#equals(Object) Object.equals(Object)
			 */
			override equals(Object other) {
				switch other {
					case null: {
						return false
					}
					case this: {
						return true
					}
					Tuple case this.dim == other.dim: {
						for (i : 0 ..< dim) {
							if (this.get(i) != other.get(i)) {
								return false
							}
						}
						return true
					}
					default: {
						return false
					}
				}
			}
			
			/**
			 * Returns a string representation of the tuple.
			 * @see Object#toString() Object.toString()
			 */
			override toString() {
				join('(', ', ', ')') [toString]
			}
			
			/**
			 * Returns an iterator over the elements of this tuple.
			 * @see Iterable#iterator() Iterable.iterator()
			 */
			override iterator() {
				new «tupleClass»Iterator(this)
			}
			
			/**
			 * An {@linkplain «shorten('java.util.Iterator')»} over a {@linkplain «tupleClass»}.
			 */
			static class «tupleClass»Iterator implements «shorten('java.util.Iterator')»<Object> {
				
				/**
				 * The tuple over which to iterate.
				 */
				val «tupleClass» «tupleClass.toFirstLower»
				
				/**
				 * The index of the current element. If this value equals -1, then no element
				 * has been returned yet.
				 */
				var int currentIndex
				
				/**
				 * Creates a new iterator for the specified <code>«tupleClass.toFirstLower»</code>.
				 */
				new(«tupleClass» «tupleClass.toFirstLower») {
					this.«tupleClass.toFirstLower» = «tupleClass.toFirstLower»
					this.currentIndex = -1
				}
				
				/**
				 * Returns {@code true} if the iteration has more elements.
				 * @ see java.util.Iterator#hasNext() Iterator.hasNext()
				 */
				override hasNext() {
					currentIndex < «tupleClass.toFirstLower».dim - 1
				}
				
				/**
			     * Returns the next element in the iteration.
			     * @throws NoSuchElementException if the iteration has no more elements.
			     * @see java.util.Iterator#next() Iterator.next()
			     */
				override next() {
					if (hasNext) {
						currentIndex += 1
						return «tupleClass.toFirstLower».get(currentIndex)
					}
					else {
						throw new «shorten('java.util.NoSuchElementException')»
					}
				}
				
			}
			
		}
		«FOR i : 1 .. maxDim»
			«val j = i - 1»
			
			/**
			 * A concrete class for tuples with a dimensionality of «i».
			 */
			«accessorsAnnotation()»
			class «i.tupleClass»«i.csvAngle['''T«it»''']» extends «j.tupleClass»«j.csvAngle['''T«it»''']» {
				
				/**
				 * The «i.ordinal» element (element with index «j») of this tuple.
				 */
				val T«j» element«j»
				
				/**
				 * Creates a new tuple with dimensionality «i».
				 */
				new(«i.csv['''T«it» element«it»''']») {
					super(«j.csv['''element«it»''']»)
					this.element«j» = element«j»
				}
				
				/**
				 * Returns the dimensionality of this tuple («i»).
				 * @see «tupleClass»#getDim() «tupleClass».getDim()
				 */
				override getDim() {
					«i»
				}
				
			}
		«ENDFOR»
	'''
	
	/**
	 * Comma separated values. Applies <code>func</code> to integers
	 * <code>0 ..&lt; count</code> and concatenates the results with commas.
	 */
	def private static csv(int count, (int) => CharSequence func) {
		(0 ..< count).join(', ', func)
	}
	
	/**
	 * Comma separated values with angle brackets. Applies <code>func</code> to
	 * integers <code>0 ..&lt; count</code>, concatenates the results with
	 * commas and wraps it in angle brackets.
	 */
	def private static csvAngle(int count, (int) => CharSequence func) {
		(0 ..< count).join('<', ', ', '>', func)
	}
	
	/**
	 * Returns a string representation of the ordinal of <code>num</code>.
	 */
	def private static ordinal(int num) {
		val suffix = switch num % 10 {
			case 1:  'st'
			case 2:  'nd'
			case 3:  'rd'
			default: 'th'
		}
		return '''«num»«suffix»'''
	}
	
}

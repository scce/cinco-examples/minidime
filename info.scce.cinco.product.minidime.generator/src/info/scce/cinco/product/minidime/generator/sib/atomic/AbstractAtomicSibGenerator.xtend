package info.scce.cinco.product.minidime.generator.sib.atomic

import info.scce.cinco.product.minidime.generator.MiniDIMEXtendTemplate

import static info.scce.cinco.product.minidime.generator.helper.NameExtension.*

/**
 * @author Fabian Storek
 */
class AbstractAtomicSibGenerator extends MiniDIMEXtendTemplate {
	
	override packageName() {
		atomicSibPackage
	}
	
	override className() {
		atomicSibClass
	}
	
	override classTemplate() '''		
		abstract class «atomicSibClass» extends «sibFqn.shorten» {
			// Intentionally left blank
		}
	'''
	
}

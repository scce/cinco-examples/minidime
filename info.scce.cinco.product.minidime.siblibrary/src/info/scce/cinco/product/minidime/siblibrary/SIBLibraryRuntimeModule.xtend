/*
 * generated by Xtext 2.14.0
 */
package info.scce.cinco.product.minidime.siblibrary


/**
 * Use this class to register components to be used at runtime / without the Equinox extension registry.
 */
class SIBLibraryRuntimeModule extends AbstractSIBLibraryRuntimeModule {
}

package info.scce.cinco.product.minidime.siblibrary.templates;

import info.scce.cinco.product.minidime.siblibrary.SIB;
import info.scce.cinco.product.minidime.siblibrary.impl.SIBImpl;

/**
 * @deprecated This class acts as a template for the {@link SIB#getId()} method.
 */
public class SIBGetIDTemplate extends SIBImpl {

	/**
	 * @deprecated This method acts as a template for the {@link SIB#getId()} method.
	 */
	public String getId() {
		org.eclipse.emf.common.util.EList<org.eclipse.emf.ecore.EObject> siblings = eContainer().eContents();
		for (org.eclipse.emf.ecore.EObject sibling: siblings) {
			if (sibling instanceof info.scce.cinco.product.minidime.siblibrary.Package) {
				info.scce.cinco.product.minidime.siblibrary.Package packageInstance = (info.scce.cinco.product.minidime.siblibrary.Package)sibling;
				String packageName = packageInstance.getName();
				return packageName + ".sib." + getName();
			}
		}
		return getName();
	}
	
//	public static String test(SIB sib) {
//		return sib.getId();
//	}
	
}

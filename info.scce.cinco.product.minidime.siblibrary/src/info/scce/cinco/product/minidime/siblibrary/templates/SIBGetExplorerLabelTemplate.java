package info.scce.cinco.product.minidime.siblibrary.templates;

import info.scce.cinco.product.minidime.siblibrary.SIB;
import info.scce.cinco.product.minidime.siblibrary.impl.SIBImpl;

/**
 * @deprecated This class acts as a template for the {@link SIB#getExplorerLabel()} method.
 */
public class SIBGetExplorerLabelTemplate extends SIBImpl {

	/**
	 * @deprecated This method acts as a template for the {@link SIB#getExplorerLabel()} method.
	 */
	public String getExplorerLabel() {
		return "sib " + getName();
	}
	
	/**
	 * @deprecated This method is for testing purposes only.
	 */
	public static String test(SIB sib) {
		return sib.getExplorerLabel();
	}
	
}

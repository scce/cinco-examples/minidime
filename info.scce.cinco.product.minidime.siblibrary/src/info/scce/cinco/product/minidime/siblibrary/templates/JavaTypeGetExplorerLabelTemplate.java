package info.scce.cinco.product.minidime.siblibrary.templates;

import info.scce.cinco.product.minidime.siblibrary.JavaType;
import info.scce.cinco.product.minidime.siblibrary.impl.JavaTypeImpl;

/**
 * @deprecated This class acts as a template for the {@link JavaType#getExplorerLabel()} method.
 */
public class JavaTypeGetExplorerLabelTemplate extends JavaTypeImpl {

	/**
	 * @deprecated This method acts as a template for the {@link JavaType#getExplorerLabel()} method.
	 */
	public String getExplorerLabel() {
		return "javaType " + getName();
	}
	
	/**
	 * @deprecated This method is for testing purposes only.
	 */
	public static String test(JavaType type) {
		return type.getExplorerLabel();
	}
	
}

package info.scce.cinco.product.minidime.siblibrary.helper

import info.scce.cinco.product.minidime.siblibrary.Branch
import info.scce.cinco.product.minidime.siblibrary.JavaType
import info.scce.cinco.product.minidime.siblibrary.JavaTypePort
import info.scce.cinco.product.minidime.siblibrary.PrimitivePort
import info.scce.cinco.product.minidime.siblibrary.PrimitiveType
import info.scce.cinco.product.minidime.siblibrary.SIB
import java.util.List
import java.util.regex.Pattern
import org.eclipse.xtext.xbase.lib.Functions.Function2

/**
 * @author Fabian Storek
 */
class SIBLibraryExtension extends ClassLoaderExtension {
	
	/**
	 * Returns the FQN of the Tuple superclass.
	 */
	def String getTupleFqn() {
		'tuple.Tuple'
	}
	
	/**
	 * Returns the FQN of the Tuple subclass with {@code i} components.
	 */
	def String getTupleFqn(int i) {
		getTupleFqn + if(i > 0) i else ''
	}
	
	/**
	 * Returns the {@link Class} of the Tuple superclass.
	 */
	def Class<?> getTupleClass() {
		0.tupleClass
	}

	/**
	 * Returns the {@link Class} of the Tuple subclass with {@code i} components.
	 */
	def Class<?> getTupleClass(int i) {
		try i.tupleFqn.loadClass
		catch (ClassNotFoundException e) null
	}
	
	/**
	 * Returns the {@link Branch} with the corresponding {@code branchName}.
	 */
	def Branch branch(SIB sib, String branchName) {
		return sib.branches.findFirst[name.toLowerCase == branchName.toLowerCase]
	}
	
	/**
	 * Returns the FQN of the type of this port.
	 */
	def dispatch String getVariableFqn(PrimitivePort it) {
		if (isList) '''java.util.List<«type.variableFqn»>''' else type.variableFqn
	}
	
	/**
	 * Returns the FQN of the type of this port.
	 */
	def dispatch String getVariableFqn(JavaTypePort it) {
		if (isList) '''java.util.List<«type.variableFqn»>''' else type.variableFqn
	}
	
	/**
	 * Returns the FQN of this type.
	 */
	def dispatch String getVariableFqn(JavaType it) {
		fqn
	}
	
	/**
	 * Returns the FQN of this type.
	 */
	def dispatch String getVariableFqn(PrimitiveType it) {
		switch it {
			case TEXT:    'java.lang.String'
			case INTEGER: 'java.lang.Long'
			case REAL:    'java.lang.Double'
			case BOOLEAN: 'java.lang.Boolean'
		}
	}
	
	/**
	 * Returns the {@link Class} of the type of this port.
	 */
	def dispatch Class<?> getVariableClass(PrimitivePort it) {
		if (isList) List else type.variableClass
	}
	
	/**
	 * Returns the {@link Class} of the type of this port.
	 */
	def dispatch Class<?> getVariableClass(JavaTypePort it) {
		if (isList) List else type.variableClass
	}
	
	/**
	 * Returns the {@link Class} of this type.
	 */
	def dispatch Class<?> getVariableClass(JavaType it) {
		fqn.loadClass
	}
	
	/**
	 * Returns the {@link Class} of this type.
	 */
	def dispatch Class<?> getVariableClass(PrimitiveType it) {
		switch it {
			case TEXT:    String
			case INTEGER: Long
			case REAL:    Double
			case BOOLEAN: Boolean
		}
	}
	
	/**
	 * Returns {@code true}, if {@code left} is a sub-class of or equal to {@code right}.
	 */
	def boolean isSubClassOf(Class<?> left, Class<?> right) {
		if (left === null || right === null) {
			return false
		}
		if (left == right) {
			return true
		}
		try {
			left.asSubclass(right)
			return true
		}
		catch (ClassCastException e) {
			return false
		}
	}
	
	/**
	 * Returns {@code true}, if {@code left} is a super-class of or equal to {@code right}.
	 */
	def boolean isSuperClassOf(Class<?> left, Class<?> right) {
		right.isSubClassOf(left)
	}
	
	/**
	 * Returns {@code true}, if {@code left} is compatible with {@code right}.
	 * <p>
	 * This means
	 * <ul>
	 *     <li>{@code left} is a sub-class of {@code right} or</li>
	 *     <li>{@code left} is equal to {@code right} or</li>
	 *     <li>{@code left} is a wrapper for {@code right} or</li>
	 *     <li>{@code right} is a wrapper for {@code left}.</li>
	 * </ul>
	 */
	def boolean compatibleWith(Class<?> left, Class<?> right) {
		if (left === null || right === null) {
			return false
		}
		if (left.unwrapped.isPrimitiveOrVoid && left.unwrapped == right.unwrapped) {
			return true
		}
		if (left.isSubClassOf(right)) {
			return true
		}
		return false
	}
	
	/**
	 * Returns {@code true}, if the class is a primitive type or {@code void}.
	 * @see Class#isPrimitive Class.isPrimitive()
	 */
	def boolean isPrimitiveOrVoid(Class<?> clazz) {
		#[byte, short, int, long, float, double, char, boolean, void].contains(clazz)
	}
	
	/**
	 * Returns the primitive type of a wrapper class.
	 */
	def Class<?> unwrapped(Class<?> clazz) {
		switch clazz {
			case Byte:		byte
			case Short:		short
			case Integer:	int
			case Long:		long
			case Float:		float
			case Double:	double
			case Character:	char
			case Boolean:	boolean
			case Void:		void
			default:		clazz
		}
	}
	
	/**
	 * Returns the wrapper class of a primitive type.
	 */
	def Class<?> wrapped(Class<?> clazz) {
		switch clazz {
			case byte:		Byte
			case short:		Short
			case int:		Integer
			case long:		Long
			case float:		Float
			case double:	Double
			case char:		Character
			case boolean:	Boolean
			case void:		Void
			default:		clazz
		}
	}
	
	/**
	 * Compares two lists pairwise.
	 */
	def <T> boolean pairwiseComparison(List<T> left, List<T> right, Function2<T, T, Boolean> comparisonFunction) {
		if (left.size != right.size) return false
		for (i: 0 ..< left.size) {
			if (!comparisonFunction.apply(left.get(i), right.get(i))) {
				return false
			}
		}
		return true
	}
	
	/**
	 * Returns the matched String of the specified group of a regex search.
	 */
	def String getRegexGroup(String text, String regex, int group) {
		val matcher = Pattern.compile(regex).matcher(text)
		if (matcher.find) {
			matcher.group(group)
		}
		else {
			throw new Exception('''Could not match "«text»" with regex "«regex»".''')
		}
	}
	
	/**
	 * Returns the main FQN without any parameter types of the provided FQN.
	 * <p>
	 * <b>Examples:</b>
	 * <ul>
	 *     <li>{@code getMainFqn("java.util.List<java.lang.Boolean>")}<br>→ {@code "java.util.List"}</li>
	 *     <li>{@code getMainFqn("tuple.Tuple2<model.Student, java.lang.String>")}<br>→ {@code "tuple.Tuple2"}</li>
	 *     <li>{@code getMainFqn("model.Student")}<br>→ {@code "model.Student"}</li>
	 * </ul>
	 */
	def String getMainFqn(String fqn) {
		val regex = '''\s*([\w\.]+)\s*(<(.*)>)?\s*'''
		fqn.getRegexGroup(regex, 1)
	}
	
	/**
	 * Returns a list of all parameter FQNs.
	 * <p>
	 * <b>Examples:</b>
	 * <ul>
	 *     <li>{@code getParameterFqns("java.util.List<java.lang.Boolean>")}<br>→ {@code ["java.lang.Boolean"]}</li>
	 *     <li>{@code getParameterFqns("tuple.Tuple2<model.Student, java.lang.String>")}<br>→ {@code ["model.Student", "java.lang.String"]}</li>
	 *     <li>{@code getParameterFqns("model.Student")}<br>→ {@code [ ]}</li>
	 * </ul>
	 */
	def List<String> getParameterFqns(String fqn) {
		val regex = '''\s*([\w\.]+)\s*(<(.*)>)?\s*'''
		val match = fqn.getRegexGroup(regex, 3) ?: ''
		match.split(',').map[trim]
	}
	
	/**
	 * Returns the {@link Class} of this FQN.
	 * Any information about parameter types gets lost.
	 */
	def Class<?> getMainClass(String fqn) {
		val mainFqn = fqn.mainFqn
		if (mainFqn.nullOrEmpty) throw new Exception('''Cannot get main FQN from "«fqn»".''')
		return mainFqn.loadClass
	}
	
	/**
	 * Returns a list of all parameter {@link Class}es of this FQN.
	 * Any information about parameter's parameter types gets lost.
	 */
	def List<Class<?>> getParameterClasses(String fqn) {
		val parameterFqns = fqn.parameterFqns.map[mainFqn]
		if (parameterFqns.nullOrEmpty || parameterFqns.exists[nullOrEmpty]) throw new Exception('''Cannot get parameter FQNs from "«fqn»".''')
		return parameterFqns.map[loadClass]
	}
	
	// TODO delete start
//	def getParameterTypesFromType(Type type) {
//		val typeFqn = type.typeName
//		val regex = '''[a-zA-Z0-9_\.]+\s*<\s*(([a-zA-Z0-9_\.]+\s*,\s*)*[a-zA-Z0-9_\.]+)\s*>'''
//		val typeFqns = typeFqn.getRegexGroup(regex, 1).split(',').map[trim]
//		if (typeFqns.nullOrEmpty || typeFqns.contains('')) return null
//		try return typeFqns.map[loadClass]
//		catch (ClassNotFoundException e) return null
//	}
//	
//	def getParameterTypesFromMethodParameter(Method method, int parameterIndex) {
//		method.genericParameterTypes.get(parameterIndex).parameterTypesFromType
//	}
//	
//	def getParameterTypesFromMethodReturnType(Method method) {
//		method.genericReturnType.parameterTypesFromType
//	}
	// TODO delete end
	
}
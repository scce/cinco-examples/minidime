# Mini DIME Documentation

1. [Creating and Editing](creating-and-editing/index.md)
	1. [Mini DIME Project](creating-and-editing/mini-dime-project/index.md)
	2. [Process Model](creating-and-editing/process-model/index.md)
	3. [JavaType and AtomicSIB](creating-and-editing/atomic-sib/index.md)
	4. [Generating and Simulating](creating-and-editing/generating-and-simulating/index.md)
2. [SIBs](sibs/index.md)
	1. [ContainsPrimitiveSIB and ContainsJavaNativeSIB](sibs/contains-sib/index.md)
	2. [IteratePrimitiveSIB and IterateJavaNativeSIB](sibs/iterate-sib/index.md)
	3. [PutToContextSIB](sibs/put-to-context-sib/index.md)
	4. [AtomicSIB](sibs/atomic-sib/index.md)
	5. [ProcessSIB](sibs/process-sib/index.md)

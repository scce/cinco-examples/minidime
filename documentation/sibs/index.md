[← Mini DIME Documentation](../index.md)

# 2. SIBs

![Example SIB](example-sib.png)

- A SIB …
	- represents a single execution step or a sub-process
	- may have 0 or more InputPorts (blue)
	- may have 1 or more Branches (connected via control flow edges)
- A Branch …
	- represents a possible outcome of a SIB
	- may have 0 or more OutputPorts (orange)
	- can have 1 outgoing control flow edge to another SIB
- An InputPort …
	- is an interface for submitting data to a SIB
	- has a type ("Text", "Integer", "Real", "Boolean", or any JavaType)
	- may be a list type (indicated by braces around the type name `[...]`)
	- can have 1 incoming data flow edge from an OutputPort or a variable
- An OutputPort …
	- is an interface for receiving data from a Branch
	- has a type (Text, Integer, Real, Boolean, or any JavaType)
	- may be a list type (indicated by braces around the type name `[...]`)
	- can have 0 or more outgoing data flow edges to InputPorts or variables

## Index

1. [ContainsPrimitiveSIB and ContainsJavaNativeSIB](contains-sib/index.md)
2. [IteratePrimitiveSIB and IterateJavaNativeSIB](iterate-sib/index.md)
3. [PutToContextSIB](put-to-context-sib/index.md)
4. [AtomicSIB](atomic-sib/index.md)
5. [ProcessSIB](process-sib/index.md)

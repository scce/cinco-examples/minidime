[← Mini DIME Documentation](../../index.md)

# 2.1. ContainsPrimitiveSIB and ContainsJavaNativeSIB

![ContainsSIB](contains-sib.png)

## Functionality

- The ContainsPrimitiveSIB and ContainsJavaNativeSIB (collectively "ContainsSIB") check whether an element is contained in a list or not
	- If the list contains the element, the "yes" Branch will be next in the control sequence
	- If the list does not contain the element, the "no" Branch will be next
	- If the list is empty or the element is `null`, the "no" Branch will be next

## Usage

- To use a ContainsPrimitiveSIB, drag it from the palette onto the canvas
	- The default type is "Text"
	- To change the type, open the "Cinco Properties View", select each InputPort ("list" and "element") and set the "dataType" attribute to your desired (primitive) type
		- Do not change the "name" and "isList" attributes
- To create a ContainsJavaNativeSIB, drag the desired JavaType from the explorer onto the canvas
	- You cannot change its type retrospectively

![Cinco Properties](cinco-properties.png)

[← Mini DIME Documentation](../../index.md)

# 2.2. IteratePrimitiveSIB and IterateJavaNativeSIB

![IterateSIB](iterate-sib.png)

## Functionality

- The IteratePrimitiveSIB and IterateJavaNativeSIB (collectively "IterateSIB") iterate over a list
	- The IteratorSIB keeps track of which elements have been returned and which have not
	- If there are still unhandled elements left in the list, the "next" Branch will be next in the control sequence
		- The next element will be returned via the "element" OutputPort
	- If all elements have been handled, the "exit" Branch will be next and the IteratorSIB will reset its state
	- A IteratorSIB can handle only one list at a time
		- If the the IteratorSIB receives a different list than before, the IteratorSIB will reset its state and will handle the new list from the start
	- If the list is empty, the "exit" Branch will be next

## Usage

- To create an IteratePrimitiveSIB, drag it from the palette onto the canvas
	- The default type is "[Text]"
	- To change the type, open the "Cinco Properties View", select the InputPort "list" and set the "dataType" attribute to your desired (primitive) type
		- Do not change the "isList" attribute
	- Select the OutputPort "element" of the "next" Branch and set the "dataType" attribute to the same (primitive) type
		- Do not change the "isList" attribute
- To create an IterateJavaNativeSIB, drag the desired JavaType from the explorer onto the canvas
	- You cannot change its type retrospectively

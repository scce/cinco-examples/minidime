[← Mini DIME Documentation](../../index.md)

# 2.5. ProcessSIB

![ProcessSIB](process-sib.png)

## Functionality

- ProcessSIBs are a representation of Process models
- You can reuse and execute Process models inside other Process models by using ProcessSIBs

## Usage

- To create your own Process models, see [1.2. Process Model](../../creating-and-editing/process-model/index.md)
- To use a ProcessSIB in a Process model, locate the corresponding `.process` file in the explorer and drag the file onto the canvas
- The ports and Branches of a ProcessSIB should not be altered, because [they are defined in the Process model](../../creating-and-editing/process-model/index.md#process-model-vs-processsib)
- Double click the ProcessSIB to open the corresponding `.process` file

![Drag](drag.png)

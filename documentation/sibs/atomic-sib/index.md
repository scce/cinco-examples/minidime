[← Mini DIME Documentation](../../index.md)

# 2.4. AtomicSIB

![AtomicSIB](atomic-sib.png)

## Functionality

- AtomicSIBs are a representation of native Java or Xtend code
- There is a set of commonly used AtomicSibs in the `common` folder

## Usage

- To create your own AtomicSIBs, see [1.3. AtomicSIB](../../creating-and-editing/atomic-sib/index.md)
- To use an AtomicSIB in a Process model, locate the corresponding `.sibs` file in the explorer, click the triangle to expand its contents and drag the SIB onto the canvas
	- If there are no triangles to expand, make sure you are in the "Mini DIME Perspective"

![Drag](drag.png)

- Double click the AtomicSIB to open the corresponding `.sibs` file
- The ports and Branches of an AtomicSIB should not be altered, because [they are defined in the `.sibs` file](../../creating-and-editing/atomic-sib/index.md)


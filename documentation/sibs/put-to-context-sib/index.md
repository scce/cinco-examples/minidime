[← Mini DIME Documentation](../../index.md)

# 2.3. PutToContextSIB

![PutToContextSIB](put-to-context-sib.png)

## Functionality

- The PutToContextSIB takes all data from its InputPorts and passes them through to its OutputPorts
	- The data will not be changed in any way
	- This SIB is especially useful for initializing (primitive) variables in the DataContext with a static value
		- Add an InputPort to the PutToContextSIB by dragging one from the palette onto the SIB (or simply double click the PutToContextSIB)
		- To set the type of the InputPort, open the "Cinco Properties View", select the InputPort and set the "dataType" attribute to your desired (primitive) type
			- You may change the "name" attribute, if you like
		- Right click the InputPort and select "Convert Input to Static Value" (or simply double click the InputPort)
		- In the Cinco properties view, you can now set a static value
		- Right click the PutToContextSIB and select "Update SIB" to automatically apply the changes to the Branch

![Initialize variable](initialize-variable.png)

## Usage

- To create a PutToContextSIB, drag it from the palette onto the canvas
	- To add InputPorts, drag them from the palette onto the SIB (or simply double click the PutToContextSIB)
	- To apply any changes of the InputPorts to the OutputPorts of the Branch, right click the PutToContextSIB and select "Update SIB"

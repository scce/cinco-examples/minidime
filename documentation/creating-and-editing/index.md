[← Mini DIME Documentation](../index.md)

# 1. Creating and Editing

## Index

1. [Mini DIME Project](mini-dime-project/index.md)
2. [Process Model](process-model/index.md)
3. [JavaType and AtomicSIB](atomic-sib/index.md)
4. [Generating and Simulating](generating-and-simulating/index.md)

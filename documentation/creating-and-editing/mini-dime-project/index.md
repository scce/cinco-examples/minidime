[← Mini DIME Documentation](../../index.md)

# 1.1. Mini DIME Project

## Create

1. Click "File → New → Other"
2. Select "Cinco Product → New Mini DIME Project"
	- Do not choose "New MiniDIMETool Project"
3. Click "Next"
4. Choose a name for your project
5. Click "Finish"

## Structure

A fresh Mini DIME project will include some pre-generated folders and code
- `src` folder
	- Place your own Java or Xtend code here
- `src-gen` folder
	- Mini DIME will put generated code here
	- **Attention:** Any code inside the `src-gen` folder might be deleted or overwritten in the generation process!
- `xtend-gen` folder
	- The Xtend compiler will put generated code here
	- **Attention:** Any code inside the `xtend-gen` folder might be deleted or overwritten in the generation process!
- `common` folder
	- Contains common AtomicSIBs
	- Use these in your Process models
	- If you see errors in the `common` folder after creating a new project, you can safely ignore them
	- The errors should vanish after the first generation process
- `models` folder
	- You can place your Process models and `.sibs` files here
	- This is only a suggestion though
- `META-INF` folder and `build.properties` file
	- Configuration files, which are needed by Java and Eclipse

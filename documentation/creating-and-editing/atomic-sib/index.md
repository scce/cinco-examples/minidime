[← Mini DIME Documentation](../../index.md)

# 1.3. JavaType and AtomicSIB

## Create

1. Write Java or Xtend classes to use as JavaTypes inside your Process models
2. Write static Java or Xtend methods with a specific signature (see below) for AtomicSIBs
3. Declare your JavaTypes and AtomicSIBs in a `.sibs` file
	1. Click "File → New → Other"
	2. Select "General → File"
	3. Select a file name and use the suffix `.sibs`
	4. Select a location
	5. Click "Finish"

### Method signature

- The underlaying method …
	- must be public and static
	- may only use parameter types
		- `boolean`, `java.lang.Boolean`
		- `long`, `java.lang.Long`
		- `double`, `java.lang.Double`
		- `java.lang.String`
		- any declared JavaType
		- a list (`java.util.List`) of any of these types
	- may only use return types
		- `void`
		- `boolean`, `java.lang.Boolean`
		- `long`, `java.lang.Long`
		- `double`, `java.lang.Double`
		- `java.lang.String`
		- any declared JavaType
		- a list (`java.util.List`) of any of these types
		- a tuple (`tuple.Tuple`) of any of these types
	- may return `null`
	- may throw exceptions
- Tuples are useful, if your AtomicSIB has multiple return values
	- After creating a Mini DIME project, you will have access to tuple classes with cardinality 1 (`tuple.Tuple1`) through 10 (`tuple.Tuple10`)
	- They are located in the `src-gen` folder
	- If your AtomicSIB has more than 10 return values, Mini DIME will generate more tuple classes accordingly
		- **Note:** In order for Mini DIME to generate more tuple classes, you have to declare your AtomicSIB with the correct number of OutputPorts, use the AtomicSIB in your Process model and then [generate your project](../generating-and-simulating/index.md)

## Edit

### `.sibs` file

- A `.sibs` file …
	- must declare a package name (should be unique among other `.sibs` files)
	- may declare any number of JavaTypes
	- may declare any number of AtomicSibs

### JavaTypes

- A declaration of a JavaType …
	- must start with the keyword `javaType`
	- followed by a name, for referencing this type in AtomicSIBs and Process models
	- followed by the fully qualified name of the Java class
	- Example: `javaType myDate: java.util.date`
- You do not need to define JavaTypes for primitive types
	- Use `boolean` for `boolean` or `java.lang.Boolean`
	- Use `integer` for `long` or `java.lang.Long`
	- Use `real` for `double` or `java.lang.Double`
	- Use `text` for `java.lang.String`
- You do not need to define JavaTypes for lists
	- Use brackets around the type: `[integer]`

### AtomicSIBs

- A declaration of an AtomicSIB …
	- must start with the keyword `sib`
	- followed by a name, for referencing this AtomicSIB in Process models
	- followed by the fully qualified name of the executing Java class
	- followed by the name of the executing method
	- followed by an enumeration of InputPorts
	- followed by an enumeration of Branches
- There a 4 different types of Branches
	- `success`: Successfully executed the method
	- `true`, `false`: Successfully executed the method and the return value is a Boolean
	- `noresult`: Successfully executed the method and the return value is `null`
	- `failure`: An exception was thrown
- Either a `success` Branch xor both `true` and `false` Branches are necessary
- The `noresult` and `failure` Branches are optional
	- Be aware, that exceptions will not be caught this way
- Only the `success` Branch may have OutputPorts
- You can use auto completion (`CTRL` + `SPACE`) after entering an executor class to generate Ports and Branches automatically
- If you make any changes to the declaration of an AtomicSIB, you must update **every occurrence** of it in Process models by right clicking the AtomicSIB and selecting "Update SIB"


```
sib «name»: «Java class»#«executor method»
	«input port name»: «input port type»
	«input port name»: «input port type»
	// ...
	-> «branch name»
		«output port name»: «output port type»
		«output port name»: «output port type»
		// ...
	-> «branch name»
	-> «branch name»
	// ...
```

### Example

```xtend
package myModel

import java.util.Date
import java.util.List
import tuple.Tuple4

class Student {
	var String name
	var Date birthday
	var int id
	var List<Lecture> lectures

	// ...

	def static getAttributes(Student s) {
		// Implied return type: Tuple4<String, Date, Long, List<Lecture>>
		return new Tuple4(s.name, s.birthday, s.id.longValue, s.lectures)
	}

	def static boolean hasPassedLecture(Student s, Lecture l) {
		// ...
	}
}

class Lecture {
	// ...
}
```

```sibs
package main

javaType Date: java.util.Date
javaType Student: myModel.Student
javaType Lecture: myModel.Lecture

sib Attributes: myModel.Student#getAttributes
	input: Student
	-> success
		name: text
		birthday: Date
		studentId: integer
		lectures: [Lecture]
	-> noresult

sib HasPassed: myModel.Student#hasPassedLecture
	student: Student
	lecture: Lecture
	-> true
	-> false
```

![Custom atomic SIBs](example.png)

[← Mini DIME Documentation](../../index.md)

# 1.4. Generating and Simulating

- After you have modeled your Processes, you can generate Xtend source code from them
- Make sure there are not any validation errors in your Process models and your `.sibs` files

## Generate

1. Open your main Process
2. Click the "G" icon in the tool bar or select "GraphModel → Generate Code"
3. Wait for the generation process to finish
	- **Attention:** Files in the `src-gen` and `xtend-gen` folders will be overwritten
4. There will be a class for every Process and SIB you have created in the `src-gen` folder
5. A `main.Main` class will be generated into the `src` folder once
	- This is an example class for running your main Process model
	- You may edit this file as you like (e. g. add input values for your model)
	- This file will not be overwritten by the generator
	- If you wish to re-generate the Main class, delete it and generate your project again
	- This might be useful after making changes to the main Process' signature

![Generate code](generate-code.png)

## Simulate

1. Open `src/main/Main.xtend`
2. Make sure that `rootProcess.execute(...)` uses your desired input parameters
3. Right click the main class
4. Select "Run As → Java Application"
5. The default implementation of the Main class will print the execution result of your Processes model to the console
	- The first line indicates the Branch, that your Process returned
	- The following lines list the OutputPorts of the Branch and their corresponding values (if applicable)

![Simulate](simulate.png)

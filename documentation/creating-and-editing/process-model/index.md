[← Mini DIME Documentation](../../index.md)

# 1.2. Process Model

## Create

1. Click "File → New → Other"
2. Select "Cinco Product → New Process"
3. Click "Next" and follow the wizzard
4. Add a StartSIB by dragging it from the palette onto the canvas
5. Add at least 1 EndSIB by dragging it from the palette onto the canvas

## Edit

- Make sure you are using the "Mini DIME Perspective"
	- Use the "Cinco Properties" view to see and edit additional properties of nodes and edges
	- Check for errors in the "Model Validation" view

![Mini DIME perspective](mini-dime-perspective.png)

### Specification

- A Process model …
	- must contain exactly 1 StartSIB
	- must contain at least 1 EndSIB
	- may contain a DataContext for storing data in variables

### Process model vs ProcessSIB

- The StartSIB and its OutputPorts correspond to the ProcessSIB and its InputPorts
- Each EndSIB and its InputPorts correspond to a Branch and its OutputPorts
- If you make any changes to the signature of a Process model, you must update **every occurrence** of it by right clicking the ProcessSIB and selecting "Update SIB"

![Process model vs ProcessSIB](process-model-vs-process-sib.png)

### Control flow

- To add SIBs to the model, drag them from the palette onto the canvas
	- You can also drag other Process models, AtomicSIBs or JavaTypes from the explorer onto the canvas
	- See [2. SIBs](../../sibs/index.md) for more info about SIBs
- ControlFlow edges define the control flow between SIBs (incl. StartSIB, EndSIBs) and Branches
	- To connect nodes via edges, hover over the node and drag the appearing arrow icon

![Hover](hover.png)

### Data flow

- DataFlow edges define the data flow between InputPorts, OutputPorts and variables
	- To connect nodes via edges, hover over the node and drag the appearing arrow icon
	- InputPorts with no incoming edge will use `null` (or an empty list) as their value
	- The values of OutputPorts with no outgoing edges will be ignored
	- Variables, that were not written to, will be `null` (or an empty list)
	- If the Branch of an OutputPort was never reached before and it is connected to an InputPort, the InputPort will receive `null` (or an empty list)

![Return null](return-null.png)

- Variables
	- Variables must be placed inside a DataContext container
	- To add a variable of a primitive type (text, integer, real, boolean), drag it from the palette onto a DataContext
	- To add a variable of a JavaType, locate the corresponding `.sibs` file in the explorer, click the triangle to expand its contents and drag the JavaType onto a DataContext
	- To initialize PrimitiveVariables, [use a PutToContextSIB](../../sibs/put-to-context-sib/index.md#functionality)
	- To create a variable for a list of type X, create a variable of type X, select it and check the "isList" box in the "Cinco Properties View"
- Ports
	- The StartSIB and Branches accept OutputPorts
	- EndSIBs and any other type of SIB accept InputPorts
	- To add a port of a primitive type, drag one from the palette onto the SIB or Branch (or use a double click on a SIB)
	- To add a port of a JavaType, locate the corresponding `.sibs` file in the explorer, click the triangle to expand its contents and drag the JavaType onto the SIB or Branch
	- To create a port for a list of type X, create a port of type X, select it and check the "isList" box in the "Cinco Properties View"
- Although all of these nodes accept additional ports, they are not always usable
	- Only the StartSIB, EndSIBS and PutToContextSIBs can make use of additional ports
	- If you alter, add or delete ports from other SIBs and Branches, errors might occur
	- The double click action only works on SIBs that truly support additional ports
- Instead of an InputPort you can also use a StaticInput
	- StaticInputs do not accept any incoming DataFlow edges
	- Instead they assign a static value to the port
	- To convert an InputPort to a StaticInput, make sure the port has the correct type, then right click it and select "Convert Input to Static Value" (or simply double click it)
	- To set the static value, select the StaticInput and change the "value" attribute in the "Cinco Properties View"

# Mini DIME

A stripped down version of DIME that only supports processes with native SIBs and native Java types.

This README describes the necessary steps to get Mini DIME up an running. For instructions on how to use Mini DIME, see the [Mini DIME Documentation](documentation/index.md).

![Hello World](documentation/readme/hello-world.png)



## Index

1. [Cinco](#1-cinco)
2. [Import Mini DIME project](#2-import-mini-dime-project)
3. [Generate SIBLibrary Language](#3-generate-siblibrary-language)
4. [Generate Cinco product](#4-generate-cinco-product)
5. [Run Mini DIME](#5-run-mini-dime)



## 1. Cinco

1. Install and start [Cinco 2.0](https://cinco.scce.info/download/)
2. Click "File → Switch Workspace → Other"
3. Choose a new location
4. Click "Launch"
5. Click "Window → Preferences"
6. Under "Java → Compiler" select "Comiler complience level: 11"
7. Restart Cinco

![Comiler complience level](documentation/readme/compiler-complience-level.png)



## 2. Import Mini DIME project

1. Click "File → Import"
2. Select "Git → Projects from Git"
3. Click "Next"
4. Select "Clone URI"
5. Click "Next"
6. Enter for "URI": `https://gitlab.com/scce/cinco-examples/minidime.git`
7. Click "Next" and follow the wizzard

![Import from Git](documentation/readme/import-from-git.png)



## 3. Generate SIBLibrary Language

1. Open `info.scce.cinco.product.minidime.siblibrary/model/SIBLibrary.genmodel`
2. Right click on the first element "SIBLibrary"
3. Click "Generate Model Code"
4. Wait until finished

![Generate SIBLibrary Model Code](documentation/readme/generate-siblibrary-model-code.png)

5. Right click on `info.scce.cinco.product.minidime.siblibrary/src/info/scce/cinco/product/minidime/siblibrary/GenerateSIBLibrary.mwe2`
6. Click "Run As → MWE2 Workflow"
7. Click "Next" and follow the wizzard

![Generate SIBLibrary Language](documentation/readme/generate-siblibrary-language.png)



## 4. Generate Cinco product

1. Right click on `info.scce.cinco.product.minidime/model/MiniDIMETool.cpd`
2. Click "Generate Cinco Product"
3. Wait until finished

![Generate Cinco Product](documentation/readme/generate-cinco-product.png)



## 5. Run Mini DIME

1. Right click on `info.scce.cinco.product.minidime`
2. Click "Run As → Eclipse Application"

![Run Mini DIME](documentation/readme/run-mini-dime.png)

package info.scce.cinco.product.minidime.checks

import de.jabc.cinco.meta.plugin.mcam.runtime.core._CincoAdapter
import de.jabc.cinco.meta.plugin.mcam.runtime.core._CincoId
import graphmodel.GraphModel
import org.eclipse.xtext.resource.XtextResource

/**
 * Copied from info.scce.dime.checks.AbstractModelIntegrityCheck - Complete
 */
abstract class AbstractModelIntegrityCheck<T1 extends _CincoId, T2 extends _CincoAdapter<T1, ? extends GraphModel>> extends AbstractCheck<T1, T2> {
	
	override void doExecute(T2 adapter) {
		val res = try adapter.model.eResource
		catch(Exception e) {
			fail(adapter, 'failed to retrieve resource')
			return
		}
		if (res === null) {
			fail(adapter, 'failed to retrieve resource')
			return
		}
		if (res instanceof XtextResource) {
			if (res.parseResult.hasSyntaxErrors) {
				fail(adapter, 'model has syntax errors')
				return
			}
		}
	}
	
	def private void fail(T2 adapter, String msg) {
		addError(adapter.getIdByString(adapter.model.id), msg)
		processResults
	}
	
	override void init() {
		// Intentionally left blank
	}
	
}

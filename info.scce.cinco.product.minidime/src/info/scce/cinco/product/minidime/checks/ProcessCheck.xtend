package info.scce.cinco.product.minidime.checks

import info.scce.cinco.product.minidime.process.helper.ProcessExtension

/**
 * Copied from info.scce.dime.checks.ProcessCheck - Complete
 */
abstract class ProcessCheck extends info.scce.cinco.product.minidime.mcam.modules.checks.MiniDIMEProcessCheck {
	
	protected extension ProcessExtension = new ProcessExtension
	
	static class SwitchException extends IllegalStateException {
		
		new() {
			super('Default case in exhaustive switch; implementation broken?')
		}
		
	}
	
}

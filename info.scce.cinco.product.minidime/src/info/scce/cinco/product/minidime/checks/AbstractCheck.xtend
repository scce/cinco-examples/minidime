package info.scce.cinco.product.minidime.checks

import de.jabc.cinco.meta.plugin.mcam.runtime.core._CincoAdapter
import de.jabc.cinco.meta.plugin.mcam.runtime.core._CincoId
import graphmodel.GraphModel
import info.scce.mcam.framework.modules.CheckModule

/**
 * Copied from info.scce.dime.checks.AbstractCheck - Complete
 */
abstract class AbstractCheck<T1 extends _CincoId, T2 extends _CincoAdapter<T1, ? extends GraphModel>> extends CheckModule<T1, T2> {
	
	override final void execute(T2 adapter) {
		try {
			adapter.doExecute
		}
		catch(Exception e) {
			addError(adapter.getIdByString(adapter.model.id), 'check execution failed')
			System.err.println(e)
			System.err.println(e.message)
			e.printStackTrace
		}
	}
	
	def void doExecute(T2 adapter)
	
}

package info.scce.cinco.product.minidime.api

import de.jabc.cinco.meta.runtime.hook.CincoPostCreateHook
import org.eclipse.emf.ecore.EObject

/**
 * Copied from info.scce.dime.api.DIMEPostCreateHook - Complete
 */
abstract class MiniDIMEPostCreateHook<T extends EObject> extends CincoPostCreateHook<T> {
	
	// Add extensions here
	
}

package info.scce.cinco.product.minidime.api

import de.jabc.cinco.meta.runtime.hook.CincoPreDeleteHook
import org.eclipse.emf.ecore.EObject

/**
 * Copied from info.scce.dime.api.DIMEPreDeleteHook - Complete
 */
abstract class MiniDIMEPreDeleteHook<T extends EObject> extends CincoPreDeleteHook<T> {
	
	// Add extensions here
	
}
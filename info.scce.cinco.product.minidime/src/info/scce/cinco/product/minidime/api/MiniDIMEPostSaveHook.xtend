package info.scce.cinco.product.minidime.api

import de.jabc.cinco.meta.runtime.hook.CincoPostSaveHook
import graphmodel.GraphModel

/**
 * @author Fabian Storek
 */
abstract class MiniDIMEPostSaveHook<T extends GraphModel> extends CincoPostSaveHook<T> {
	
	// Add extensions here
	
}

package info.scce.cinco.product.minidime.api

import de.jabc.cinco.meta.runtime.hook.CincoPostDeleteHook
import de.jabc.cinco.meta.runtime.xapi.CollectionExtension
import de.jabc.cinco.meta.runtime.xapi.FileExtension
import de.jabc.cinco.meta.runtime.xapi.ResourceExtension
import de.jabc.cinco.meta.runtime.xapi.WorkbenchExtension
import de.jabc.cinco.meta.runtime.xapi.WorkspaceExtension
import graphmodel.ModelElement

/**
 * Copied from info.scce.dime.api.DIMEPostDeleteHook - Complete
 */
abstract class MiniDIMEPostDeleteHook<T extends ModelElement> extends CincoPostDeleteHook<T> {
	
	protected extension CollectionExtension         = new CollectionExtension
    protected extension FileExtension               = new FileExtension
    protected extension MiniDIMEGraphModelExtension = new MiniDIMEGraphModelExtension
	protected extension ResourceExtension           = new ResourceExtension
    protected extension WorkbenchExtension          = new WorkbenchExtension
    protected extension WorkspaceExtension          = new WorkspaceExtension
	
}

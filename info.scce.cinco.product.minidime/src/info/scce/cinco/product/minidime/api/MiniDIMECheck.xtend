package info.scce.cinco.product.minidime.api

import de.jabc.cinco.meta.plugin.mcam.runtime.core._CincoAdapter
import de.jabc.cinco.meta.plugin.mcam.runtime.core._CincoId
import de.jabc.cinco.meta.runtime.xapi.GraphModelExtension
import graphmodel.GraphModel
import info.scce.cinco.product.minidime.checks.AbstractCheck

/**
 * Copied from info.scce.dime.api.MiniDIMECheck - Complete
 */
abstract class MiniDIMECheck <T1 extends _CincoId, T2 extends _CincoAdapter<T1, ? extends GraphModel>> extends AbstractCheck<T1, T2> {
	
	protected extension GraphModelExtension = new GraphModelExtension
	
	/**
	 * This method is final. If you want to add own behavior to be executed
	 * before every check, implement {@link #beforeCheck() beforeCheck()}. 
	 */
	override final init() {
		// Always reinitialize extensions to avoid caching problems.
		_graphModelExtension = new GraphModelExtension	
		beforeCheck
	}
	
	def void beforeCheck() {
		// Does nothing by default
	}
	
}

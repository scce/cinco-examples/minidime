package info.scce.cinco.product.minidime.api

import de.jabc.cinco.meta.runtime.hook.CincoPreSaveHook
import graphmodel.GraphModel

/**
 * @author Fabian Storek
 */
abstract class MiniDIMEPreSaveHook<T extends GraphModel> extends CincoPreSaveHook<T> {
	
	// Add extensions here
	
}

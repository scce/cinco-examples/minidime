package info.scce.cinco.product.minidime.api

import de.jabc.cinco.meta.runtime.xapi.GraphModelExtension
import de.jabc.cinco.meta.runtime.xapi.ResourceExtension
import graphmodel.GraphModel
import graphmodel.IdentifiableElement
import graphmodel.ModelElement
import graphmodel.ModelElementContainer
import info.scce.cinco.product.minidime.process.ProcessSIB

/**
 * Copied from info.scce.dime.api.DIMEGraphModelExtension - Complete
 */
class MiniDIMEGraphModelExtension extends GraphModelExtension {
	
	protected extension ResourceExtension = new ResourceExtension	
	
	/**
	 * Retrieve the local package string (project relative) in standard Java syntax.
	 * @param it - The model element in some model, for which the package should be built.
	 */
	dispatch def String getLocalPkg(ModelElement it) {
		rootElement.localPkg
	}
	
	/**
	 * Retrieve the local package string (project relative) in standard Java syntax.
	 * @param it - The model, for which the package should be built.
	 */
	dispatch def getLocalPkg(GraphModel it) {
		buildLocalPkg(false)
	}
	
	/**
	 * Retrieve the local package string (project relative) in standard Java syntax.
	 * @param it - The model element in some model, for which the package should be built.
	 */
	dispatch def String getLocalPkgWithFilename(ModelElement it) {
		rootElement.localPkgWithFilename
	}
	
	/**
	 * Retrieve the local package string (project relative) in standard Java syntax.
	 * @param it - The model, for which the package should be built.
	 */
	dispatch def getLocalPkgWithFilename(GraphModel it) {
		buildLocalPkg(true)
	}
	
	/**
	 * Retrieve the local package string (project relative) in standard Java syntax.
	 * @param model - The model element in some model, for which the package should be built.
	 * @param includeFileName - Include the name of the file (without extension) as the last element of the package.
	 */
	private def buildLocalPkg(GraphModel model, boolean includeFileName) {
		val project = model.eResource.project
		val absoluteProjectPath = project.fullPath.toFile.absolutePath
		val absoluteModelPath = switch (it : model.eResource.IResource.fullPath.toFile) {
			case !includeFileName: parentFile
			case includeFileName: it
		}.absolutePath
		if (!absoluteModelPath.startsWith(absoluteProjectPath)) throw new IllegalStateException(
			'''«model.id»: model path does not start with project path'''
		)
		val suffixPos = absoluteModelPath.lastIndexOf('.')
		// remove project path end suffix
		val relativeModelPath = if (includeFileName && suffixPos > 0) {
			absoluteModelPath.substring(absoluteProjectPath.length, suffixPos)
		}
		else {
			absoluteModelPath.substring(absoluteProjectPath.length)
		}
		relativeModelPath.replaceFirst("^(\\.|\\\\|\\/)*", '') // eliminate leading '/' or '\' or '.'
		                 .replaceAll("(\\\\|\\/)", '.')        // replace all '/' or '\' by '.'
		                 .toLowerCase
	}
	
	def ModelElementContainer getPrimeReferencedContainer(IdentifiableElement it) {
		switch it {
			ProcessSIB: proMod
		}
	}
	
}

package info.scce.cinco.product.minidime.api

import de.jabc.cinco.meta.runtime.action.CincoCustomAction
import graphmodel.IdentifiableElement
import info.scce.cinco.product.minidime.process.helper.ProcessExtension

/**
 * Copied from info.scce.dime.api.DIMECustomAction - Complete
 */
abstract class MiniDIMECustomAction<T extends IdentifiableElement> extends CincoCustomAction<T> {
	
	protected extension ProcessExtension = new ProcessExtension
	
}

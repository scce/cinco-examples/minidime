package info.scce.cinco.product.minidime.process.build

import graphmodel.ModelElement
import info.scce.cinco.product.minidime.process.Branch
import info.scce.cinco.product.minidime.process.EndSIB
import info.scce.cinco.product.minidime.process.Input
import info.scce.cinco.product.minidime.process.Output
import info.scce.cinco.product.minidime.process.ProcessSIB
import java.util.List

/**
 * Copied from info.scce.dime.process.build.ProcessSIBBuild - Complete
 */
class ProcessSIBBuild<T extends ProcessSIB> extends SIBBuild<T, ModelElement, EndSIB, Input> {
	
	static def initialize(ProcessSIB sib) {
		new ProcessSIBBuild(sib).initialize
	}
	
	static def update(ProcessSIB sib) {
		new ProcessSIBBuild(sib).update
	}
	
	new(T sib) {
		super(sib)
	}
	
	override getSIBReference(T sib) {
		sib.proMod
	}
	
	override isSIBReferenceValid(T sib) {
		!sib.proMod.hasErrors
	}
	
	override getSIBPortReferences(T sib) {
		val List<ModelElement> references = newArrayList
		val startSIB = sib.proMod.startSIB
		if (startSIB !== null) {
			references.addAll(startSIB.outputs)
		}
		return references
	}
	
	override isSIBPortReference(Input sibPort, ModelElement element) {
		if (element instanceof Output) {
			return sibPort.name == element.name
		}
		return false
	}
	
	override getBranchName(EndSIB endSib) {
		endSib.branchName
	}
	
	override getBranchReferences(T sib) {
		sib.proMod.endSIBs
	}
	
	override isBranchReference(Branch branch, EndSIB endSib) {
		branch.name == endSib.branchName
	}
	
	override getBranchPortReferences(EndSIB endSib) {
		endSib.inputs
	}
	
	override isBranchPortReference(Output branchPort, Input endSibPort) {
		branchPort.name == endSibPort.name
	}
	
}
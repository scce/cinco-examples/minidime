package info.scce.cinco.product.minidime.process.hooks

import graphmodel.Direction
import graphmodel.ModelElement
import graphmodel.Node
import info.scce.cinco.product.minidime.api.MiniDIMEPostResizeHook
import info.scce.cinco.product.minidime.process.DataContext
import org.eclipse.swt.widgets.Display

import static java.lang.Math.abs
import static java.lang.Math.max

/**
 * Copied from info.scce.dime.process.hooks.ProcessNodePostResize - Complete
 */
class ProcessNodePostResize<T extends ModelElement> extends MiniDIMEPostResizeHook<T> {
	
	override postResize(T node, int oldWidth, int oldHeight, int oldX, int oldY, Direction direction) {
		if (node instanceof Node) {
			node.updateDataContext(node.height - oldHeight)
		}
	}
	
	def updateDataContext(Node resizedNode, int deltaHeight) {
		if (Display.current === null) {
			return // Only resize if triggered by changes in the UI editor
		}
		val model = resizedNode.rootElement
		val nodes = model.allNodes.drop(DataContext)
		if (!nodes.nullOrEmpty) {
			val botNode = nodes.sortBy[y + height].last
			val scndBotNode = nodes.filter[it != resizedNode].sortBy[y + height].last ?: botNode
			val oldMax = max(resizedNode.y + resizedNode.height - deltaHeight, scndBotNode.y + scndBotNode.height)
			for (dataContext : model.getNodes(DataContext)) {
				val oldDataContextBot = dataContext.y + dataContext.height
				val minHeight = (dataContext.allNodes.map[y + height].sort.last ?: 0) + 17
				if (abs(oldMax - oldDataContextBot) < 2 || dataContext.height == minHeight) {
					// Data context has been aligned to the bottom of the bottom node -> Resize
					val dY = botNode.y + botNode.height - dataContext.y - dataContext.height
					if (dY != 0) {
						println('''Resize data context height from «dataContext.height» to «botNode.y + botNode.height - dataContext.y» by dY=«dY»''')
						dataContext.height = max(minHeight, dataContext.height + dY)
					}
				}
			}
		}
	}
	
}

package info.scce.cinco.product.minidime.process.checks

import graphmodel.Node
import info.scce.cinco.product.minidime.checks.AbstractCheck
import info.scce.cinco.product.minidime.mcam.adapter.MiniDIMEProcessAdapter
import info.scce.cinco.product.minidime.mcam.adapter.MiniDIMEProcessId
import info.scce.cinco.product.minidime.process.BranchConnector
import info.scce.cinco.product.minidime.process.ControlFlow
import info.scce.cinco.product.minidime.process.DataFlow
import info.scce.cinco.product.minidime.process.DataFlowSource
import info.scce.cinco.product.minidime.process.DataFlowTarget
import info.scce.cinco.product.minidime.process.DirectDataFlow
import info.scce.cinco.product.minidime.process.MiniDIMEProcess
import info.scce.cinco.product.minidime.process.SIB
import info.scce.cinco.product.minidime.process.Variable
import java.util.ArrayList
import java.util.HashMap
import java.util.HashSet

/**
 * Copied from info.scce.dime.process.checks.DataFlowInitializeCheck - Complete
 */
class DataFlowInitializeCheck extends AbstractCheck<MiniDIMEProcessId, MiniDIMEProcessAdapter> {
	
	var MiniDIMEProcessAdapter adapter
	var ArrayList<Node> knownElements                         = new ArrayList
	var HashMap<String, Variable> variables                   = new HashMap
	var HashSet<String> usedVariables                         = new HashSet
	var HashMap<String, HashSet<String>> initializedVariables = new HashMap
	var boolean changed                                       = true
	
	override void doExecute(MiniDIMEProcessAdapter adapter) {
		try {
			this.adapter = adapter
			for (id : adapter.entityIds) {
				val obj = id.element
				if (obj instanceof MiniDIMEProcess) {
					variables = obj.collectVariables

					// Calculate initSets
					while (changed) {
						changed = false
						knownElements = new ArrayList
						for (startSib : obj.startSIBs) {
							startSib.analyzeAvailability
						}
					}
					addAutoInitializeForListVariables
					usedVariables = new HashSet
					knownElements = new ArrayList
					for (startSib : obj.startSIBs) {
						startSib.analyzeUsage
					}
					for (variable : variables.values) {
						if (!usedVariables.contains(variable.name))
							addWarning(adapter.getIdByString(variable.id), '''"«variable.name»" not used''')
					}
				}
			}
			processResults
		}
		catch(Exception e) {
			addError(adapter.getIdByString(adapter.model.id), 'check execution failed')
		}
	}

	override void init() {
		knownElements.clear
		variables.clear
		usedVariables.clear
		initializedVariables.clear
		changed = true
	}

	def private void analyzeUsage(Node node) {
		// Don't visit nodes twice
		if (knownElements.contains(node)) { return }
		knownElements.add(node)

		// Variable getter
		if (node instanceof DataFlowTarget) {
			for (input : node.inputs) {
				for (df : input.getIncoming(DataFlow)) {
					if (df instanceof DirectDataFlow) {
						// continue
					}
					else {
						val variable = df.sourceElement.rootVariable
						if (!initializedVariables.get(node.id).contains(variable.name)) {
							addWarning(adapter.getIdByString(node.id), '''"«variable.name»" on port "«input.name»" not initialized correctly''')
						}
						usedVariables.add(variable.name)
					}
				}
			}
		}

		// Variable setter
		if (node instanceof DataFlowSource) {
			for (output : node.outputs) {
				for (DataFlow df : output.getOutgoing(DataFlow)) {
					if (df instanceof DirectDataFlow) {
						// continue
					}
					else {
						val dfNode = df.targetElement
						val variable = dfNode.rootVariable
						if (dfNode.id != variable.id) {
							if (!initializedVariables.get(node.id).contains(variable.name)) {
								addWarning(adapter.getIdByString(node.id), '''"«variable.name»"' on port "«output.name»" not initialized correctly''')
							}
							usedVariables.add(variable.name)
						}
					}
				}
			}
		}

		// Recursive call
		for (Node nextNode : node.nextNodes) {
			nextNode.analyzeUsage
		}
	}

	def private void analyzeAvailability(Node node) {
		// Don't visit nodes twice
		if (knownElements.contains(node)) { return }
		knownElements.add(node)

		// Init map
		if (initializedVariables.get(node.id) === null) {
			initializedVariables.put(node.id, new HashSet)
		}

		// Calculate retain
		val tempInitVars = new HashMap<Node, HashSet<String>>
		for (prevNode : node.prevNodes) {
			if (initializedVariables.get(prevNode.id) !== null) {
				tempInitVars.put(prevNode, new HashSet(initializedVariables.get(prevNode.id)))
			}
		}
		
		// Calculate retain
		var tempSet = new HashSet<String>
		var i = 0
		for (prevNode : tempInitVars.keySet) {
			if (i == 0) {
				tempSet = tempInitVars.get(prevNode)
			}
			else {
				tempSet.retainAll(tempInitVars.get(prevNode))
			}
			i++
		}

		// Variable setter
		if (node instanceof DataFlowSource) {
			for (output : node.outputs) {
				for (df : output.getOutgoing(DataFlow)) {
					if (df instanceof DirectDataFlow) {
						// continue
					}
					else {
						val dfNode = df.targetElement
						val variable = dfNode.rootVariable
						if (dfNode.id == variable.id && !tempSet.contains(variable.name)) {
							tempSet.add(variable.name)
						}
					}
				}
			}
		}

		if (!initializedVariables.get(node.id).containsAll(tempSet)
				|| !tempSet.containsAll(initializedVariables.get(node.id))) {
			initializedVariables.put(node.id, new HashSet(tempSet))
			changed = true
		}

		// Recursive call
		for (Node nextNode : node.nextNodes) {
			nextNode.analyzeAvailability
		}
	}
	
	def private HashSet<Node> getNextNodes(Node node) {
		val nodeSet = new HashSet<Node>
		for (edge : node.getOutgoing(ControlFlow)) {
			nodeSet.add(edge.targetElement)
		}
		for (edge : node.getOutgoing(BranchConnector)) {
			nodeSet.add(edge.targetElement)
		}
		return nodeSet
	}
	
	def private HashSet<Node> getPrevNodes(Node node) {
		val nodeSet = new HashSet<Node>
		for (edge : node.getIncoming(ControlFlow)) {
			nodeSet.add(edge.sourceElement)
		}
		for (edge : node.getIncoming(BranchConnector)) {
			nodeSet.add(edge.sourceElement)
		}
		if (node.container instanceof SIB) {
			nodeSet.add(node.container as SIB)
		}
		return nodeSet
	}

	def private void addAutoInitializeForListVariables() {
		for (varName : variables.keySet) {
			val variable = variables.get(varName)
			if (variable.isIsList) {
				for (nodeId : initializedVariables.keySet) {
					initializedVariables.get(nodeId).add(varName)
				}
			}
		}
	}

	def private Variable getRootVariable(Node node) {
		if (node instanceof Variable) {
			return node
		}
		throw new IllegalStateException('could not identify root variable, maybe missing implementation')
	}

	def private HashMap<String, Variable> collectVariables(MiniDIMEProcess process) {
		variables = new HashMap
		for (dc : process.dataContexts) {
			for (variable : dc.variables) {
				variables.put(variable.name, variable)
			}
		}
		return variables
	}

}

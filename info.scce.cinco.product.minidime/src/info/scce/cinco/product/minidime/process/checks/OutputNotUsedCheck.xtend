package info.scce.cinco.product.minidime.process.checks

import info.scce.cinco.product.minidime.checks.AbstractCheck
import info.scce.cinco.product.minidime.mcam.adapter.MiniDIMEProcessAdapter
import info.scce.cinco.product.minidime.mcam.adapter.MiniDIMEProcessId
import info.scce.cinco.product.minidime.process.DataFlow
import info.scce.cinco.product.minidime.process.DataFlowSource

/**
 * Copied from info.scce.dime.process.checks.OutputNotUsedCheck - Complete
 */
class OutputNotUsedCheck extends AbstractCheck<MiniDIMEProcessId, MiniDIMEProcessAdapter> {
	
	override void doExecute(MiniDIMEProcessAdapter adapter) {
		for (id : adapter.entityIds) {
			val obj = id.getElement();
			if (obj instanceof DataFlowSource) {
				if (obj.container instanceof Process) {
					for (output : obj.outputs) {
						if (output.getOutgoing(DataFlow).empty) {
							addWarning(adapter.getIdByString(output.id), 'not used')
						}
					}
				}
			}
		}
	}
	
	override void init() {
		// Intentionally left blank
	}
	
}

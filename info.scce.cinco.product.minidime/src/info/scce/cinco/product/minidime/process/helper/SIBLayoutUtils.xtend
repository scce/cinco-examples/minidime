package info.scce.cinco.product.minidime.process.helper

import graphmodel.Container
import graphmodel.ModelElementContainer
import graphmodel.Node
import info.scce.cinco.product.minidime.process.AbstractBranch
import info.scce.cinco.product.minidime.process.EndSIB
import info.scce.cinco.product.minidime.process.IO
import info.scce.cinco.product.minidime.process.Input
import info.scce.cinco.product.minidime.process.Output
import info.scce.cinco.product.minidime.process.SIB
import info.scce.cinco.product.minidime.process.StartSIB

import static info.scce.cinco.product.minidime.process.helper.LayoutConstants.*
import static info.scce.cinco.product.minidime.process.helper.NodeLayoutUtils.*

/**
 * Copied from info.scce.dime.process.helper.SIBLayoutUtils - Complete
 */
class SIBLayoutUtils {
	
	def private static void resize(Node node, int width, int height) {
		node.resize(width, height)
	}
	
	def private static void moveTo(Node node, Container container, int x, int y) {
		node.moveTo(container, x, y)
	}
	
	def private static void resizeAndLayoutBeforeDelete(Container container, Node doomedNode, int xMargin, int initialY, int ySpace, Class<? extends Node> ... layoutedTypes) {
		var y = initialY
		var ioAmount = 0
		for (layoutedType : layoutedTypes) {
			ioAmount += container.getModelElements(layoutedType).size
		}
		ioAmount -= if (doomedNode === null) 0 else 1 
		resize(container, container.width, getHeight(ioAmount, container.class))
		for (layoutedType : layoutedTypes) {
			for (node: container.getModelElements(layoutedType)) {
				if (node !== doomedNode) {
					moveTo(node, container, xMargin, y)
					resize(node, container.width - 2 * xMargin, node.height)
					y += ySpace
				}
			}
		}
	}

	/**
	 * Convenience wrapper for {@link SIBLayoutUtils#resizeAndLayoutBeforeDelete(
	 * CModelElementContainer, CIO) resizeAndLayoutBeforeDelete(
	 * CModelElementContainer, CIO)} with doomedInputOutput = null
	 */
	def static void resizeAndLayout(ModelElementContainer container) {
		resizeAndLayoutBeforeDelete(container, null)
	}

	/**
	 * Resizes container's height so that all contained elements (minus the one
	 * that is about to be deleted) fit, positions them within the containerand
	 * resizes their width according to container's width. 
	 * <p>
	 * This only works for a predefined set of containers:
	 * {@link CStartSIB}, {@link CEndSIB}, {@link CSIB}, {@link CAbstractBranch} 
	 * @param container the layouted container
	 * @param doomedInputOutput The data port that is about to be deleted.
	 *        Will be ignored for layouting. Can be null if nothing is going to
	 *        be deleted
	 */
	def static void resizeAndLayoutBeforeDelete(ModelElementContainer container, IO doomedInputOutput) {
		switch container {
			StartSIB:
				resizeAndLayoutBeforeDelete(container as StartSIB, doomedInputOutput)
			EndSIB:
				resizeAndLayoutBeforeDelete(container as EndSIB, doomedInputOutput)
			SIB:
				resizeAndLayoutBeforeDelete(container as SIB, doomedInputOutput)
			AbstractBranch:
				resizeAndLayoutBeforeDelete(container as AbstractBranch, doomedInputOutput)
		}
	}

	def static void resizeAndLayoutBeforeDelete(StartSIB startSIB, IO doomedInputOutput) {
		resizeAndLayoutBeforeDelete(startSIB, doomedInputOutput, PORT_X, SIB_FIRST_PORT_Y, PORT_SPACE, Output)
	}

	def static void resizeAndLayoutBeforeDelete(EndSIB endSIB, IO doomedInputOutput) {
		resizeAndLayoutBeforeDelete(endSIB, doomedInputOutput, PORT_X, SIB_FIRST_PORT_Y, PORT_SPACE, IO)
	}

	def static void resizeAndLayoutBeforeDelete(SIB cSib, IO doomedInputOutput) {
		resizeAndLayoutBeforeDelete(cSib, doomedInputOutput, PORT_X, SIB_FIRST_PORT_Y, PORT_SPACE, Input)
	}
	
	def static void resizeAndLayoutBeforeDelete(AbstractBranch cBranch, IO doomedOutput) {
		resizeAndLayoutBeforeDelete(cBranch, doomedOutput, PORT_X, BRANCH_FIRST_PORT_Y, PORT_SPACE, Output)
	}

}
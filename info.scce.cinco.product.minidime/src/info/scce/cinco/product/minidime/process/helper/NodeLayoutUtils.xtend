package info.scce.cinco.product.minidime.process.helper

import graphmodel.Container
import info.scce.cinco.product.minidime.process.AbstractBranch
import info.scce.cinco.product.minidime.process.EndSIB
import info.scce.cinco.product.minidime.process.SIB
import info.scce.cinco.product.minidime.process.StartSIB
import info.scce.cinco.product.minidime.process.Variable

import static info.scce.cinco.product.minidime.process.helper.LayoutConstants.*

/**
 * Copied from info.scce.dime.process.helper.NodeLayout - Complete
 */
class NodeLayoutUtils {
	
	def static int getSIBHeight(int portAmount) {
		return 65 + portAmount * PORT_SPACE + if (portAmount > 0) 7 else 0
	}

	def static int getBranchHeight(int portAmount) {
		return 25 + portAmount * PORT_SPACE + if (portAmount > 0) 7 else 0
	}
	
	def static int getVariableHeight(int attrAmount) {
		return 25 + attrAmount * VAR_ATTR_SPACE + if (attrAmount > 0) -1 else 0
	}

	def static int getTypeHeight(int attrAmount) {
		return 30 + attrAmount * ATTR_SPACE + if (attrAmount > 0) 7 else 0
	}
	
	def static int getHeight (int amount, Class<? extends Container> containerType) {
		if (SIB.isAssignableFrom(containerType)) {
			return getSIBHeight(amount)
		}
		else if (StartSIB.isAssignableFrom(containerType)) {
			return getSIBHeight(amount)
		}
		else if (EndSIB.isAssignableFrom(containerType)) {
			return getSIBHeight(amount)
		}
		else if (AbstractBranch.isAssignableFrom(containerType)) {
			return getBranchHeight(amount)
		}
		else if (Variable.isAssignableFrom(containerType)) {
			return getVariableHeight(amount)
		}
		else {
			// Should not happen...
			return 1337
		}
	}
	
}
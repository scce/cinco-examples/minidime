package info.scce.cinco.product.minidime.process.build

import info.scce.cinco.product.minidime.process.AtomicSIB
import info.scce.cinco.product.minidime.process.Input
import info.scce.cinco.product.minidime.process.Output
import info.scce.cinco.product.minidime.siblibrary.Branch
import info.scce.cinco.product.minidime.siblibrary.Port
import info.scce.cinco.product.minidime.siblibrary.SIB

/**
 * Copied from info.scce.dime.process.build.AtomicSIBBuild - Complete
 */
class AtomicSIBBuild extends SIBBuild<AtomicSIB, Port, Branch, Port> {
	
	new(AtomicSIB sib) {
		super(sib)
	}
	
	static def initialize(AtomicSIB sib) {
		new AtomicSIBBuild(sib).initialize
	}
	
	static def update(AtomicSIB sib) {
		new AtomicSIBBuild(sib).update
	}
	
	override getSIBReference(AtomicSIB sib) {
		sib.getSib
	}
	
	override isSIBReferenceValid(AtomicSIB sib) {
		true
	}
	
	override getSIBPortReferences(AtomicSIB sib) {
		(sib.getSib as SIB).inputPorts
	}
	
	override isSIBPortReference(Input port, Port portRef) {
		port.name == portRef.name
	}
	
	override getBranchReferences(AtomicSIB sib) {
		(sib.getSib as SIB).branches
	}
	
	override isBranchReference(info.scce.cinco.product.minidime.process.Branch branch, Branch branchRef) {
		branch.name == branchRef.name
	}
	
	override getBranchName(Branch branchRef) {
		branchRef.name
	}
	
	override getBranchPortReferences(Branch branchRef) {
		branchRef.outputPorts
	}
	
	override isBranchPortReference(Output branchPort, Port portRef) {
		branchPort.name == portRef.name
	}
	
}
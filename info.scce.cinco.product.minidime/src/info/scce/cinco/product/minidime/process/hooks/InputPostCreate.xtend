package info.scce.cinco.product.minidime.process.hooks

import info.scce.cinco.product.minidime.api.MiniDIMEPostCreateHook
import info.scce.cinco.product.minidime.process.EndSIB
import info.scce.cinco.product.minidime.process.Input
import info.scce.cinco.product.minidime.process.PrimitiveInputPort
import info.scce.cinco.product.minidime.process.JavaNativeInputPort
import info.scce.cinco.product.minidime.siblibrary.Type
import info.scce.cinco.product.minidime.process.SIB
import info.scce.cinco.product.minidime.process.PutToContextSIB

import static info.scce.cinco.product.minidime.process.helper.LayoutConstants.*

import static extension info.scce.cinco.product.minidime.process.helper.SIBLayoutUtils.*
import static extension info.scce.cinco.product.minidime.process.helper.NodeLayoutUtils.*

/**
 * Copied from info.scce.dime.process.hooks.InputHook - Complete
 */
class InputPostCreate extends MiniDIMEPostCreateHook<Input> {
	
	override void postCreate(Input port) {
		try {
			switch port.container {
				
				EndSIB: {
					val cEndSIB = port.container as EndSIB
     			
	     			// SIB's height is resized so that all input ports fit exactly into it
	     			val inputPortAmount = cEndSIB.inputs.size
	     			cEndSIB.resize(cEndSIB.width, getSIBHeight(inputPortAmount))
	     		
	     			val y = SIB_FIRST_PORT_Y + PORT_SPACE * (inputPortAmount - 1)
	     			port.moveTo(cEndSIB, PORT_X, y)
	     			switch port {
	     				PrimitiveInputPort:
	     					port.name = port.dataType.getName.toLowerCase + inputPortAmount
	     				JavaNativeInputPort:
	     					port.name = (port.dataType as Type).getName.toLowerCase + inputPortAmount
	     				default:
		     				throw new IllegalStateException('Reached else/default case in exhaustive if/switch. Please fix the code.')
	     			}
				}
				
				SIB: {
					if(port.container instanceof PutToContextSIB) {
     				
	     				// Add fitting output ports as well
	     				val ptc = port.getContainer as PutToContextSIB
	     				port.name = port.name + (ptc.inputs.size + 1)
	     				if (port instanceof PrimitiveInputPort) {
	     					val pip = port as PrimitiveInputPort
	     					ptc.branchSuccessors.forEach[n |
	     						val pop = n.newPrimitiveOutputPort(0, 0)
	     						pop.name = port.name
	     						pop.dataType = pip.dataType
	     					]
	     				}
	     			}
	     			port.container.resizeAndLayout
				}
				
			}
		} catch (Exception e) {
			e.printStackTrace
		}
	}
	
}
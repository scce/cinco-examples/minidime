package info.scce.cinco.product.minidime.process.checks

import info.scce.cinco.product.minidime.checks.AbstractCheck
import info.scce.cinco.product.minidime.mcam.adapter.MiniDIMEProcessAdapter
import info.scce.cinco.product.minidime.mcam.adapter.MiniDIMEProcessId
import info.scce.cinco.product.minidime.process.TextInputStatic
import java.util.List

/**
 * Copied from info.scce.dime.process.checks.InputStaticCheck - Complete
 */
class InputStaticCheck extends AbstractCheck<MiniDIMEProcessId, MiniDIMEProcessAdapter> {
	
	public static List<String> invalid             = #['"', "\\'"]
	public static List<String> invalidWithMustache = #['"', "\\'", '{{', '}}']
	public static List<String> escapable           = #[]
	
	override void doExecute(MiniDIMEProcessAdapter adapter) {
		for (id : adapter.entityIds) {
			val obj = id.element
			if (obj instanceof TextInputStatic) {
				checkText(id, obj)	
			}
		}
	}
	
	override void init() {
		// Intentionally left blank
	}
	
	def private void checkText(MiniDIMEProcessId id, TextInputStatic te) {
		val value = te.value
		if (value !== null) {
		
			// Check invalid characters
			for(i: invalid) {
				if(value.contains(i)) {
					addError(id, '''value contains illegal chars: «i»''')
				}
			}

			// Check escapable characters
			for(i: escapable){
				var subString = value
				while (subString.indexOf(i) >= 0) {
					val indexOf = subString.indexOf(i)
					if (indexOf > 0){
					 	if (subString.charAt(indexOf - 1) != '\\') {
							addError(id, '''value char: "«i»" has to be escaped''')
						}
					}
					else {
						addError(id, '''value char: "«i»" has to be escaped''')
					}
					subString = subString.substring(indexOf + 1)
				}
			}
		}
	}
	
}

package info.scce.cinco.product.minidime.process.checks

import info.scce.cinco.product.minidime.checks.AbstractCheck
import info.scce.cinco.product.minidime.mcam.adapter.MiniDIMEProcessAdapter
import info.scce.cinco.product.minidime.mcam.adapter.MiniDIMEProcessId
import info.scce.cinco.product.minidime.process.EndSIB
import info.scce.cinco.product.minidime.process.Variable

/**
 * Copied from info.scce.dime.process.checks.NotAllowedNamesCheck - Complete
 */
class NotAllowedNamesCheck extends AbstractCheck<MiniDIMEProcessId, MiniDIMEProcessAdapter> {
	
	val notAllowedVariableNames   = #['var']
	val notAllowedEndSibPortNames = #['var', 'iteratorElement']
	
	override void doExecute(MiniDIMEProcessAdapter adapter) {
		for (id : adapter.entityIds) {
			val obj = id.element
			switch obj {
				Variable case notAllowedVariableNames.contains(obj.name):
					addError(id, '''«obj.name» not allowed''')
				EndSIB:
					obj.inputs.forEach[
						if (notAllowedEndSibPortNames.contains(name)) {
							addError(id, '''«name» not allowed''')
						}
					]
			}
		}
	}
	
	override void init() {
		// Intentionally left blank
	}
	
}

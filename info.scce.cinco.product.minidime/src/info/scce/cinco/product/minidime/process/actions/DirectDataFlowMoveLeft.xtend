package info.scce.cinco.product.minidime.process.actions

import info.scce.cinco.product.minidime.api.MiniDIMECustomAction
import info.scce.cinco.product.minidime.process.DirectDataFlow

import static extension info.scce.cinco.product.minidime.process.helper.EdgeLayoutUtils.*

/**
 * Copied from info.scce.dime.process.actions.DirectDataFlowToLeft - Complete
 */
class DirectDataFlowMoveLeft<T extends DirectDataFlow> extends MiniDIMECustomAction<DirectDataFlow> {
	
	override getName() {
		'Move left'
	}
	
	override canExecute(DirectDataFlow edge) {
		!edge.bendpoints.empty
	}

	override execute(DirectDataFlow edge) {
		val bps = edge.bendpoints
		for (i: 0 ..< bps.size) {
			val bp = bps.get(i)
			edge.moveBendpoint(i, bp.x - 10, bp.y)
		}
	}
	
}

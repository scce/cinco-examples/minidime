package info.scce.cinco.product.minidime.process.aps

import de.jabc.cinco.meta.core.ge.style.generator.runtime.appearance.StyleAppearanceProvider
import graphmodel.ModelElement
import style.StyleFactory
import info.scce.cinco.product.minidime.process.MiniDIMEProcess

/**
 * Copied from info.scce.dime.process.aps.DataFlowAppearance - Complete
 */
class DataFlowAppearance implements StyleAppearanceProvider<ModelElement> {
	
	extension StyleFactory = StyleFactory.eINSTANCE
	
	override getAppearance(ModelElement element, String elementName) {
		val appearance = createAppearance
		val processModel = element.rootElement as MiniDIMEProcess
		val df = if (processModel !== null) processModel.isDataFlowView else true
		appearance.transparency = if (df) 0.0 else 1.0
		return appearance
	}
	
}

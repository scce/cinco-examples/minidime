package info.scce.cinco.product.minidime.process.aps

import de.jabc.cinco.meta.core.ge.style.generator.runtime.appearance.StyleAppearanceProvider
import info.scce.cinco.product.minidime.process.AbstractBranch
import style.StyleFactory

/**
 * Copied from info.scce.dime.process.aps.BranchHLineAppearance - Complete
 */
class BranchHLineAppearance implements StyleAppearanceProvider<AbstractBranch> {
	
	extension StyleFactory = StyleFactory.eINSTANCE

	val visible = createColor => [
		r = 0
		g = 0
		b = 0
	]
	
	val invisible = createColor => [
		r = 245
		g = 245 
		b = 245
	]

	override getAppearance(AbstractBranch branch, String elementName) {
		val foregroundColor = if (!branch.rootElement.isDataFlowView || branch.outputPorts.empty) {
			invisible
		}
		else {
			visible
		}
		if (elementName == "hline") {
			return createAppearance => [foreground = foregroundColor]
		}
		else {
			return null
		}
	}
	
}

package info.scce.cinco.product.minidime.process.actions

import info.scce.cinco.product.minidime.api.MiniDIMECustomAction
import info.scce.cinco.product.minidime.process.DataFlowTarget

/**
 * Copied from info.scce.dime.process.actions.EndSIBNewPrimitiveOutput - Complete
 */
class DataFlowTargetNewPrimitiveOutput extends MiniDIMECustomAction<DataFlowTarget> {
	
	override getName() {
		'New Primitive Output'
	}

	override execute(DataFlowTarget dft) {
		try {
			// Layout and resize is then automatically done by OutputPostCreate
			dft.newPrimitiveInputPort(1, 1)
		}
		catch (Exception e) {
			e.printStackTrace
		}
	}
	
}

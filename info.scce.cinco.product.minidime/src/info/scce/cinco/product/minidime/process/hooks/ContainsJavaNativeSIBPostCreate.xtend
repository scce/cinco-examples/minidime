package info.scce.cinco.product.minidime.process.hooks

import info.scce.cinco.product.minidime.process.ContainsJavaNativeSIB
import info.scce.cinco.product.minidime.siblibrary.Type

/**
 * Copied from info.scce.dime.process.hooks.ContainsJavaNativeSIBHook - Complete
 */
class ContainsJavaNativeSIBPostCreate extends AbstractSIBPostCreate<ContainsJavaNativeSIB> {
	
	override boolean initialize(ContainsJavaNativeSIB sib) {
   		val listType = sib.listType as Type
   		label = '''«listType.name» List Contains'''
   		addJavaNativeInputPort('list', listType, true)
   		addJavaNativeInputPort('element', listType, false)
   		addBranch('yes')
   		addBranch('no')
   		return true
	}
	
}

package info.scce.cinco.product.minidime.process.actions

import info.scce.cinco.product.minidime.process.PutToContextSIB
import info.scce.cinco.product.minidime.process.build.PutToContextSIBBuild

/**
 * Copied from info.scce.dime.process.actions.UpdatePutToContextSIB - Complete
 */
class PutToContextSIBUpdate extends SIBUpdate<PutToContextSIB> {
	
	override update(PutToContextSIB sib) {
		PutToContextSIBBuild.update(sib)
	}
	
}

package info.scce.cinco.product.minidime.process.actions

import info.scce.cinco.product.minidime.api.MiniDIMECustomAction
import info.scce.cinco.product.minidime.process.SIB

/**
 * Copied from info.scce.dime.process.actions.UpdateSIBAction - Complete
 */
abstract class SIBUpdate<T extends SIB> extends MiniDIMECustomAction<T>{
	
	override getName() {
		'Update SIB'
	}
	
	override execute(T sib) {
		update(sib)
	}
	
	def void update(T sib)
	
}

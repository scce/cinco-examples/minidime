package info.scce.cinco.product.minidime.process.hooks

import graphmodel.Direction
import info.scce.cinco.product.minidime.process.AbstractBranch

import static info.scce.cinco.product.minidime.process.helper.LayoutConstants.*

/**
 * Copied from info.scce.dime.process.hooks.BranchPostResize - Complete
 */
class BranchPostResize extends ProcessNodePostResize<AbstractBranch> {
	
	override void postResize(AbstractBranch cModelElement, int oldWidth, int oldHeight, int oldX, int oldY, Direction direction) {
		super.postResize(cModelElement, oldWidth, oldHeight, oldX, oldY, direction)
		for (o : cModelElement.outputs) {
			o.resize(cModelElement.width - oldWidth - PORT_X * 2, o.height)
		}
	}
	
}

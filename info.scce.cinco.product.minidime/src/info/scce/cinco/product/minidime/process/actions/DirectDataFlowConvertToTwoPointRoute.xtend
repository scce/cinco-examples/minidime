package info.scce.cinco.product.minidime.process.actions

import info.scce.cinco.product.minidime.api.MiniDIMECustomAction
import info.scce.cinco.product.minidime.process.DirectDataFlow

import static extension info.scce.cinco.product.minidime.process.helper.EdgeLayoutUtils.*

/**
 * Copied from info.scce.dime.process.actions.SimpleTwoPointRoute - Complete
 */
class DirectDataFlowConvertToTwoPointRoute<T extends DirectDataFlow> extends MiniDIMECustomAction<DirectDataFlow> {
	
	override getName() {
		'Convert to Two-Point Route'
	}
	
	override execute(DirectDataFlow edge) {
		val source = edge.sourceElement
		val target = edge.targetElement
		for (i: 0 ..< edge.bendpoints.size)
			edge.removeBendpoint(0)
		for (point : getManhattanPoints(source, target)) {
			edge.addBendpoint(point.x, point.y)
		}
	}
	 
}

package info.scce.cinco.product.minidime.process.actions

import info.scce.cinco.product.minidime.api.MiniDIMECustomAction
import info.scce.cinco.product.minidime.process.DataFlowSource

/**
 * Copied from info.scce.dime.process.actions.StartSIBNewPrimitiveInput - Complete
 */
class DataFlowSourceNewPrimitiveInput extends MiniDIMECustomAction<DataFlowSource> {
	
	override getName() {
		'New Primitive Input'
	}

	override execute(DataFlowSource dfs) {
		try {
			dfs.newPrimitiveOutputPort(1, 1)
		}
		catch (Exception e) {
			e.printStackTrace
		}
	}
	
}

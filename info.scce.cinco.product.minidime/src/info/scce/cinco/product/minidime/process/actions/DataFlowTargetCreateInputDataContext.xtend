package info.scce.cinco.product.minidime.process.actions

import de.jabc.cinco.meta.runtime.action.CincoCustomAction
import graphmodel.Node
import info.scce.cinco.product.minidime.process.DataContext
import info.scce.cinco.product.minidime.process.DataFlowTarget
import info.scce.cinco.product.minidime.process.InputPort
import info.scce.cinco.product.minidime.process.OutputPort
import info.scce.cinco.product.minidime.process.PrimitiveInputPort
import info.scce.cinco.product.minidime.process.PrimitiveOutputPort
import info.scce.cinco.product.minidime.process.PrimitiveVariable
import info.scce.cinco.product.minidime.process.Variable
import info.scce.cinco.product.minidime.process.MiniDIMEProcess

/**
 * Copied from info.scce.dime.process.actions.CreateInputDataContext - Complete
 */
class DataFlowTargetCreateInputDataContext extends CincoCustomAction<DataFlowTarget> {
	
	var xOffset = 10
	
	override getName() {
		'Create input data context'
	}
	
	override execute(DataFlowTarget sib) {
		if (sib.container instanceof MiniDIMEProcess) {
			val container = sib.container as MiniDIMEProcess
			val width = sib.inputPorts.size * 10 + 20
			val dataContext = container.newDataContext(sib.x - width, sib.y, width, sib.height)
			for (port : sib.inputPorts) {
				val variable = dataContext.newVariable(port) => [
					move(xOffset, y)
					resize(10,10)
					name = port.name + port.id
					isList = port.isList
				]
				val source = port.incoming.head?.sourceElement
				if (source instanceof OutputPort) {
					port.incoming.head.delete
					source.newUpdate(variable)
				}
				variable.newRead(port)
				xOffset += 10
			}
		}
	}
		
	dispatch def newVariable(DataContext dataContext, PrimitiveInputPort port) {
		dataContext.newPrimitiveVariable(xOffset, port.alignedY) => [
			dataType = port.dataType
		]
	}
	
	dispatch def newVariable(DataContext dataContext, InputPort port) {
		warn('''Unmatched input port: «port»''')
	}
	
	dispatch def newUpdate(PrimitiveOutputPort source, PrimitiveVariable target) {
		source.newPrimitiveUpdate(target)
	}
	
	dispatch def newUpdate(OutputPort source, Variable target) {
		warn('''Unmatched types: «source.class» and «target.class»''')
	}
	
	dispatch def newRead(PrimitiveVariable source, PrimitiveInputPort target) {
		source.newPrimitiveRead(target)
	}
	
	dispatch def newRead(Variable source, InputPort target) {
		warn('''Unmatched types: «source.class» and «target.class»''')
	}
	
	def getAlignedY(Node node) {
		node.y + node.height / 2 - 5
	}
	
}

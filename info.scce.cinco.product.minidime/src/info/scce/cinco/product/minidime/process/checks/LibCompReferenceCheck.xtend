package info.scce.cinco.product.minidime.process.checks

import graphmodel.GraphModel
import graphmodel.ModelElement
import info.scce.cinco.product.minidime.checks.AbstractCheck
import info.scce.cinco.product.minidime.mcam.adapter.MiniDIMEProcessAdapter
import info.scce.cinco.product.minidime.mcam.adapter.MiniDIMEProcessId

/**
 * Copied from info.scce.dime.process.checks.LibCompReferenceCheck - Complete
 */
class LibCompReferenceCheck extends AbstractCheck<MiniDIMEProcessId, MiniDIMEProcessAdapter> {
	
	override void doExecute(MiniDIMEProcessAdapter adapter) {
		val eObj = adapter.model
		if (eObj instanceof GraphModel) {
			val iterator = eObj.eAllContents
			while (iterator.hasNext) {
				val obj = iterator.next
				if (obj instanceof ModelElement) {
					val libCompFeature = obj.eClass.getEStructuralFeature('libraryComponentUID')
					if (libCompFeature !== null) {
						val value = obj.eGet(libCompFeature)
						if (value === null) {
							addError(adapter.getIdByString(obj.id), 'Reference is NULL')
						}
					}
				}
			}
		}
	}
	
	override void init() {
		// Intentionally left blank
	}
	
}

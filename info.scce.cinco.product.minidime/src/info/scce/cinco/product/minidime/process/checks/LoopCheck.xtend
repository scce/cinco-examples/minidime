package info.scce.cinco.product.minidime.process.checks

import graphmodel.Node
import info.scce.cinco.product.minidime.checks.AbstractCheck
import info.scce.cinco.product.minidime.mcam.adapter.MiniDIMEProcessAdapter
import info.scce.cinco.product.minidime.mcam.adapter.MiniDIMEProcessId
import info.scce.cinco.product.minidime.process.AbstractIterateSIB
import info.scce.cinco.product.minidime.process.Branch
import info.scce.cinco.product.minidime.process.ControlFlow
import info.scce.cinco.product.minidime.process.DataFlowSource
import info.scce.cinco.product.minidime.process.DataFlowTarget
import info.scce.cinco.product.minidime.process.MiniDIMEProcess
import java.util.ArrayList
import java.util.HashMap

/**
 * Coped from info.scce.dime.process.checks.LoopCheck - Complete
 */
class LoopCheck extends AbstractCheck<MiniDIMEProcessId, MiniDIMEProcessAdapter> {
	
	var MiniDIMEProcessAdapter adapter
	var ArrayList<Node> knownElements    = new ArrayList
	var HashMap<Node, Integer> loopLevel = new HashMap

	override void doExecute(MiniDIMEProcessAdapter adapter) {
		this.adapter = adapter
		for (id : adapter.entityIds) {
			val obj = id.element
			if (obj instanceof MiniDIMEProcess) {
				obj.calculateLoopAssociation
				obj.analyzeLoopAssociation
			}
		}
		processResults
	}
	
	override void init() {
		knownElements.clear
		loopLevel.clear
	}

	def private void analyzeLoopAssociation(MiniDIMEProcess process) {
		for (node : loopLevel.keySet) {
			if (node instanceof DataFlowTarget) {
				for (edge : node.getIncoming(ControlFlow)) {
					val preNode = edge.sourceElement
					if (loopLevel.get(node) !== null
							&& loopLevel.get(preNode) !== null
							&& loopLevel.get(node) < loopLevel.get(preNode)) {
						addWarning(adapter.getIdByString(preNode.id), 'Eary loop exit detected')
					}
				}
			}
		}
	}

	def private void calculateLoopAssociation(MiniDIMEProcess process) {
		for (startSib : process.startSIBs) {
			loopLevel.put(startSib, 0)
			for (dft : startSib.getSuccessors(DataFlowTarget)) {
				traverseAssociation(dft, loopLevel.get(startSib))
			}
		}
	}

	def private void traverseAssociation(DataFlowTarget dft, Integer level) {
		// Already visited?
		if (knownElements.contains(dft)) { return }
		knownElements.add(dft)

		// Set loop information
		loopLevel.put(dft, level)
//		if (dft instanceof IterateSIB) {			// Original code
		if (dft instanceof AbstractIterateSIB) {	// Fabian's code
			loopLevel.put(dft, level + 1)
		}

		for (dfs : dft.getSuccessors(DataFlowSource)) {
			loopLevel.put(dfs, loopLevel.get(dft))
//			if (dft instanceof IterateSIB && dfs instanceof Branch) {			// Original code
			if (dft instanceof AbstractIterateSIB && dfs instanceof Branch) {	// Fabian's code
				val branch = dfs as Branch
				if (branch.name != 'next') {
					loopLevel.put(dfs, loopLevel.get(dft) - 1)
				}
			}
			for (dftSucc : dfs.getSuccessors(DataFlowTarget)) {
				traverseAssociation(dftSucc, loopLevel.get(dfs))
			}
		}
	}
	
}

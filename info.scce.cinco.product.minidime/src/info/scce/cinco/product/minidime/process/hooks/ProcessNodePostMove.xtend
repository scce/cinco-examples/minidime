package info.scce.cinco.product.minidime.process.hooks

import graphmodel.ModelElement
import graphmodel.ModelElementContainer
import graphmodel.Node
import graphmodel.internal.InternalModelElement
import info.scce.cinco.product.minidime.api.MiniDIMEPostMoveHook
import info.scce.cinco.product.minidime.process.DataContext
import org.eclipse.swt.widgets.Display

import static java.lang.Math.abs
import static java.lang.Math.max
import static java.lang.Math.min

/**
 * Copied from info.scce.dime.process.hooks.ProcessNodePostMove - Complete
 */
abstract class ProcessNodePostMove<T extends ModelElement> extends MiniDIMEPostMoveHook<T> {
	
	override postMove(T node,
		              ModelElementContainer sourceContainer,
		              ModelElementContainer targetContainer,
		              int x, int y, int deltaX, int deltaY) {
		if (node instanceof Node && targetContainer == node.rootElement) {
			updateDataContext(node as Node, deltaY)
		}
	}
	
	def updateDataContext(Node movedNode, int deltaY) {
		if (Display.current === null) {
			return // Only resize if triggered by changes in the UI editor
		}
		val model = movedNode.rootElement
		val nodes = model.allNodes.drop(DataContext)
		if (!nodes.nullOrEmpty) {
			val topNode = nodes.sortBy[y].head
			val movedNodeOldY = movedNode.y - deltaY
			val scndTopNode = nodes.filter[it != movedNode].sortBy[y].head ?: topNode
			val oldMin = min(movedNodeOldY, scndTopNode.y)
			val botNode = nodes.sortBy[y + height].reverse.head
			val scndBotNode = nodes.filter[it != movedNode].sortBy[y + height].reverse.head ?: botNode
			val oldMax = max(movedNodeOldY + movedNode.height, scndBotNode.y + scndBotNode.height)
			val selection = model.diagram.diagramBehavior.selectedPictogramElements.map[businessObject].filter(InternalModelElement).map[element].toList
			for (dataContext : model.getNodes(DataContext)) {
				val selected = selection.contains(dataContext)
				if (!selected) {
					val oldDataContextBot = dataContext.y + dataContext.height
					if (abs(oldMin - dataContext.y) < 2) {
						// Data context has been aligned to the y of the top node -> Resize
						val dY = topNode.y - dataContext.y
						if (dY != 0) {
							println('''Resize data context y/height from «dataContext.y»/«dataContext.height» to «topNode.y»/«(dataContext.height - dY)» by dY=«dY»''')
							dataContext.y = topNode.y
							for (innerNode : dataContext.allNodes) {
								innerNode.move(innerNode.x, max(20, innerNode.y - dY))
							}
							dataContext.height = dataContext.height - dY
						}
					}
					val minHeight = (dataContext.allNodes.map[y + height].sort.reverse.head ?: 0) + 17
					if (abs(oldMax - oldDataContextBot) < 2 || dataContext.height == minHeight) {
						// Data context has been aligned to the bottom of the bottom node -> Resize
						val dY2 = botNode.y + botNode.height - dataContext.y - dataContext.height
						if (dY2 != 0) {
							println('''Resize data context height from «dataContext.height» to «botNode.y + botNode.height - dataContext.y» by dY=«dY2»''')
							dataContext.height = max(minHeight, dataContext.height + dY2)
						}
					}
				}
			}
		}
	}
	
}

package info.scce.cinco.product.minidime.process.helper

/**
 * Copied from info.scce.dime.process.helper.LayoutConstants - Complete
 */
class LayoutConstants {
	
	// CONSTANTS FOR PROCESS MODEL LAYOUT	
	public static final int SIB_WIDTH = 120
	public static final int SIB_FIRST_PORT_Y = 67
	public static final int BRANCH_FIRST_PORT_Y = 27
	public static final int PORT_X = 5

	/**
	 * Distance between ports (10 is port width)
	 */
	public static final int PORT_SPACE = 18
	
	public static final int BRANCH_WIDTH = 120
	
	/** 
	 * How far from the SIB the branches are inserted
	 */
	public static final int BRANCH_H_DISTANCE = 70
	public static final int BRANCH_V_DISTANCE = 10
	public static final int BRANCH_H_SPACE = 40
	
	/**
	 * The number of branches to be centrally aligned with the SIB
	 */
	public static final int BRANCH_THRESHOLD = 4
	
	// PROCESS MODEL DATA CONTEXT
	public static final int VAR_ATTR_X = 10
	public static final int VAR_FIRST_ATTR_Y = 23
	public static final int VAR_ATTR_SPACE = 27
	
	// CONSTANTS FOR DATA MODEL LAYOUT
	public static final int ATTR_X = 5
	public static final int ATTR_SPACE = 18
	public static final int TYPE_FIRST_ATTR_Y = 32
	
}
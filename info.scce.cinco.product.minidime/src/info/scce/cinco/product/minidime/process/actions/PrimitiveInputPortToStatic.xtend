package info.scce.cinco.product.minidime.process.actions

import info.scce.cinco.product.minidime.api.MiniDIMECustomAction
import info.scce.cinco.product.minidime.process.Input
import info.scce.cinco.product.minidime.process.PrimitiveInputPort

import static extension info.scce.cinco.product.minidime.process.helper.PortUtils.*

/**
 * Copied from info.scce.dime.process.actions.PrimitivePortToStatic - Complete
 */
class PrimitiveInputPortToStatic extends MiniDIMECustomAction<PrimitiveInputPort> {
	
	override getName() {
		'Convert Input to Static Value'
	}

	override execute(PrimitiveInputPort port) {
		try {
			val container = port.container
			val oldPorts = newArrayList
			// Re-add all ports to preserve their order
			container.allNodes.filter(Input).forEach[oldPort |
				if (oldPort === port) {
					container.addStaticPort(port)
				} else {
					var oldClone = oldPort.clone(container)
					oldPort.reconnectIncoming(oldClone)
				}
				oldPorts.add(oldPort)
			]
			oldPorts.forEach[delete]
		}
		catch (Exception e) {
			e.printStackTrace
		}
	}
	
}
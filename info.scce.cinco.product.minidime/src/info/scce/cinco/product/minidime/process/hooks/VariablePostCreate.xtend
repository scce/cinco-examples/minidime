package info.scce.cinco.product.minidime.process.hooks

import de.jabc.cinco.meta.runtime.hook.CincoPostCreateHook
import info.scce.cinco.product.minidime.process.DataContext
import info.scce.cinco.product.minidime.process.Variable

/**
 * Copied from info.scce.dime.process.hooks.Variable_PostCreate - Complete
 */
class VariablePostCreate extends CincoPostCreateHook<Variable> {
	
	static final int OFFSET = 30
	
	override postCreate(Variable node) {
		if(node.container instanceof DataContext) {
			var dataContext = node.container as DataContext
			dataContext.height = dataContext.maxHeight + OFFSET
		}
	}
	
	def maxHeight(DataContext context) {
		var maxLowerBound = 50
		for(node : context.nodes) {
			if(node.y + node.height > maxLowerBound) {
				maxLowerBound = node.y + node.height 
			}
		}
		return maxLowerBound
	}
	
}

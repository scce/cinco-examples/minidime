package info.scce.cinco.product.minidime.process.checks

import info.scce.cinco.product.minidime.checks.AbstractModelIntegrityCheck
import info.scce.cinco.product.minidime.mcam.adapter.MiniDIMEProcessAdapter
import info.scce.cinco.product.minidime.mcam.adapter.MiniDIMEProcessId

/**
 * Copied from info.scce.dime.process.checks.ModelIntegrityCheck - Complete
 */
class ModelIntegrityCheck extends AbstractModelIntegrityCheck<MiniDIMEProcessId, MiniDIMEProcessAdapter> {
	// Intentionally left blank
}

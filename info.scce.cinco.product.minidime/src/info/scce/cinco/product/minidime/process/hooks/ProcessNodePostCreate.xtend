package info.scce.cinco.product.minidime.process.hooks

import graphmodel.ModelElement
import graphmodel.Node
import info.scce.cinco.product.minidime.api.MiniDIMEPostCreateHook
import info.scce.cinco.product.minidime.process.DataContext
import org.eclipse.swt.widgets.Display

import static java.lang.Math.abs
import static java.lang.Math.max
import info.scce.cinco.product.minidime.process.MiniDIMEProcess

/**
 * Copied from info.scce.dime.process.hooks.ProcessNodePostCreate - Complete
 */
class ProcessNodePostCreate<T extends ModelElement> extends MiniDIMEPostCreateHook<T> {
	
	override postCreate(T it) {
		
		// Modified by Fabian Storek:
		// Set the model name of the parent Process, if it has no or the default model name.
		val process = rootElement as MiniDIMEProcess
		if (process.getModelName.nullOrEmpty || process.getModelName == 'unnamed model') {
			val fileName = process.file.fullPath.lastSegment.replaceAll('\\.process$', '')
			if (!fileName.nullOrEmpty && fileName != 'unnamed model') {
				process.modelName = fileName
				process.save
			}
		}
		// End modified
		
		if (it instanceof Node) {
			updateDataContext(it)
		}
	}
	
	def updateDataContext(Node movedNode) {
		if (Display.current === null) {
			return // only resize if triggered by changes in the UI editor
		}
		val model = movedNode.rootElement
		val nodes = model.allNodes.drop(DataContext)
		if (!nodes.nullOrEmpty) {
			val topNode = nodes.sortBy[y].head
			val scndTopNode = nodes.filter[it != movedNode].sortBy[y].head ?: topNode
			val oldMin = scndTopNode.y
			val botNode = nodes.sortBy[y + height].reverse.head
			val scndBotNode = nodes.filter[it != movedNode].sortBy[y + height].reverse.head ?: botNode
			val oldMax = scndBotNode.y + scndBotNode.height
			for (dataContext : model.getNodes(DataContext)) {
				val oldDataContextBot = dataContext.y + dataContext.height
				if (abs(oldMin - dataContext.y) < 2) {
					// data context has been aligned to the y of the top node => resize
					val dY = topNode.y - dataContext.y
					if (dY != 0) {
						println('''Resize data context y/height from «dataContext.y»/«dataContext.height» to «topNode.y»/«(dataContext.height - dY)» by dY=«dY»''')
						dataContext.y = topNode.y
						for (innerNode : dataContext.allNodes) {
							innerNode.move(innerNode.x, max(20, innerNode.y - dY))
						}
						dataContext.height = dataContext.height - dY
					}
				}
				val minHeight = (dataContext.allNodes.map[y + height].sort.reverse.head ?: 0) + 17
				if (abs(oldMax - oldDataContextBot) < 2 || dataContext.height == minHeight) {
					// data context has been aligned to the bottom of the bottom node => resize
					val dY2 = botNode.y + botNode.height - dataContext.y - dataContext.height
					if (dY2 != 0) {
						println('''Resize data context height from «dataContext.height» to «botNode.y + botNode.height - dataContext.y» by dY=«dY2»''')
						dataContext.height = max(minHeight, dataContext.height + dY2)
					}
				}
			}
		}
	}
	
}
package info.scce.cinco.product.minidime.process.hooks

import info.scce.cinco.product.minidime.api.MiniDIMEPostCreateHook
import info.scce.cinco.product.minidime.process.DirectDataFlow
import info.scce.cinco.product.minidime.process.helper.EdgeLayoutUtils.Location

import static extension info.scce.cinco.product.minidime.process.helper.EdgeLayoutUtils.*

/**
 * Copied from info.scce.dime.process.hooks.DirectDataFlowPostCreate - Complete
 */
class DirectDataFlowPostCreate extends MiniDIMEPostCreateHook<DirectDataFlow> {
	
	override postCreate(DirectDataFlow edge) {
		for (Location p : getManhattanPoints(edge.sourceElement, edge.targetElement)) {
			edge.addBendpoint(p.x, p.y)
		}
	}
	
}
package info.scce.cinco.product.minidime.process.checks

import graphmodel.Edge
import info.scce.cinco.product.minidime.process.DataFlow
import info.scce.cinco.product.minidime.process.JavaNativeDirectDataFlow
import info.scce.cinco.product.minidime.process.JavaNativeRead
import info.scce.cinco.product.minidime.process.JavaNativeUpdate
import info.scce.cinco.product.minidime.process.PrimitiveDirectDataFlow
import info.scce.cinco.product.minidime.process.PrimitiveRead
import info.scce.cinco.product.minidime.process.PrimitiveUpdate
import info.scce.cinco.product.minidime.process.Variable
import info.scce.cinco.product.minidime.checks.ProcessCheck
import info.scce.cinco.product.minidime.process.MiniDIMEProcess

/**
 * Copied from info.scce.dime.process.checks.DataFlowTypeCheck - Complete
 */
class DataFlowTypeCheck extends ProcessCheck {
	
	override void check(MiniDIMEProcess model) {
		model.find(DataFlow).forEach[
			switch it {
				PrimitiveRead,  PrimitiveUpdate,  PrimitiveDirectDataFlow:  checkPrimitive
				JavaNativeRead, JavaNativeUpdate, JavaNativeDirectDataFlow: checkNative
				default: throw new SwitchException
			}
		]
	}
	
	def checkPrimitive(Edge it) {
		val srcType = sourceElement.primitiveType
		val tgtType = targetElement.primitiveType
		check[
			srcType == tgtType
		].elseError('''«srcType» != «tgtType»''')
		checkListState
	}
	
	def checkNative(Edge it) {
		val srcType = sourceElement.nativeType
		val tgtType = targetElement.nativeType
		check[
			srcType.name == tgtType.name
		].elseError('''«srcType» != «tgtType»''')
		checkListState
	}
	
	def private void checkListState(Edge it) {
		val source = sourceElement
		val target = targetElement
		if (target instanceof Variable) {
			check[
				target.isListType || !source.isListType
			].elseError(
				if (source.isListType) 'target should be list'
				else 'target should not be list'
			)
		}
		else check[
			sourceElement.isListType == targetElement.isListType
		].elseError(
			'source and target should be lists, or none of them'
		)
	}
	
}

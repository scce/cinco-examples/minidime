package info.scce.cinco.product.minidime.process.checks

import info.scce.cinco.product.minidime.checks.AbstractCheck
import info.scce.cinco.product.minidime.mcam.adapter.MiniDIMEProcessAdapter
import info.scce.cinco.product.minidime.mcam.adapter.MiniDIMEProcessId
import info.scce.cinco.product.minidime.process.AtomicSIB
import info.scce.cinco.product.minidime.process.Branch
import info.scce.cinco.product.minidime.process.IO
import info.scce.cinco.product.minidime.process.Input
import info.scce.cinco.product.minidime.process.InputStatic
import info.scce.cinco.product.minidime.process.JavaNativeInputPort
import info.scce.cinco.product.minidime.process.JavaNativeOutputPort
import info.scce.cinco.product.minidime.process.Output
import info.scce.cinco.product.minidime.process.PrimitiveInputPort
import info.scce.cinco.product.minidime.process.PrimitiveOutputPort
import info.scce.cinco.product.minidime.process.helper.ProcessExtension
import info.scce.cinco.product.minidime.siblibrary.JavaTypePort
import info.scce.cinco.product.minidime.siblibrary.Port
import info.scce.cinco.product.minidime.siblibrary.PrimitivePort
import info.scce.cinco.product.minidime.siblibrary.SIB
import java.util.ArrayList

/**
 * Copied from info.scce.dime.process.checks.AtomicInterfaceCheck - Complete
 */
class AtomicInterfaceCheck extends AbstractCheck<MiniDIMEProcessId, MiniDIMEProcessAdapter> {
	
	extension ProcessExtension = new ProcessExtension

	var MiniDIMEProcessAdapter adapter

	override void doExecute(MiniDIMEProcessAdapter adapter) {
		this.adapter = adapter
		for (id : adapter.entityIds) {
			val obj = id.element
			if (obj instanceof AtomicSIB) {
				checkInputs(id, obj)
				checkBranches(id, obj)
			}
		}
		processResults
	}
	
	override void init() {
		// Intentionally left blank
	}

	def private void checkInputs(MiniDIMEProcessId id, AtomicSIB sib) {
		val inputs = new ArrayList(sib.inputs)
		val origSib = sib.sib as SIB
		if (origSib === null) {
			addError(id, '''SIB Reference of "«sib.label»" not found.''')
			return
		}
		for (port : origSib.inputPorts) {
			var count = 0
			var Input oInput = null
			for (input : inputs) {
				if (input.name == port.name) {
					oInput = input
					count++
				}
			}
			if (count > 1) {
				addError(id, '''Input "«port.name»" exists «count» times''')
			}
			else if (count <= 0) {
				addError(id, '''Input "«port.name»" not found''')
			}
			else {
				checkTypeOfIO(adapter.getIdByString(oInput.id), oInput, port)
				inputs.remove(oInput)
			}
		}
		for (input : inputs) {
			addError(id, '''Reference for "«input.name»" not found''')
		}
	}

	def private void checkBranches(MiniDIMEProcessId sibId, AtomicSIB sib) {
		val branches = new ArrayList(sib.getSuccessors(Branch))
		val origSib = sib.sib as SIB
		if (origSib === null) {
			addError(sibId, '''SIB Reference of «sib.label» not found.''')
			return
		}
		for (info.scce.cinco.product.minidime.siblibrary.Branch origBranch : origSib.branches) {
			var count = 0
			var Branch oBranch = null
			for (Branch branch : branches) {
				if (branch.name == origBranch.name) {
					oBranch = branch
					count++
				}
			}
			if (count > 1) {
				addError(sibId, '''Branch "«origBranch.name»" exists «count» times''')
			}
			else if (count <= 0) {
				addWarning(sibId, '''Branch "«origBranch.name»" not found''')
			}
			else {
				checkOutputs(adapter.getIdByString(oBranch.id), oBranch, origBranch)
				branches.remove(oBranch)
			}
		}
		for (branch : branches) {
			addError(sibId, '''Reference for Branch "«branch.name»" not found''')
		}
	}

	def private void checkOutputs(MiniDIMEProcessId branchId, Branch branch, info.scce.cinco.product.minidime.siblibrary.Branch origBranch) {
		val outputs = new ArrayList(branch.outputs)
		for (origPort : origBranch.outputPorts) {
			var count = 0
			var Output oOutput = null
			for (output : outputs) {
				if (output.name == origPort.name) {
					oOutput = output
					count++
				}
			}
			if (count > 1) {
				addError(branchId, '''Output "«origPort.name»" exists «count»" times''')
			}
			else if (count <= 0) {
				addError(branchId, '''Output "«origPort.name»" not found''')
			}
			else {
				checkTypeOfIO(adapter.getIdByString(oOutput.id), oOutput, origPort)
				outputs.remove(oOutput)
			}
		}
		for (output : outputs) {
			addError(branchId, '''Reference for "«output.name»" not found''')
		}
	}

	def private void checkTypeOfIO(MiniDIMEProcessId thisId, IO thisIO, Port port) {
		val String thisType = switch thisIO {
			PrimitiveOutputPort,
			PrimitiveInputPort:   thisIO.primitiveTypeString
			JavaNativeOutputPort,
			JavaNativeInputPort:  thisIO.nativeTypeString
			InputStatic:          thisIO.staticTypeString
			default:              throw new RuntimeException('Could not find port type')
		}
		val String refType = switch port {
			PrimitivePort:                   port.type.literal
			JavaTypePort
			case port.type.name == 'Object': { return }
			JavaTypePort:                    port.type.name
			default:                         throw new RuntimeException('Could not find port type')
		}
		if (thisType != refType) {
			addError(thisId, '''Type mismatch, should be "«refType»"''')
		}	
		if (thisIO.isListType != port.isList) {
			addError(thisId, '''isList property mismatch, should be «port.isList»''')
		}
	}
	
}

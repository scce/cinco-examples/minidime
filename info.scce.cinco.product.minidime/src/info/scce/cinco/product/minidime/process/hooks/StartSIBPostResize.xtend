package info.scce.cinco.product.minidime.process.hooks

import graphmodel.Direction
import info.scce.cinco.product.minidime.process.StartSIB

import static info.scce.cinco.product.minidime.process.helper.LayoutConstants.*

/**
 * Copied from info.scce.dime.process.hooks.StartSIBPostResize - Complete
 */
class StartSIBPostResize extends ProcessNodePostResize<StartSIB> {
	
	override void postResize(StartSIB cStartSIB, int oldWidth, int oldHeight, int oldX, int oldY, Direction direction) {
		super.postResize(cStartSIB, oldWidth, oldHeight, oldX, oldY, direction)
		for (o : cStartSIB.outputs) {
			o.resize(cStartSIB.width - oldWidth - PORT_X * 2, o.height)
		}
	}
	
}

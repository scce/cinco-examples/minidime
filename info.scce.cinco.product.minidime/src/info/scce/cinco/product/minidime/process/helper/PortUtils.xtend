package info.scce.cinco.product.minidime.process.helper

import graphmodel.ModelElement
import info.scce.cinco.product.minidime.process.BooleanInputStatic
import info.scce.cinco.product.minidime.process.DataFlow
import info.scce.cinco.product.minidime.process.DataFlowSource
import info.scce.cinco.product.minidime.process.DataFlowTarget
import info.scce.cinco.product.minidime.process.IO
import info.scce.cinco.product.minidime.process.Input
import info.scce.cinco.product.minidime.process.InputStatic
import info.scce.cinco.product.minidime.process.IntegerInputStatic
import info.scce.cinco.product.minidime.process.JavaNativeInputPort
import info.scce.cinco.product.minidime.process.JavaNativeOutputPort
import info.scce.cinco.product.minidime.process.Output
import info.scce.cinco.product.minidime.process.PrimitiveInputPort
import info.scce.cinco.product.minidime.process.PrimitiveOutputPort
import info.scce.cinco.product.minidime.process.PrimitiveType
import info.scce.cinco.product.minidime.process.PrimitiveVariable
import info.scce.cinco.product.minidime.process.ProcessFactory
import info.scce.cinco.product.minidime.process.RealInputStatic
import info.scce.cinco.product.minidime.process.TextInputStatic
import info.scce.cinco.product.minidime.process.Variable
import info.scce.cinco.product.minidime.siblibrary.JavaType
import info.scce.cinco.product.minidime.siblibrary.JavaTypePort
import info.scce.cinco.product.minidime.siblibrary.Port
import info.scce.cinco.product.minidime.siblibrary.PrimitivePort
import java.util.ArrayList
import java.util.HashMap
import org.eclipse.emf.common.util.Enumerator

/**
 * Copied from info.scce.dime.process.helper.PortUtils - Complete
 */
class PortUtils {
	
	static final HashMap<?, PrimitiveType> PRIMITIVE_TYPE = newHashMap(
		BooleanInputStatic                                                 -> PrimitiveType::BOOLEAN,
		TextInputStatic                                                    -> PrimitiveType::TEXT,
		IntegerInputStatic                                                 -> PrimitiveType::INTEGER,
		RealInputStatic                                                    -> PrimitiveType::REAL,
		info.scce.cinco.product.minidime.siblibrary.PrimitiveType::BOOLEAN -> PrimitiveType::BOOLEAN,
		info.scce.cinco.product.minidime.siblibrary.PrimitiveType::TEXT    -> PrimitiveType::TEXT,
		info.scce.cinco.product.minidime.siblibrary.PrimitiveType::INTEGER -> PrimitiveType::INTEGER,
		info.scce.cinco.product.minidime.siblibrary.PrimitiveType::REAL    -> PrimitiveType::REAL,
		PrimitiveType::BOOLEAN                                             -> PrimitiveType::BOOLEAN,
		PrimitiveType::TEXT                                                -> PrimitiveType::TEXT,
		PrimitiveType::INTEGER                                             -> PrimitiveType::INTEGER,
		PrimitiveType::REAL                                                -> PrimitiveType::REAL
	)
	
	static final HashMap<PrimitiveType, Class<? extends InputStatic>> STATIC_INPUT_TYPE = newHashMap(
		PrimitiveType::BOOLEAN -> BooleanInputStatic,
		PrimitiveType::TEXT    -> TextInputStatic,
		PrimitiveType::INTEGER -> IntegerInputStatic,
		PrimitiveType::REAL    -> RealInputStatic
	)
	
	static def toPrimitiveType(IO type) {
		val key = PRIMITIVE_TYPE.keySet.filter(Class).findFirst[isInstance(type)]
		PRIMITIVE_TYPE.get(key ?: type.class)
	}
	
	static def toPrimitiveType(Enumerator type) {
		PRIMITIVE_TYPE.get(type)
	}
	
	static def toStaticInputType(PrimitiveType type) {
		STATIC_INPUT_TYPE.get(type)
	}
	
	static def IO addInput(DataFlowTarget dft, Object portReference) {
		switch portReference {
			
			PrimitiveOutputPort:
				dft.newPrimitiveInputPort(0, 0) => [
					dataType = portReference.dataType
					isList = portReference.isList
					name = portReference.name
				]
						
			JavaNativeOutputPort:
				dft.newJavaNativeInputPort(portReference.dataType, 0, 0) => [
					isList = portReference.isList
					name = portReference.name
				]
			
			PrimitiveInputPort:
				dft.newPrimitiveInputPort(0, 0) => [
					dataType = portReference.dataType
					isList = portReference.isList
					name = portReference.name
				]
			
			InputStatic:
				dft.newPrimitiveInputPort(0, 0) => [
					dataType = portReference.toPrimitiveType
					isList = false
					name = portReference.name
				]
			
			JavaNativeInputPort:
				dft.newJavaNativeInputPort(portReference.dataType, 0, 0) => [
					isList = portReference.isList
					name = portReference.name
				]
						
			PrimitiveVariable:
				dft.newPrimitiveInputPort(0, 0) => [
					dataType = portReference.dataType.toPrimitiveType
					isList = portReference.isList
					name = portReference.name
				]
			
			PrimitiveType:
				dft.newPrimitiveInputPort(0, 0) => [
					dataType = portReference
					isList = false
					name = portReference.getName?.replace(" ", "").trim.toFirstLower ?: ""
				]
			
			PrimitivePort:
				dft.newPrimitiveInputPort(0, 0) => [
					dataType = portReference.type.toPrimitiveType
					isList = portReference.isList
					name = portReference.name
				]
			
			JavaTypePort:
				dft.newJavaNativeInputPort(portReference.type, 0, 0) => [
					isList = portReference.isList
					name = portReference.name
				]
			
			default:
				throw new SwitchException
		}
	}
	
	static def clonePort(DataFlowTarget dft, Input port) {
		switch port {
			
			PrimitiveInputPort:
				dft.newPrimitiveInputPort(port.x, port.y) => [
					dataType = port.dataType
					isList = port.isIsList
					name = port.name
				]
			
			JavaNativeInputPort:
				dft.newJavaNativeInputPort((port as JavaNativeInputPort).dataType, port.x, port.y) => [
					isList = port.isList
					name = port.name
				]
				
			BooleanInputStatic:
				dft.newBooleanInputStatic(port.x, port.y) => [
					name = port.name
					value = port.value
				]
			
			IntegerInputStatic:
				dft.newIntegerInputStatic(port.x, port.y) => [
					name = port.name
					value = port.value
				]
			
			RealInputStatic:
				dft.newRealInputStatic(port.x, port.y) => [
					name = port.name
					value = port.value
				]
			
			TextInputStatic:
				dft.newTextInputStatic(port.x, port.y) => [
					name = port.name
					value = port.value
				]
						
			default:
				throw new SwitchException
		}
	}
	
	static def dispatch addStaticPort(DataFlowTarget dft, PrimitiveInputPort archetype) {
		switch archetype.dataType {
			case BOOLEAN: dft.newBooleanInputStatic(archetype.x, archetype.y)
			case INTEGER: dft.newIntegerInputStatic(archetype.x, archetype.y)
			case REAL:    dft.newRealInputStatic(archetype.x, archetype.y)
			case TEXT:    dft.newTextInputStatic(archetype.x, archetype.y)
			default:      throw new SwitchException
		} => [
			name = archetype.name
		]
	}
	
	static def dispatch addStaticPort(DataFlowTarget dft, InputStatic archetype) {
		switch archetype {
			BooleanInputStatic:
				dft.newBooleanInputStatic(archetype.x, archetype.y) => [
					value = archetype.value
				]
			
			IntegerInputStatic:
				dft.newIntegerInputStatic(archetype.x, archetype.y) => [
					value = archetype.value
				]
			
			RealInputStatic:
				dft.newRealInputStatic(archetype.x, archetype.y) => [
					value = archetype.value
				]
			
			TextInputStatic:
				dft.newTextInputStatic(archetype.x, archetype.y) => [
					value = archetype.value
				]
				
			default:
				throw new SwitchException
		} => [
			name = archetype.name
		]
	}
	
	static def cloneStaticPort(InputStatic port) {
		cloneStaticPort(port.container as DataFlowTarget, port)
	}
	
	static def cloneStaticPort(DataFlowTarget dft, InputStatic port) {
		switch port {
			BooleanInputStatic:
				dft.newBooleanInputStatic(port.x, port.y) => [
					name = port.name
					value = port.value
				]
			
			IntegerInputStatic:
				dft.newIntegerInputStatic(port.x, port.y) => [
					name = port.name
					value = port.value
				]
			
			RealInputStatic:
				dft.newRealInputStatic(port.x, port.y) => [
					name = port.name
					value = port.value
				]
			
			TextInputStatic:
				dft.newTextInputStatic(port.x, port.y) => [
					name = port.name
					value = port.value
				]
			
			default:
				throw new SwitchException
 		}
	}
	
	static def addOutput(DataFlowSource branch, Object ref) {
		switch ref {
			PrimitiveInputPort:
				branch.newPrimitiveOutputPort(0, 0) => [
					dataType = ref.dataType
					isList = ref.isList
					name = ref.name
				]
			
			JavaNativeInputPort:
				branch.newJavaNativeOutputPort(ref.dataType, 0, 0) => [
					isList = ref.isList
					name = ref.name
				]
			
			PrimitiveOutputPort:
				branch.newPrimitiveOutputPort(0, 0) => [
					isList = ref.isList
					dataType = ref.dataType
					name = ref.name
				]
			
			JavaNativeOutputPort:
				branch.newJavaNativeOutputPort(ref.dataType, 0, 0) => [
					isList = ref.isList
					name = ref.name
				]
			
			InputStatic:
				branch.newPrimitiveOutputPort(0, 0) => [
					dataType = ref.toPrimitiveType
					isList = false
					name = ref.name
				]
			
			PrimitiveType:
				branch.newPrimitiveOutputPort(0, 0) => [
					dataType = ref
					isList = false
					name = ref.getName?.replace(" ", "").trim.toFirstLower ?: ""
				]
			
			PrimitiveVariable:
				branch.newPrimitiveOutputPort(0, 0) => [
					dataType = ref.dataType.toPrimitiveType
					isList = ref.isList
					name = ref.name
				]

			PrimitivePort:
				branch.newPrimitiveOutputPort(0, 0) => [
					dataType = ref.type.toPrimitiveType
					isList = ref.isList
					name = ref.name
				]
			
			JavaTypePort:
				branch.newJavaNativeOutputPort(ref.type, 0, 0) => [
					isList = ref.isList
					name = ref.name
				]
						
			default:
				throw new SwitchException(ref)
		}
	}
	
	static def reconnect(DataFlow edge, Output source, Input target) {
		edge.reconnectSource(source)
		edge.reconnectTarget(target)
	}
	
	static def reconnectIncoming(Input oldTarget, Input newTarget) {
		for (edge : new ArrayList(oldTarget.incoming)) {
			(edge as DataFlow).reconnectTarget(newTarget)
		}
	}
	
	static def reconnectOutgoing(Output oldSource, Output newSource) {
		for (edge : new ArrayList(oldSource.outgoing)) {
			(edge as DataFlow).reconnectSource(newSource)
		}
	}
	
	static def toProcessInputPort(Port port) {
		switch port {
			PrimitivePort:
				ProcessFactory.eINSTANCE.createPrimitiveInputPort => [
					dataType = port.type.toPrimitiveType
					isList = port.isIsList
					name = port.name
				]
			
			JavaTypePort:
				ProcessFactory.eINSTANCE.createJavaNativeInputPort => [
					isList = port.isIsList
					name = port.name
					libraryComponentUID = (port.type as JavaType)?.fqn
				]
		}
	}
	
	static def toProcessOutputPort(Port port) {
		switch port {
			PrimitivePort:
				ProcessFactory.eINSTANCE.createPrimitiveOutputPort => [
					dataType = port.type.toPrimitiveType
					isList = port.isIsList
					name = port.name
				]
			
			JavaTypePort:
				ProcessFactory.eINSTANCE.createJavaNativeOutputPort => [
					isList = port.isIsList
					name = port.name
					libraryComponentUID = (port.type as JavaType)?.fqn
				]
		}
	}
	
	static def hasPrimitiveType(ModelElement it) {
		getPrimitiveType !== null
	}
	
	static def getPrimitiveType(IO it) {
		 switch it {
			PrimitiveInputPort:  dataType
			PrimitiveOutputPort: dataType
			InputStatic:         toPrimitiveType
		}
	}
	
	static def getPrimitiveType(Variable it) {
		 switch it {
			PrimitiveVariable: dataType.toPrimitiveType
		}
	}
	
	static def getPrimitiveType(ModelElement it) {
		 switch it {
			IO:       primitiveType
			Variable: primitiveType
		}
	}
	
	static def getPrimitiveType(Port it) {
		switch it {
			PrimitivePort: type.toPrimitiveType
		}
	}
	
	/**
	 * A simple synchronization of a modifiable list of items with a static
	 * list of references.
	 * <ul>
	 * <li>Items are compared via the {@code match} function.
	 * <li>Missing items are added via the {@code match} function.
	 * <li>Consistent items are updated via the {@code match} function.
	 * <li>Unnecessary items are deleted via the {@code match} function
	 * </ul>
	 */
	static class Sync<Item,Reference> {
		public (Item, Reference) => boolean equals = [ false ]
		public (Reference)       => void    add    = [ ]
		public (Item, Reference) => void    update = [ ]
		public (Item)            => void    delete = [ ]
		
		def apply(Iterable<Item> items, Iterable<Reference> refs) {
			val _items = items.toNewList
			val _refs = refs.toNewList
			
			// Update consistent items, add non-existent ones
			for (ref : _refs) {
				val item = _items.findFirst[equals.apply(it, ref)]
				if (item !== null) {
					_items.remove(item)
					update.apply(item, ref)
				}
				else {
					add.apply(ref)
				}
			}
			
			// Remove unnecessary items
			_items.forEach(delete)
		}
		
		def <X> toNewList(Iterable<X> list) {
			val newList = newArrayList
			if (list !== null) {
				newList.addAll(list)
			}
			return newList
		}
	}
	
	static class SwitchException extends IllegalStateException {
		new() {
			super('Default case in exhaustive switch; implementation broken?')
		}
		new(Object obj) {
			super('''Default case in exhaustive switch for «obj?.class?.simpleName»; implementation broken?''')
		}
	}
	
}
package info.scce.cinco.product.minidime.process.checks

import info.scce.cinco.product.minidime.checks.AbstractCheck
import info.scce.cinco.product.minidime.mcam.adapter.MiniDIMEProcessAdapter
import info.scce.cinco.product.minidime.mcam.adapter.MiniDIMEProcessId
import info.scce.cinco.product.minidime.process.DataContext
import info.scce.cinco.product.minidime.process.DataFlowSource
import info.scce.cinco.product.minidime.process.DataFlowTarget
import info.scce.cinco.product.minidime.process.EndSIB
import info.scce.cinco.product.minidime.process.StartSIB
import java.util.ArrayList
import java.util.HashSet

/**
 * Copied from info.scce.dime.process.checks.UniqueNamesCheck - Complete
 */
class UniqueNamesCheck extends AbstractCheck<MiniDIMEProcessId, MiniDIMEProcessAdapter> {
	
	ArrayList<String> contextVarNames = new ArrayList

	override void doExecute(MiniDIMEProcessAdapter adapter) {
		val branchNames = new HashSet
		for (id : adapter.entityIds) {
			switch obj : id.element {
				StartSIB: {
					adapter.checkDFS(obj)
				}
				EndSIB: {
					adapter.checkDFT(obj)
					if (!branchNames.add(obj.branchName)) {
						id.addError('''«obj.branchName» not unique''')
					}
				}
				DataContext: {
					adapter.checkDataContext(obj)
				}
			}
		}
	}
	
	override void init() {
		contextVarNames.clear
	}
	
	def private void checkDataContext(MiniDIMEProcessAdapter adapter, DataContext context) {
		for (variable : context.variables) {
			if (contextVarNames.contains(variable.name)) {
				adapter.getIdByString(variable.id).addError('''«variable.name» not unique''')
			}
			contextVarNames.add(variable.name)
		}
	}

	def private void checkDFT(MiniDIMEProcessAdapter adapter, DataFlowTarget sib) {
		val names = new ArrayList
		for (inPort : sib.inputs) {
			if (names.contains(inPort.name)) {
				adapter.getIdByString(inPort.id).addError('''«inPort.name» not unique''')
			}
			names.add(inPort.name)
		}
	}

	def private void checkDFS(MiniDIMEProcessAdapter adapter, DataFlowSource sib) {
		val names = new ArrayList
		for (outPort : sib.outputs) {
			if (names.contains(outPort.name)) {
				adapter.getIdByString(outPort.id).addError('''«outPort.name» not unique''')
			}
			names.add(outPort.name)
		}
	}
	
}

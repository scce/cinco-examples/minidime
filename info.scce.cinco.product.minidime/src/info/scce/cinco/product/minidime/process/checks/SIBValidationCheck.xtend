package info.scce.cinco.product.minidime.process.checks

import info.scce.cinco.product.minidime.api.MiniDIMECheck
import info.scce.cinco.product.minidime.mcam.adapter.MiniDIMEProcessAdapter
import info.scce.cinco.product.minidime.mcam.adapter.MiniDIMEProcessId
import info.scce.cinco.product.minidime.process.Branch
import info.scce.cinco.product.minidime.process.DataFlowTarget
import info.scce.cinco.product.minidime.process.EndSIB
import info.scce.cinco.product.minidime.process.IO
import info.scce.cinco.product.minidime.process.Input
import info.scce.cinco.product.minidime.process.InputStatic
import info.scce.cinco.product.minidime.process.JavaNativeInputPort
import info.scce.cinco.product.minidime.process.JavaNativeOutputPort
import info.scce.cinco.product.minidime.process.Output
import info.scce.cinco.product.minidime.process.PrimitiveInputPort
import info.scce.cinco.product.minidime.process.PrimitiveOutputPort
import info.scce.cinco.product.minidime.process.PutToContextSIB
import info.scce.cinco.product.minidime.process.SIB
import info.scce.cinco.product.minidime.process.helper.ProcessExtension
import java.util.ArrayList
import java.util.HashMap
import java.util.List

/**
 * Copied from info.scce.dime.process.checks.SibValidationCheck - Complete
 */
class SIBValidationCheck extends MiniDIMECheck<MiniDIMEProcessId, MiniDIMEProcessAdapter> {
	
	extension ProcessExtension = new ProcessExtension
	
	var MiniDIMEProcessAdapter adapter
	
	override void doExecute(MiniDIMEProcessAdapter adapter) {
		this.adapter = adapter
		for (id : adapter.entityIds) {
			val obj = id.element
			if (obj instanceof DataFlowTarget) {
				checkLabel(id, obj)
			}
			switch obj {
				PutToContextSIB:
					checkPutToContextSIB(id, obj)
			}
		}
		processResults
	}
	
	def private void checkLabel(MiniDIMEProcessId id, DataFlowTarget obj) {
		switch obj {
			SIB    case obj.label      === null: addError(id, 'label has to be provided')
			EndSIB case obj.branchName === null: addError(id, 'label has to be provided')
		}
	}
	
	def private void checkPutToContextSIB(MiniDIMEProcessId sibId, PutToContextSIB sib) {
		val inputList = new ArrayList<String>
		val branchMap = new HashMap<String, List<String>>
		branchMap.put('success', #[]);
		checkSIB(sibId, sib, inputList, branchMap, false, true, false)

		// Collect inputs
		val inputs = new HashMap<String, Input>
		sib.inputs.forEach[inputs.put(name, it)]

		// Get success branch
		val successBranch = sib.getSuccessors(Branch).findFirst[name == 'success']
		if (successBranch === null) {
			return
		}

		// Collect outputs
		val outputs = new HashMap<String, Output>
		successBranch.outputs.forEach[outputs.put(name, it)]

		// Check missing outputs
		val missingOutputs = new ArrayList(inputs.keySet)
		missingOutputs.removeAll(outputs.keySet)
		missingOutputs.forEach[
			addError(adapter.getIdByString(successBranch.id), '''missing output "«it»"''')
		]

		// Check missing inputs
		val missingInputs = new ArrayList(outputs.keySet)
		missingInputs.removeAll(inputs.keySet)
		missingInputs.forEach[
			addError(adapter.getIdByString(successBranch.id), '''missing input "«it»"''')
		]

		// Check existing ports
		val checkIOs = new ArrayList(inputs.keySet)
		checkIOs.retainAll(outputs.keySet)
		checkIOs.forEach[
			val input  = inputs.get(it)
			val output = outputs.get(it)
			checkTypeOfIO(adapter.getIdByString(output.id), input, output)
		]
	}
	
	def private void checkSIB(MiniDIMEProcessId sibId,
		                      DataFlowTarget dft,
		                      List<String> inputList,
		                      HashMap<String, List<String>> branchMap,
		                      boolean exlusiveInputs,
		                      boolean exclusiveBranches,
		                      boolean exclusiveOutputs) {
		// Checking inputs
		val inputs = new ArrayList(inputList)
		for (input : dft.inputs) {
			if (!inputList.contains(input.name) && exlusiveInputs) {
				addError(sibId, '''input "«input.name»" not allowed''')
			}
			inputs.remove(input.name)
		}
		inputs.forEach[addError(sibId, '''missing input "«it»"''')]
		
		// Check duplicated input names
		val portNames = new ArrayList<String>
		for (input : dft.inputs) {
			if (portNames.contains(input.name)) {
				addError(sibId, '''duplicated input name "«input.name»"''')
			}
			else {
				portNames.add(input.name)
			}
		}

		// Checking branch
		val branches = new ArrayList(branchMap.keySet)
		for (branch : dft.getSuccessors(Branch)) {
			if (!branchMap.keySet.contains(branch.name)) {
				if (exclusiveBranches) {
					addError(sibId, '''unallowed branch "«branch.name»"''')
				}
			}
			else {
				// Checking outputs
				val outputs = new ArrayList(branchMap.get(branch.name))
				for (output : branch.outputs) {
					if (!branchMap.get(branch.name).contains(output.name) && exclusiveOutputs) {
						addError(sibId, '''unallowed output "«output.name»"''')
					}
					outputs.remove(output.name)
				}
				outputs.forEach[addError(sibId, '''missing output "«it»"''')]
			}
			branches.remove(branch.name)
		}
		branches.forEach[addWarning(sibId, '''missing branch "«it»"''')]
	}
	
	def private void checkTypeOfIO(MiniDIMEProcessId thisId, IO thisIO, IO referencedIO) {
		val String thisType = switch thisIO {
			PrimitiveOutputPort,
			PrimitiveInputPort:   thisIO.primitiveTypeString
			JavaNativeOutputPort,
			JavaNativeInputPort:  thisIO.nativeTypeString
			InputStatic:          thisIO.staticTypeString
			default:              throw new RuntimeException('Could not find port type')
		}
		val String refType = switch referencedIO {
			PrimitiveOutputPort,
			PrimitiveInputPort:   referencedIO.primitiveTypeString
			JavaNativeOutputPort,
			JavaNativeInputPort:  referencedIO.nativeTypeString
			InputStatic:          referencedIO.staticTypeString
			default:              throw new RuntimeException('Could not find port type')
		}
		if (thisType != refType) {
			addError(thisId, '''Type mismatch, should be "«refType»"''')
		}
		if (thisIO.isListType != referencedIO.isListType) {
			addError(thisId, '''isList property mismatch, should be "«referencedIO.isListType»"''')
		}
	}
	
}

package info.scce.cinco.product.minidime.process.hooks

import info.scce.cinco.product.minidime.process.AtomicSIB
import info.scce.cinco.product.minidime.process.build.AtomicSIBBuild
import info.scce.cinco.product.minidime.siblibrary.SIB

/**
 * Copied from info.scce.dime.process.hooks.AtomicSIBHook - Complete
 */
class AtomicSIBPostCreate extends AbstractSIBPostCreate<AtomicSIB> {
	
	override initialize(AtomicSIB sib) {
		val lcAtomicSIB = sib.sib as SIB
		label = lcAtomicSIB.name
		return AtomicSIBBuild.initialize(sib)
	}
	
}
package info.scce.cinco.product.minidime.process.hooks

import info.scce.cinco.product.minidime.api.MiniDIMEPostCreateHook
import info.scce.cinco.product.minidime.process.JavaNativeOutputPort
import info.scce.cinco.product.minidime.process.Output
import info.scce.cinco.product.minidime.process.PrimitiveOutputPort
import info.scce.cinco.product.minidime.process.StartSIB
import info.scce.cinco.product.minidime.siblibrary.Type

import static extension info.scce.cinco.product.minidime.process.helper.SIBLayoutUtils.*

/**
 * Copied from info.scce.dime.process.hooks.OutputPortHook - Complete
 */
class OutputPostCreate extends MiniDIMEPostCreateHook<Output> {
	
	override void postCreate(Output port) {
		try {
	 		if (port.container instanceof StartSIB) {
	 			val cStartSIB = port.container as StartSIB
	 			val outputPortAmount = cStartSIB.outputs.size
	 			switch port {
	 				PrimitiveOutputPort:
	 					port.name = port.dataType.getName.toLowerCase + outputPortAmount
	 				JavaNativeOutputPort:
	 					port.name = (port.dataType as Type).name.toLowerCase + outputPortAmount
					default:
						throw new IllegalStateException('Reached else/default case in exhaustive if/switch. Please fix the code.')
	 			}
	 		}
	 		port.container.resizeAndLayout
		}
		catch (Exception e) {
			e.printStackTrace
		}
	}
	
}
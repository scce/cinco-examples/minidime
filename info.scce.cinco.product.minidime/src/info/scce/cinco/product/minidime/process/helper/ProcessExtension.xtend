package info.scce.cinco.product.minidime.process.helper

//import de.jabc.cinco.meta.runtime.active.Memoizable
import graphmodel.Container
import graphmodel.Node
import info.scce.cinco.product.minidime.process.AbstractBranch
import info.scce.cinco.product.minidime.process.AbstractIterateSIB
import info.scce.cinco.product.minidime.process.BooleanInputStatic
import info.scce.cinco.product.minidime.process.BranchConnector
import info.scce.cinco.product.minidime.process.DataFlow
import info.scce.cinco.product.minidime.process.DataFlowTarget
import info.scce.cinco.product.minidime.process.DirectDataFlow
import info.scce.cinco.product.minidime.process.EndSIB
import info.scce.cinco.product.minidime.process.Input
import info.scce.cinco.product.minidime.process.InputPort
import info.scce.cinco.product.minidime.process.InputStatic
import info.scce.cinco.product.minidime.process.IntegerInputStatic
import info.scce.cinco.product.minidime.process.JavaNativeInputPort
import info.scce.cinco.product.minidime.process.JavaNativeOutputPort
import info.scce.cinco.product.minidime.process.JavaNativeVariable
import info.scce.cinco.product.minidime.process.OutputPort
import info.scce.cinco.product.minidime.process.PrimitiveDirectDataFlow
import info.scce.cinco.product.minidime.process.PrimitiveInputPort
import info.scce.cinco.product.minidime.process.PrimitiveOutputPort
import info.scce.cinco.product.minidime.process.PrimitiveType
import info.scce.cinco.product.minidime.process.PrimitiveVariable
import info.scce.cinco.product.minidime.process.ProcessSIB
import info.scce.cinco.product.minidime.process.ProcessType
import info.scce.cinco.product.minidime.process.Read
import info.scce.cinco.product.minidime.process.RealInputStatic
import info.scce.cinco.product.minidime.process.SIB
import info.scce.cinco.product.minidime.process.StartSIB
import info.scce.cinco.product.minidime.process.TextInputStatic
import info.scce.cinco.product.minidime.process.Update
import info.scce.cinco.product.minidime.process.Variable
import info.scce.cinco.product.minidime.siblibrary.Type
import java.util.HashMap
import java.util.List
import java.util.Set
import java.util.regex.Pattern

import static info.scce.cinco.product.minidime.process.PrimitiveType.*
import info.scce.cinco.product.minidime.process.MiniDIMEProcess

/**
 * Copied from info.scce.dime.process.helper.ProcessExtension - Complete
 * <br>
 * and some methods from info.scce.dime.process.checks.ProcessUtils - Complete
 */
class ProcessExtension {
	
	static public final HashMap<info.scce.cinco.product.minidime.siblibrary.PrimitiveType, PrimitiveType> PROCESS_PRIMITIVE_TYPE = newHashMap(
		info.scce.cinco.product.minidime.siblibrary.PrimitiveType::BOOLEAN -> BOOLEAN,
		info.scce.cinco.product.minidime.siblibrary.PrimitiveType::INTEGER -> INTEGER,
		info.scce.cinco.product.minidime.siblibrary.PrimitiveType::REAL    -> REAL,
		info.scce.cinco.product.minidime.siblibrary.PrimitiveType::TEXT    -> TEXT
	)
	
//	@Memoizable
	def List<Input> getProcessInputs(ProcessSIB it) {
		inputs.sortBy[name]
	}
	
//	@Memoizable
	def Iterable<Container> getProcessAllInputs(MiniDIMEProcess it) {
		processInputs.filter(Container).sortBy [
			switch it {
				OutputPort: name
			}
		]
	}
	
//	@Memoizable
	def List<OutputPort> getProcessInputs(MiniDIMEProcess it) {
		startSIB.outputPorts.sortBy[name]
	}
	
//	@Memoizable
	def Iterable<OutputPort> getProcessInputsInUse(MiniDIMEProcess process) {
		process.processInputs.filter[!dataEdges.empty]
	}
	
//	@Memoizable
	def StartSIB getStartSIB(MiniDIMEProcess process) {
		process.startSIBs.head
	}
	
//	@Memoizable
	def Iterable<DataFlow> getDataEdges(OutputPort outputPort) {
		outputPort.getOutgoing.filter(DataFlow).filter[it instanceof Update || it instanceof DirectDataFlow]
	}
	
//	@Memoizable
	def Iterable<DataFlow> getOutgoingDataFlows(Iterable<OutputPort> outputPorts) {
		outputPorts.map[outgoingDataFlows].flatten
	}
	
//	@Memoizable
	def Iterable<DataFlow> getOutgoingDataFlows(OutputPort outputPort) {
		// Take only the update edges and one of the direct data edges
		// (as it is the representative for all direct data flows). 
		var outgoings = outputPort.dataEdges.filter[it instanceof Update]
		val ddf = outputPort.dataEdges.findFirst[it instanceof DirectDataFlow]
		if (ddf !== null) {
			outgoings = outgoings + #[ddf]
		}
		return outgoings
	}
	
//	@Memoizable
	def Iterable<DataFlowTarget> getDataFlowTargets(MiniDIMEProcess process) {
		process.allNodes.filter(DataFlowTarget)
	}
	
//	@Memoizable
	def List<MiniDIMEProcess> getSubProcesses(MiniDIMEProcess process) {
		process.processSIBs.map[proMod]
	}
	
//	@Memoizable
	def Iterable<Variable> getVariables(MiniDIMEProcess process) {
		process.dataContexts.map[variables].flatten
	}
	
//	@Memoizable
	def List<DataFlow> sortTopologically(Iterable<DataFlow> outgoingDataFlows) {	
		outgoingDataFlows.sortWith[a, b | a.id.compareTo(b.id)]
	}
	
//	@Memoizable
	def Set<OutputPort> getUniqueSources(Iterable<DirectDataFlow> ddfs) {
		ddfs.map[source].toSet
	}
	
	def getSource(DirectDataFlow ddf) {
		ddf.sourceElement as OutputPort
	}
	
//	@Memoizable
	def AbstractBranch getBranchByName(SIB sib, String name) {
		sib.branches.filter[b | b.name == name].head
	}
	
//	@Memoizable
	def Iterable<AbstractBranch> getBranches(DataFlowTarget sib) {
		sib.getOutgoing(BranchConnector).map[targetElement].filter(AbstractBranch)
	}
	
//	@Memoizable
	def DataFlow getDataEdge(InputPort inputPort) {
		inputPort.incoming.filter(DataFlow).filter[it instanceof Read || it instanceof DirectDataFlow].head
	}
	
//	@Memoizable
	def Iterable<OutputPort> getOutputsInUse(AbstractBranch branch) {
		branch.outputPorts.filter[!dataEdges.empty]
	}
	
//	@Memoizable
	def Iterable<InputStatic> getStaticModelBranchOutputs(EndSIB endSIB) {
		endSIB.inputs.filter(InputStatic)
	}
	
//	@Memoizable
	def Iterable<InputPort> getInputsInUse(SIB it) {
		primitiveInputsInUse + javaNativeInputsInUse 
	}
	
//	@Memoizable
	def Iterable<PrimitiveInputPort> getPrimitiveInputsInUse(SIB sib) {
		sib.primitiveInputPorts.filter[dataEdge !== null]
	}
	
//	@Memoizable
	def Iterable<JavaNativeInputPort> getJavaNativeInputsInUse(SIB sib) {
		sib.javaNativeInputPorts.filter[dataEdge !== null]
	}
	
//	@Memoizable
	def Iterable<AbstractIterateSIB> getIterateSIBsInUse(MiniDIMEProcess process) {
		process.abstractIterateSIBs.filter[inputsInUse.size > 0]
	}
	
	def getDataType(InputStatic input) {
		switch input {
			BooleanInputStatic: BOOLEAN
			IntegerInputStatic: INTEGER
			RealInputStatic:    REAL
			TextInputStatic:    TEXT
		}
	}
	
//	@Memoizable
	def Iterable<DirectDataFlow> getDirectDataFlows(MiniDIMEProcess process) {
		process.getModelElements(PrimitiveDirectDataFlow).map[it as DirectDataFlow]
	}
	
//	@Memoizable 
	def Iterable<InputPort> getModelBranchOutputsInUse(EndSIB endSIB) {
		endSIB.inputPorts.filter[dataEdge !== null]
	}
	
	def isOfType(MiniDIMEProcess process, ProcessType type) {
		process?.getProcessType == type
	}
	
	def getPrimitiveType(Node it) {
		switch it {
			PrimitiveVariable:   dataType
			PrimitiveInputPort:  dataType
			PrimitiveOutputPort: dataType
			InputStatic:         primitiveType
			default:             throw new SwitchException
		}
		
	}
	
	def getPrimitiveType(InputStatic input) {
		switch input {
			BooleanInputStatic: BOOLEAN
			TextInputStatic:    TEXT
			IntegerInputStatic: INTEGER
			RealInputStatic:    REAL
			default:            throw new SwitchException
		}
	}

	/**
	 * Copied from info.scce.dime.process.checks.ProcessUtils#getPrimitiveTypeString(Node)
	 */
	def String getPrimitiveTypeString(Node it) {
		primitiveType.literal
	}
	
	/**
	 * Copied from info.scce.dime.process.checks.ProcessUtils#getStaticTypeString(Node)
	 */
	def String getStaticTypeString(Node it) {
		primitiveTypeString
	}
	
	def getNativeType(Node it) {
		switch it {
			JavaNativeVariable:   dataType
			JavaNativeInputPort:  dataType
			JavaNativeOutputPort: dataType
			default:              throw new SwitchException
		} as Type
	}
	
	/**
	 * Copied from info.scce.dime.process.checks.ProcessUtils#getNativeType(Node)
	 */
	def String getNativeTypeString(Node it) {
		nativeType.name
	}
	
	def isListType(Node it) {
		switch it {
			InputPort:   isList
			OutputPort:  isList
			Variable:    isList
			InputStatic: false
			default:     throw new SwitchException
		}
	}
		
	static class SwitchException extends IllegalStateException {
		
		new() {
			super('Default case in exhaustive switch; implementation broken?')
		}
		
		new(Object obj) {
			super('''Default case in exhaustive switch for «obj?.class?.simpleName»; implementation broken?''')
		}
		
	}
	
	/**
	 * Copied from info.scce.dime.process.checks.ProcessUtils#getExpressionPattern()
	 */
	def Pattern getExpressionPattern() {
		Pattern.compile('(([^\\.\\[\\]])+)(\\[[0-9]+\\])?\\.?')
	}
	
	/**
	 * Copied from info.scce.dime.process.checks.ProcessUtils#getExpressionStringPattern()
	 */
	def Pattern getExpressionStringPattern() {
		Pattern.compile('(.*?)\\{\\{(.*?)\\}\\}')
	}
	
}
package info.scce.cinco.product.minidime.process.hooks

import graphmodel.Direction
import info.scce.cinco.product.minidime.process.SIB

import static info.scce.cinco.product.minidime.process.helper.LayoutConstants.*

/**
 * Copied from info.scce.dime.process.hooks.SIBPostResize - Complete
 */
class SIBPostResize extends ProcessNodePostResize<SIB> {
	
	override void postResize(SIB cModelElement, int oldWidth, int oldHeight, int oldX, int oldY, Direction direction) {
		super.postResize(cModelElement, oldWidth, oldHeight, oldX, oldY, direction)
		for (i : cModelElement.inputs) {
			i.resize(cModelElement.width - oldWidth - PORT_X * 2, i.height)
		}
	}
	
}

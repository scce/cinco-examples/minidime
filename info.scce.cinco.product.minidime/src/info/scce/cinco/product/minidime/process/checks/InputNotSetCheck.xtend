package info.scce.cinco.product.minidime.process.checks

import info.scce.cinco.product.minidime.checks.AbstractCheck
import info.scce.cinco.product.minidime.mcam.adapter.MiniDIMEProcessAdapter
import info.scce.cinco.product.minidime.mcam.adapter.MiniDIMEProcessId
import info.scce.cinco.product.minidime.process.DataFlow
import info.scce.cinco.product.minidime.process.DataFlowTarget
import info.scce.cinco.product.minidime.process.InputStatic

/**
 * Copied from info.scce.dime.process.checks.InputNotSetCheck - Complete
 */
class InputNotSetCheck extends AbstractCheck<MiniDIMEProcessId, MiniDIMEProcessAdapter> {
	
	override void doExecute(MiniDIMEProcessAdapter adapter) {
		for (id : adapter.entityIds) {
			val obj = id.element
			if (obj instanceof DataFlowTarget) {
				if (obj.container instanceof Process) {
					for (input : obj.inputs) {
						if (!(input instanceof InputStatic) && input.getIncoming(DataFlow).empty) {
							addWarning(adapter.getIdByString(input.id), 'not set')
						}
					}
				}
			}
		}
	}
	
	override void init() {
		// Intentionally left blank
	}
	
}

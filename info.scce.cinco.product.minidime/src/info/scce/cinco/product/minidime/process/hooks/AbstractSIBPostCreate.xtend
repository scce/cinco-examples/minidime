package info.scce.cinco.product.minidime.process.hooks

import info.scce.cinco.product.minidime.process.Branch
import info.scce.cinco.product.minidime.process.ControlFlow
import info.scce.cinco.product.minidime.process.DataFlowSource
import info.scce.cinco.product.minidime.process.IO
import info.scce.cinco.product.minidime.process.Input
import info.scce.cinco.product.minidime.process.OutputPort
import info.scce.cinco.product.minidime.process.PrimitiveType
import info.scce.cinco.product.minidime.process.SIB
import info.scce.cinco.product.minidime.siblibrary.Type
import java.util.ArrayList
import java.util.List

import static info.scce.cinco.product.minidime.process.helper.LayoutConstants.*

import static extension info.scce.cinco.product.minidime.process.helper.PortUtils.*
import info.scce.cinco.product.minidime.process.MiniDIMEProcess

/**
 * Copied from info.scce.dime.process.hooks.AbstractPostCreateSIBHook - Complete
 */
abstract class AbstractSIBPostCreate<T extends SIB> extends ProcessNodePostCreate<T> {
	
	protected MiniDIMEProcess model
	protected T sib

	Integer branchX
	Integer branchY
	int sibInputPortY
	int branchOutputPortY
	Branch currentBranch
	
	override final void postCreate(T sib) {
		super.postCreate(sib)
		sib.init
		try {
			sib.preProcess
			if (sib.initialize) {
				sib.postProcess
				finish
			}
		} catch(StopHookExecution e) {
			println('''Exeecution of «class.simpleName» stopped.''')
		}
	}
	
	/**
	 * Any kind of pre-processing, e.g. to decide whether the creation of the SIB
	 * is allowed. If not, the execution of this hook should be stopped.
	 * <p>To stop the execution, call {@link #stop(String)}</p>
	 * <p>An error message can be shown via {@link #showError(String)}</p>
	 * <p>If necessary, the SIB can be deleted via {@link CSIB#delete() cSib.delete()}</p>
	 * <p> Phase: <strong>Pre-Processing</strong> -> Processing -> Post-Processing</p>
	 * @param sib - the SIB to be processed.
	 */
	def void preProcess(T sib) throws StopHookExecution {
		if (sib.container instanceof DataFlowSource) {
			val cBranch = sib.container as DataFlowSource
			if (!cBranch.getOutgoing(ControlFlow).empty) {
				sib.delete
				stop('Outgoing control flow already exists.')
			}
			val x = cBranch.x - (SIB_WIDTH - cBranch.width) / 2
			val y = cBranch.y + cBranch.height + 40
			sib.moveTo(sib.rootElement, x, y)
			cBranch.newControlFlow(sib)
		}
	}
	
	/**
	 * Do the actual work that is necessary to enrich the SIB.
	 * <p>
	 * This class provides some convenient mehtods to add Input Ports and
	 * Branches as well as Output Ports for the latter.
	 * </p>
	 * <p>To stop the execution, call {@link #stop(String)}</p>
	 * <p>An error message can be shown via {@link #showError(String)}</p>
	 * <p>If necessary, the SIB can be deleted via {@link CSIB#delete()}</p>
	 * <p>Phase: Pre-Processing -> <strong>Processing</strong> -> Post-Processing</p>
	 * @see #addInputPortForReference(Object)
	 * @see #addBranch(String)
	 * @see #addOutputPortForReference(Object)
	 * @param sib - the SIB to be processed.
	 */
	def /* abstract */ boolean initialize(T sib)
	
	/**
	 * Any kind of post-processing, e.g. to apply a custom layout.
	 * <p>To stop the execution, call {@link #stop(String)}</p>
	 * <p>An error message can be shown via {@link #showError(String)}</p>
	 * <p>If necessary, the SIB can be deleted via {@link CSIB#delete()}</p>
	 * <p> Phase: Pre-Processing -> Processing -> <strong>Post-Processing</strong></p>
	 * @param sib - the SIB to be processed.
	 */
	def void postProcess(T sib) {
		// Do nothing per default
	}
	
	/**
	 * Finishes the sib creation. currently only sets the SIB width to 150.
	 */
	def protected void finish() {
		sib.resize(SIB_WIDTH, sib.height)
		val y = sib.calcBranchY
		for (cBranch : sib.getSuccessors(Branch)) {
			cBranch.y = y
		}
	}
	
	/**
	 * Initializes the creation/autolayouting of the SIB.
	 * @param createdSib The SIB that is created
	 */
	def protected void init(T createdSib) {
		model = createdSib.rootElement as MiniDIMEProcess
		sib = createdSib
		sibInputPortY = SIB_FIRST_PORT_Y
	}
	
	def private int calcBranchX(SIB csib) {
		return csib.getX() + SIB_WIDTH / 2 - BRANCH_WIDTH / 2
	}
	
	def private int calcBranchY(SIB csib) {
		return csib.getY() + csib.getHeight() + BRANCH_V_DISTANCE
	}
	
	def protected void setLabel(String label) {
		sib.label = label
	}
	
	def protected void showError(String message) {
		showErrorDialog("Operation canceled", message)
	}
	
	def protected void stop(String message) throws StopHookExecution {
		if (message !== null) showErrorDialog("Operation canceled", message)
		throw new StopHookExecution
	}
	
	def protected IO addInputPortForReference(Object portReference) {
		return sib.addInput(portReference)
	}
	
	def protected IO addInputPortForReference(String name, Object portReference) {
		val port = sib.addInput(portReference)
		if (port instanceof Input) {
			port.name = name
		}
		return port
	}
	
	def protected List<IO> addInputPortsForReferences(Iterable<?> portReferences) {
		val inputs = new ArrayList
		for (ref : portReferences)
			inputs.add(addInputPortForReference(ref))
		return inputs
	}
	
	def protected Branch addBranch(String name) {
		if (branchX === null) {
			branchX = sib.calcBranchX
		}
		if (branchY === null) {
			branchY = sib.calcBranchY
		}
		val branches = sib.getSuccessors(Branch)
		val numBranches = branches.size
		if (numBranches < BRANCH_THRESHOLD) {
			for (branch : branches) {
				branch.x = branch.x - BRANCH_WIDTH / 2 - BRANCH_H_SPACE / 2
			}
		}
		currentBranch = model.newBranch(branchX, branchY)
		currentBranch.name = name
		currentBranch.resize(BRANCH_WIDTH, currentBranch.height)
		val connector = sib.newBranchConnector(currentBranch)
		if (numBranches >= BRANCH_THRESHOLD) {
			connector.addBendpoint(currentBranch.x + currentBranch.width / 2, sib.y + sib.height / 2)
		}
		if (numBranches + 1 < BRANCH_THRESHOLD) {
			branchX = branchX + BRANCH_WIDTH / 2 + BRANCH_H_SPACE / 2
		}
		else {
			branchX = branchX + BRANCH_WIDTH + BRANCH_H_SPACE
		}
		return currentBranch
	}
	
	/**
	 * <strong>Attention!</strong> Output Ports are added to the branch that
	 * has last been added. In particular, it makes no sense to add Output
	 * Ports prior to adding any branch.
	 * @see #addBranch(String)
	 */
	def protected OutputPort addOutputPortForReference(Object portReference) {
		return currentBranch.addOutput(portReference)
	}
	
	def protected OutputPort addOutputPortForReference(String name, Object portReference) {
		val port = currentBranch.addOutput(portReference)
		port.name = name
		return port
	}
	
	def protected List<OutputPort> addOutputPortsForReferences(Iterable<?> portReferences) {
		val ports = new ArrayList
		for (ref : portReferences) {
			ports.add(addOutputPortForReference(ref))
		}
		return ports
	}
	
	def protected void addPrimitiveOutputPort(String name, PrimitiveType type, boolean isList) {
		val outputPort = currentBranch.newPrimitiveOutputPort(PORT_X, branchOutputPortY)
		outputPort.dataType = type
		outputPort.isList = isList
		outputPort.name = name
		branchOutputPortY += PORT_SPACE
	}
	
//	def protected void addComplexOutputPort(String name, info.scce.dime.data.data.Type type, boolean isList) {
//		
//	}
	
	def protected void addJavaNativeOutputPort(String name, Type type, boolean isList) {
		val outputPort = currentBranch.newJavaNativeOutputPort(type, PORT_X, branchOutputPortY)
		outputPort.isList = isList
		outputPort.name = name
		branchOutputPortY += PORT_SPACE
	}
	
//	def protected void addGenericOutputPort(String name, info.scce.dime.process.process.TypeIO tio, boolean isList) {
//		
//	}
	
	def protected void addPrimitiveInputPort(String name, PrimitiveType type, boolean isList) {
		val inputPort = sib.newPrimitiveInputPort(PORT_X, sibInputPortY)
		inputPort.dataType = type
		inputPort.isList = isList
		inputPort.name = name
		sibInputPortY += PORT_SPACE
	}
	
//	def protected void addComplexInputPort(String name, info.scce.dime.data.data.Type type, boolean isList) {
//		
//	}
	
//	def protected void addInputGeneric(String name, info.scce.dime.data.data.Type upperBound, boolean isList) {
//		
//	}
	
	def protected void addJavaNativeInputPort(String name, Type type, boolean isList) {
		val inputPort = sib.newJavaNativeInputPort(type, PORT_X, sibInputPortY)
		inputPort.isList = isList
		inputPort.name = name
		sibInputPortY += PORT_SPACE
	}
	
//	def protected void addGenericInputPort(String name, info.scce.dime.process.process.TypeIO tio, boolean isList) {
//		
//	}
	
//	def protected TypeInput addTypeInput(String name, info.scce.dime.data.data.Type upperBound) {
//		
//	}
	
//	def protected void addPrimitiveOutputPort(String name, info.scce.dime.data.data.PrimitiveType type, boolean isList) {
//		
//	}
	
	def protected void addPrimitiveOutputPort(String name, info.scce.cinco.product.minidime.siblibrary.PrimitiveType type, boolean isList) {
		val processPrimitiveType = type.siblibraryPrimitiveTypeToProcessPrimitiveType
		addPrimitiveOutputPort(name, processPrimitiveType, isList)
	}
	
//	def protected void addPrimitiveOutputPort(String name, info.scce.dime.gui.gui.PrimitiveType type, boolean isList) {
//		
//	}
	
//	def protected void addPrimitiveOutputPort(String name, info.scce.dime.search.search.PrimitiveType type, boolean isList) {
//		
//	}
	
//	def protected void addPrimitiveInputPort(String name, info.scce.dime.data.data.PrimitiveType type, boolean isList) {
//		
//	}
	
	def protected void addPrimitiveInputPort(String name, info.scce.cinco.product.minidime.siblibrary.PrimitiveType type, boolean isList) {
		val processPrimitiveType = type.siblibraryPrimitiveTypeToProcessPrimitiveType
		addPrimitiveInputPort(name, processPrimitiveType, isList)
		
	}
	
//	def protected void addPrimitiveInputPort(String name, info.scce.dime.gui.gui.PrimitiveType type, boolean isList) {
//		
//	}
	
//	def protected void addPrimitiveInputPort(String name, info.scce.dime.search.search.PrimitiveType type, boolean isList) {
//		
//	}
	
	/**
	 * Returns the corresconding {@link PrimitiveType}
	 * from a {@link info.scce.cinco.product.minidime.siblibrary.PrimitiveType}.
	 * @author Fabian Storek
	 */
	def private PrimitiveType siblibraryPrimitiveTypeToProcessPrimitiveType(info.scce.cinco.product.minidime.siblibrary.PrimitiveType type) {
		return switch type {
			case BOOLEAN: PrimitiveType.BOOLEAN
			case INTEGER: PrimitiveType.INTEGER
			case REAL:    PrimitiveType.REAL
			case TEXT:    PrimitiveType.TEXT
			default:      throw new IllegalStateException('Reached else/default case in exhaustive if/switch. Please fix the code.')
		}
	}
	
	static class StopHookExecution extends RuntimeException {
		// Intentionally left blank
	}
	
}
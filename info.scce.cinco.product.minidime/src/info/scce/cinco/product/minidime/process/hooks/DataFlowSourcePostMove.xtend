package info.scce.cinco.product.minidime.process.hooks

import info.scce.cinco.product.minidime.process.DataFlowSource
import graphmodel.ModelElementContainer
import de.jabc.cinco.meta.core.ge.style.generator.runtime.api.CNode
import info.scce.cinco.product.minidime.process.helper.EdgeLayoutUtils.Location

import static extension info.scce.cinco.product.minidime.process.helper.EdgeLayoutUtils.*

/**
 * Copied from info.scce.dime.process.hooks.DataFlowSourcePostMove - Complete
 */
class DataFlowSourcePostMove extends ProcessNodePostMove<DataFlowSource> {
	
	override void postMove(DataFlowSource dfSource,
		                   ModelElementContainer sourceContainer,
		                   ModelElementContainer targetContainer,
		                   int x, int y, int deltaX, int deltaY) {
		super.postMove(dfSource, sourceContainer, targetContainer, x, y, deltaX, deltaY)		
		// Find out which edges have already been moved completely
		if (dfSource instanceof CNode) {
			val movables = (dfSource as CNode).movableEdges
			for (output : dfSource.outputs) {
				val absLoc = output.absoluteLocation
				val oldLoc = new Location(absLoc.x + 6 - deltaX, absLoc.y + 6 - deltaY)
				for (edge : output.outgoing) {
					if (!movables.contains(edge.connection)) {
						val points = edge.bendpoints
						for (i : 0 ..< points.size) {
							val point = points.get(i)
							if (getDistance(oldLoc, Location.from(point)) < 60) {
								edge.moveBendpoint(i, point.x + deltaX, point.y + deltaY)
							}
						}
					}
				}
			}
		}
	}
	
}

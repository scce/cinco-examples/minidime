package info.scce.cinco.product.minidime.process.actions

import graphmodel.Node
import info.scce.cinco.product.minidime.api.MiniDIMECustomAction
import info.scce.cinco.product.minidime.process.AtomicSIB
import info.scce.cinco.product.minidime.process.Branch
import info.scce.cinco.product.minidime.process.ContainsJavaNativeSIB
import info.scce.cinco.product.minidime.process.IterateJavaNativeSIB
import info.scce.cinco.product.minidime.process.JavaNativeInputPort
import info.scce.cinco.product.minidime.process.JavaNativeOutputPort
import info.scce.cinco.product.minidime.process.JavaNativeVariable
import info.scce.cinco.product.minidime.process.ProcessSIB

/**
 * Copied from info.scce.dime.process.actions.OpenModel - Complete
 */
class ProcessNodeOpenSubmodel extends MiniDIMECustomAction<Node> {
	
	override execute(Node it) {
		println('''Opening submodel for «eClass.name»''')
		switch it {
			ProcessSIB:            proMod
			AtomicSIB:             sib
			ContainsJavaNativeSIB: listType
			IterateJavaNativeSIB:  iteratedType
			JavaNativeInputPort:   dataType
			JavaNativeOutputPort:  dataType
			JavaNativeVariable:    dataType
			Branch: switch predecessor : predecessors.head {
				ProcessSIB: predecessor.proMod
				AtomicSIB:  predecessor.sib
			}
		}.openEditor
	}

	override hasDoneChanges() {
		false
	}
	
}

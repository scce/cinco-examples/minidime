package info.scce.cinco.product.minidime.process.actions

import info.scce.cinco.product.minidime.api.MiniDIMECustomAction
import info.scce.cinco.product.minidime.process.AbstractBranch
import info.scce.cinco.product.minidime.process.DataFlow
import info.scce.cinco.product.minidime.process.DataFlowSource
import info.scce.cinco.product.minidime.process.DataFlowTarget

import static info.scce.cinco.product.minidime.process.helper.NodeLayoutUtils.*
import info.scce.cinco.product.minidime.process.MiniDIMEProcess

/**
 * Copied from info.scce.dime.process.actions.ToggleDataFlowView - Complete
 */
class ProcessToggleDataFlowView extends MiniDIMECustomAction<MiniDIMEProcess> {
	
	override getName() {
		'Toggle DataFlow View'
	}
	
	override execute(MiniDIMEProcess processModel) {
		try {
			var df = !processModel.isDataFlowView
			processModel.dataFlowView = df
			for (it : processModel.allContainers) {
				switch it {
					DataFlowTarget: resize(width, getSIBHeight(if (df) inputs.size else 0))
					DataFlowSource:
					if (it instanceof AbstractBranch) {
						resize(width, getBranchHeight(if (df) outputs.size else 0))
					}
					else {
						resize(width, getSIBHeight(if (df) outputs.size else 0))
					}
				}
			}
			for (it : processModel.allNodes.filter(DataFlow)){
				dfViewWorkaround = !dfViewWorkaround
			}
			for (it : processModel.dataContexts) {
				dfViewWorkaround = !dfViewWorkaround
				for (variable : variables) {
					variable.dfViewWorkaround = !variable.dfViewWorkaround
				}
			}
		}
		catch (Exception e) {
			e.printStackTrace
		}
	}
	
}

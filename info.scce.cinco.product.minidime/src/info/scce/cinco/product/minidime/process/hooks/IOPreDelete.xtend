package info.scce.cinco.product.minidime.process.hooks

import info.scce.cinco.product.minidime.api.MiniDIMEPreDeleteHook
import info.scce.cinco.product.minidime.process.IO

import static extension info.scce.cinco.product.minidime.process.helper.SIBLayoutUtils.*

/**
 * Copied from info.scce.dime.process.hooks.CIOPreDelete - Complete
 */
class IOPreDelete extends MiniDIMEPreDeleteHook<IO> {
	
	override void preDelete(IO port) {
		val container = port.container
		container.resizeAndLayoutBeforeDelete(port)
	}
	
}

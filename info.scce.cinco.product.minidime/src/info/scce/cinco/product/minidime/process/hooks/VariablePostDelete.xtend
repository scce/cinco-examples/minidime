package info.scce.cinco.product.minidime.process.hooks

import de.jabc.cinco.meta.runtime.hook.CincoPostDeleteHook
import info.scce.cinco.product.minidime.process.DataContext
import info.scce.cinco.product.minidime.process.Variable

/**
 * Copied from info.scce.dime.process.hooks.Variable_PostDelete - Complete
 */
class VariablePostDelete<T extends Variable> extends CincoPostDeleteHook<T> {
	
	static final int OFFSET = 30
	
	override getPostDeleteFunction(T node) {
		if(node.container instanceof DataContext){
			val dataContext = node.container as DataContext
			return [dataContext.layout]
		}
	}
	
	def layout(DataContext dataContext) {
		dataContext.height = dataContext.maxHeight + OFFSET
	}
	
	def maxHeight(DataContext context) {
		var maxLowerBound = 50
		for (node : context.nodes) {
			if (node.y + node.height > maxLowerBound) {
				maxLowerBound = node.y + node.height 
			}
		}
		return maxLowerBound
	}
	
}

package info.scce.cinco.product.minidime.process.checks

import info.scce.cinco.product.minidime.checks.AbstractCheck
import info.scce.cinco.product.minidime.mcam.adapter.MiniDIMEProcessAdapter
import info.scce.cinco.product.minidime.mcam.adapter.MiniDIMEProcessId
import info.scce.cinco.product.minidime.process.Branch
import info.scce.cinco.product.minidime.process.EndSIB
import info.scce.cinco.product.minidime.process.IO
import info.scce.cinco.product.minidime.process.Input
import info.scce.cinco.product.minidime.process.InputStatic
import info.scce.cinco.product.minidime.process.JavaNativeInputPort
import info.scce.cinco.product.minidime.process.JavaNativeOutputPort
import info.scce.cinco.product.minidime.process.Output
import info.scce.cinco.product.minidime.process.PrimitiveInputPort
import info.scce.cinco.product.minidime.process.PrimitiveOutputPort
import info.scce.cinco.product.minidime.process.ProcessSIB
import info.scce.cinco.product.minidime.process.helper.ProcessExtension
import java.util.ArrayList

/**
 * Copied from info.scce.dime.process.checks.ProcessInterfaceCheck - Complete
 */
class ProcessInterfaceCheck extends AbstractCheck<MiniDIMEProcessId, MiniDIMEProcessAdapter> {
	
	extension ProcessExtension = new ProcessExtension
	
//	private ProcessUtils processUtils = new ProcessUtils();
	
	var MiniDIMEProcessAdapter adapter

	override void doExecute(MiniDIMEProcessAdapter adapter) {
		this.adapter = adapter
		for (id : adapter.entityIds) {
			val obj = id.element
			if (obj instanceof ProcessSIB) {
				checkInputs(id, obj)
				checkBranches(id, obj)
			}
		}
		processResults
	}
	
	override void init() {
		// Intentionally left blank
	}

	def private void checkInputs(MiniDIMEProcessId id, ProcessSIB sib) {
		val process = sib.proMod
		val inputs = new ArrayList(sib.inputs)
		for (output : process.startSIBs.head.outputs) {
			var count = 0
			var Input oInput = null
			for (input : inputs) {
				if (input.name == output.name) {
					oInput = input
					count++
				}
			}
			switch count {
				case count > 1:  addError(id, '''Input "«output.name»" exists «count» times''')
				case count <= 0: addError(id, '''Input "«output.name»" not found''')
				default: {
					checkTypeOfIO(adapter.getIdByString(oInput.id), oInput, output)
					inputs.remove(oInput)
				}
			}
		}
		inputs.forEach[addError(id, '''Reference for "«name»" not found''')]
	}
	
	def private void checkBranches(MiniDIMEProcessId sibId, ProcessSIB sib) {
		val process = sib.proMod
		val branches = new ArrayList(sib.getSuccessors(Branch))
		for (endSib : process.endSIBs) {
			var count = 0
			var Branch oBranch = null
			for (branch : branches) {
				if (branch.name == endSib.branchName) {
					oBranch = branch
					count++
				}
			}
			switch count {
				case count > 1:    addError(sibId, '''Branch "«endSib.branchName»" exists «count» times''')
				case count <= 0: addWarning(sibId, '''Branch "«endSib.branchName»" not found''')
				default: {
					checkOutputs(adapter.getIdByString(oBranch.id), oBranch, endSib)
					branches.remove(oBranch)
				}
			}
		}
		branches.forEach[addError(sibId, '''Reference for Branch "«name»" not found''')]
	}

	def private void checkOutputs(MiniDIMEProcessId branchId, Branch branch, EndSIB endSib) {
		val outputs = new ArrayList(branch.outputs)
		for (input : endSib.inputs) {
			var count = 0
			var Output oOutput = null
			for (output : outputs) {
				if (output.name == input.name) {
					oOutput = output
					count++
				}
			}
			switch count {
				case count > 1:  addError(branchId, '''Output "«input.name»" exists «count» times''')
				case count <= 0: addError(branchId, '''Output "«input.name»" not found''')
				default: {
					checkTypeOfIO(adapter.getIdByString(oOutput.id), oOutput, input)
					outputs.remove(oOutput)
				}
			}
		}
		outputs.forEach[addError(branchId, '''Reference for "«name»" not found''')]
	}

	def private void checkTypeOfIO(MiniDIMEProcessId thisId, IO thisIO, IO referencedIO) {
		val String thisType = switch thisIO {
			PrimitiveOutputPort,
			PrimitiveInputPort:   thisIO.primitiveTypeString
			JavaNativeOutputPort,
			JavaNativeInputPort:  thisIO.nativeTypeString
			InputStatic:          thisIO.staticTypeString
			default:              throw new RuntimeException('Could not find port type')
		}
		val String refType = switch referencedIO {
			PrimitiveOutputPort,
			PrimitiveInputPort:   referencedIO.primitiveTypeString
			JavaNativeOutputPort,
			JavaNativeInputPort:  referencedIO.nativeTypeString
			InputStatic:          referencedIO.staticTypeString
			default:              throw new RuntimeException('Could not find port type')
		}
		if (thisType != refType) {
			addError(thisId, '''Type mismatch, should be "«refType»"''')
		}
		if (thisIO.isListType != referencedIO.isListType) {
			addError(thisId, '''isList property mismatch, should be "«referencedIO.isListType»"''')
		}
	}
	
}

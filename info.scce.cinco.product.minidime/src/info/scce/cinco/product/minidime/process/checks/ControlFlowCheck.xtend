package info.scce.cinco.product.minidime.process.checks

import info.scce.cinco.product.minidime.checks.AbstractCheck
import info.scce.cinco.product.minidime.mcam.adapter.MiniDIMEProcessAdapter
import info.scce.cinco.product.minidime.mcam.adapter.MiniDIMEProcessId
import info.scce.cinco.product.minidime.process.AbstractBranch
import info.scce.cinco.product.minidime.process.BranchConnector
import info.scce.cinco.product.minidime.process.ControlFlow
import info.scce.cinco.product.minidime.process.DataFlowSource
import info.scce.cinco.product.minidime.process.DataFlowTarget

/**
 * Copied from info.scce.dime.process.checks.ControlFlowCheck - Complete
 */
class ControlFlowCheck extends AbstractCheck<MiniDIMEProcessId, MiniDIMEProcessAdapter> {
	
	override void doExecute(MiniDIMEProcessAdapter adapter) {
		for (id : adapter.entityIds) {
			val obj = id.element
			switch obj {
				AbstractBranch
					case obj.getIncoming(BranchConnector).empty:
						addError(id, 'no branch connector')
				DataFlowSource
					case obj.container instanceof Process
					&& obj.getOutgoing(ControlFlow).empty:
						addError(id, 'no outgoing control flow')
				DataFlowTarget
					case obj.container instanceof Process
					&& obj.getIncoming(ControlFlow).empty:
						addWarning(id, 'no incoming control flow')
			}
		}
		processResults
	}
	
	override void init() {
		// Intentionally left blank
	}
	
}

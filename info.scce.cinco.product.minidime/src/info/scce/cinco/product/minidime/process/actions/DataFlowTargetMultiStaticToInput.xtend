package info.scce.cinco.product.minidime.process.actions

import info.scce.cinco.product.minidime.api.MiniDIMECustomAction
import info.scce.cinco.product.minidime.process.DataFlowTarget
import info.scce.cinco.product.minidime.process.InputStatic

/**
 * Copied from info.scce.dime.process.actions.MultiStaticToInput - Complete
 * <p>
 * Context menu action, which converts all static ports to input ports
 * @author zweihoff
 */
class DataFlowTargetMultiStaticToInput<T extends DataFlowTarget> extends MiniDIMECustomAction<DataFlowTarget> {
	
	/** 
	 * Returns the name of the context menu entry
	 */
	override getName() {
		'Convert to input ports'
	}

	/** 
	 * Checks if the r GUI SIB is not already dispatched
	 */
	override canExecute(DataFlowTarget guiSib) {
		// At least one primitive input port without incoming edge
		!guiSib.inputStatics.empty
	}

	override void execute(DataFlowTarget guiSib) {
		// Replacing directly will result in concurrent modification problem (without exception though)
		val allToBeReplaced = guiSib.inputs.filter(InputStatic).map[id].toList
		val staticToPrimitiveConverter = new InputStaticToPrimitive
		for (toBeReplaced : allToBeReplaced) {
			guiSib.inputs.filter(InputStatic).filter[id == toBeReplaced].forEach[n | staticToPrimitiveConverter.execute(n)]
		}
	}
	
}

package info.scce.cinco.product.minidime.process.build

import info.scce.cinco.product.minidime.process.Branch
import info.scce.cinco.product.minidime.process.Input
import info.scce.cinco.product.minidime.process.Output
import info.scce.cinco.product.minidime.process.PutToContextSIB

/**
 * Copied from info.scce.dime.process.build.PutToContextSIBBuild - Complete
 */
class PutToContextSIBBuild extends SIBBuild<PutToContextSIB, Input, String, Input> {
	
	static def initialize(PutToContextSIB sib) {
		new PutToContextSIBBuild(sib).initialize
	}
	
	static def update(PutToContextSIB sib) {
		new PutToContextSIBBuild(sib).update
	}
	
	new(PutToContextSIB sib) {
		super(sib)
	}
	
	override getSIBReference(PutToContextSIB sib) {
		sib
	}
	
	override isSIBReferenceValid(PutToContextSIB sib) {
		!sib.inputs.isEmpty
	}
	
	override getSIBPortReferences(PutToContextSIB sib) {
		sib.inputs // Just keep the existing ones
	}
	
	override isSIBPortReference(Input port, Input portRef) {
		port == portRef
	}
	
	override getBranchReferences(PutToContextSIB sib) {
		newArrayList('success') // Single static branch
	}
	
	override isBranchReference(Branch branch, String staticBranchName) {
		branch.name == staticBranchName
	}
	
	override getBranchName(String staticBranchName) {
		staticBranchName
	}
	
	override getBranchPortReferences(String branchRef) {
		sib.inputs
	}
	
	override isBranchPortReference(Output branchPort, Input sibPort) {
		branchPort.name == sibPort.name
	}
	
}

package info.scce.cinco.product.minidime.process.hooks

import info.scce.cinco.product.minidime.process.ContainsPrimitiveSIB
import info.scce.cinco.product.minidime.process.PrimitiveType

/**
 * Copied from info.scce.dime.process.hooks.ContainsPrimitiveSIBHook - Complete
 */
class ContainsPrimitiveSIBPostCreate extends AbstractSIBPostCreate<ContainsPrimitiveSIB> {
	
	override boolean initialize(ContainsPrimitiveSIB sib) {
   		label = 'List Contains'
   		addPrimitiveInputPort('list', PrimitiveType.TEXT, true)
   		addPrimitiveInputPort('element', PrimitiveType.TEXT, false)
   		addBranch('yes')
   		addBranch('no')
   		return true
	}
	
}

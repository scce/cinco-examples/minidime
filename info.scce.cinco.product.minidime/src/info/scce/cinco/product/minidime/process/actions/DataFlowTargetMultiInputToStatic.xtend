package info.scce.cinco.product.minidime.process.actions

import info.scce.cinco.product.minidime.api.MiniDIMECustomAction
import info.scce.cinco.product.minidime.process.DataFlowTarget
import info.scce.cinco.product.minidime.process.PrimitiveInputPort

/** 
 * Copied from info.scce.dime.process.actions.MultiInputToStatic - Complete
 * <p>
 * Context menu action, which converts all primitive
 * input ports, which are not connected by data flow, to a static input
 * @author zweihoff
 */
class DataFlowTargetMultiInputToStatic<T extends DataFlowTarget> extends MiniDIMECustomAction<T> {
	
	/** 
	 * Returns the name of the context menu entry
	 */
	override getName() {
		'Convert to static inputs'
	}

	/** 
	 * Checks if the r GUI SIB is not already dispatched
	 */
	override canExecute(DataFlowTarget guiSib) {
		// At least one primitive input port without incoming edge
		!guiSib.inputs.filter(PrimitiveInputPort).filter[!isIsList].filter[incoming.empty].empty
	}

	override void execute(DataFlowTarget guiSib) {
		// Replacing directly will result in concurrent modification problem (without exception though)
		val allToBeReplaced = guiSib.inputs.filter(PrimitiveInputPort).filter[!isIsList].filter[incoming.empty].map[id].toList
		val primitiveToStaticConverter = new PrimitiveInputPortToStatic
		for (toBeReplaced : allToBeReplaced) {
			guiSib.inputs.filter(PrimitiveInputPort).filter[id == toBeReplaced].forEach[n | primitiveToStaticConverter.execute(n)]
		}
	}
	
}

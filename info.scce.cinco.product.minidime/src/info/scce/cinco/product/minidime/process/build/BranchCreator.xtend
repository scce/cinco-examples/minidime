package info.scce.cinco.product.minidime.process.build

import info.scce.cinco.product.minidime.process.Branch
import info.scce.cinco.product.minidime.process.SIB

import static info.scce.cinco.product.minidime.process.helper.LayoutConstants.*
import info.scce.cinco.product.minidime.process.MiniDIMEProcess

/**
 * Copied from info.scce.dime.process.build.BranchCreator - Complete
 */
interface BranchCreator {
	
	def Branch getNewBranch()
	
}

class UpdateBranchCreator implements BranchCreator {

	SIB sib
	MiniDIMEProcess model
	int branchOffsetX = 20
	int branchOffsetY = -20

	new(SIB sib) {
		this.sib = sib
		model = sib.rootElement
	}

	override getNewBranch() {
		val branch = model.newBranch(sib.x + sib.width + branchOffsetX,
		                             sib.y + branchOffsetY)
		sib.newBranchConnector(branch)
		branchOffsetY += branch.height
		return branch
	}

}

class InitializationBranchCreator implements BranchCreator {

	SIB sib
	MiniDIMEProcess model
	int branchX

	new(SIB sib) {
		this.sib = sib
		model = sib.rootElement
		branchX = calcBranchX
	}
	
	override getNewBranch() {
		val branches = sib.branchSuccessors
		val numBranches = branches.size 
		if (numBranches < BRANCH_THRESHOLD) {
			for (branch : branches) {
				branch.internalBranch.x = branch.x - BRANCH_WIDTH / 2 - BRANCH_H_SPACE / 2
			}
		}
		val branch = model.newBranch(branchX, calcBranchY)
		branch.resize(BRANCH_WIDTH, branch.height)
    	val connector = sib.newBranchConnector(branch)
    	if (numBranches >= BRANCH_THRESHOLD) {
    		connector.addBendpoint(branch.x + branch.width / 2,
			                       sib.y + sib.height / 2)
    	}
    	if (numBranches + 1 < BRANCH_THRESHOLD) {
			branchX += BRANCH_WIDTH / 2 + BRANCH_H_SPACE / 2
		}
		else {
	    	branchX += BRANCH_WIDTH + BRANCH_H_SPACE
	    }
		return branch
	}

	private def calcBranchX() {
		sib.x + SIB_WIDTH / 2 - BRANCH_WIDTH / 2
	}
	
	private def calcBranchY() {
		sib.y + sib.height + BRANCH_V_DISTANCE
	}
	
}

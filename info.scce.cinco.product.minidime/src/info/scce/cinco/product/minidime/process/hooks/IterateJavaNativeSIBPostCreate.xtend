package info.scce.cinco.product.minidime.process.hooks

import info.scce.cinco.product.minidime.process.IterateJavaNativeSIB
import info.scce.cinco.product.minidime.siblibrary.Type

/**
 * Copied from info.scce.dime.process.hooks.IterateJavaNativeSIBHook - Complete
 */
class IterateJavaNativeSIBPostCreate extends AbstractSIBPostCreate<IterateJavaNativeSIB> {
	
	override boolean initialize(IterateJavaNativeSIB sib) {
   		val iteratedType = sib.iteratedType as Type
   		label = '''Iterate «iteratedType.name»'''
   		addJavaNativeInputPort('list', iteratedType, true)
   		addBranch('next')
   		addJavaNativeOutputPort('element', iteratedType, false)
   		addBranch('exit')
   		return true
	}
	
}
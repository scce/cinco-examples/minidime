package info.scce.cinco.product.minidime.process.hooks

import de.jabc.cinco.meta.runtime.hook.CincoPostMoveHook
import graphmodel.ModelElementContainer
import info.scce.cinco.product.minidime.process.DataContext
import info.scce.cinco.product.minidime.process.Variable

/**
 * Copied from info.scce.dime.process.hooks.Variable_PostMove - Complete
 */
class VariablePostMove extends CincoPostMoveHook<Variable> {
	
	static final int OFFSET = 30
		
	override postMove(Variable node,
		              ModelElementContainer sourceContainer,
		              ModelElementContainer targetContainer,
		              int x, int y, int deltaX, int deltaY) {
		if (node.container instanceof DataContext){
			var dataContext = node.container as DataContext
			dataContext.setHeight(dataContext.maxHeight + OFFSET)
		}
	}
	
	def maxHeight(DataContext context) {
		var maxLowerBound = 50
		for(node : context.nodes){
			if(node.y + node.height > maxLowerBound) {
				maxLowerBound = node.y + node.height 
			}
		}
		return maxLowerBound
	}
	
}
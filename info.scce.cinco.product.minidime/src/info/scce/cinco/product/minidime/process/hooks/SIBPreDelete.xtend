package info.scce.cinco.product.minidime.process.hooks

import info.scce.cinco.product.minidime.api.MiniDIMEPreDeleteHook
import info.scce.cinco.product.minidime.process.SIB

/**
 * Copied from info.scce.dime.process.hooks.DeleteBranchesOnSIBDelete - Complete
 */
class SIBPreDelete extends MiniDIMEPreDeleteHook<SIB> {
	
	override void preDelete(SIB sib) {
		val branches = sib.abstractBranchSuccessors
		branches.forEach[delete]
	}
	
}

package info.scce.cinco.product.minidime.process.actions

import info.scce.cinco.product.minidime.process.AtomicSIB
import info.scce.cinco.product.minidime.process.build.AtomicSIBBuild

/**
 * Copied from info.scce.dime.process.actions.UpdateAtomicSIB - Complete
 */
class AtomicSIBUpdate extends SIBUpdate<AtomicSIB> {
	
	override update(AtomicSIB sib) {
		AtomicSIBBuild.update(sib)
	}
	
}

package info.scce.cinco.product.minidime.process.hooks

import info.scce.cinco.product.minidime.process.IteratePrimitiveSIB
import info.scce.cinco.product.minidime.process.PrimitiveType

/**
 * Copied from info.scce.dime.process.hooks.IteratePrimitiveSIBHook - Complete
 */
class IteratePrimitiveSIBPostCreate extends AbstractSIBPostCreate<IteratePrimitiveSIB> {
	
	override boolean initialize(IteratePrimitiveSIB sib) {
   		label = 'Iterate'
   		addPrimitiveInputPort('list', PrimitiveType.TEXT, true)
   		addBranch('next')
   		addPrimitiveOutputPort('element', PrimitiveType.TEXT, false)
   		addBranch('exit')
   		return true
	}
	
}

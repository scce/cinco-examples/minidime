package info.scce.cinco.product.minidime.process.hooks

import info.scce.cinco.product.minidime.process.ProcessSIB
import info.scce.cinco.product.minidime.process.build.ProcessSIBBuild

/**
 * Copied from info.scce.dime.process.hooks.ProcessSIBHook - Complete
 */
class ProcessSIBPostCreate extends AbstractSIBPostCreate<ProcessSIB> {
	
	override boolean initialize(ProcessSIB sib) {
		
		val primeModel = sib.proMod
		
		// Modified by Fabian Storek:
		// Set the model name of the referenced ProcessSIB, if it has no or the default model name.
		if (primeModel.getModelName.nullOrEmpty || primeModel.getModelName == 'unnamed model') {
			val fileName = primeModel.file.fullPath.lastSegment.replaceAll('\\.process$', '')
			if (!fileName.nullOrEmpty && fileName != 'unnamed model') {
				primeModel.modelName = fileName
				primeModel.save
			}
		}
		// End modified
		
		label = primeModel.getModelName	
		return ProcessSIBBuild.initialize(sib)
		
	}
	
}

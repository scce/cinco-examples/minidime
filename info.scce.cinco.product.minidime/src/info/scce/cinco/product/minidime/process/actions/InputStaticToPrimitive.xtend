package info.scce.cinco.product.minidime.process.actions

import info.scce.cinco.product.minidime.api.MiniDIMECustomAction
import info.scce.cinco.product.minidime.process.Input
import info.scce.cinco.product.minidime.process.InputStatic

import static extension info.scce.cinco.product.minidime.process.helper.PortUtils.*

/**
 * Copied from info.scce.dime.process.actions.StaticToPrimitivePort - Complete
 */
class InputStaticToPrimitive<T extends InputStatic> extends MiniDIMECustomAction<InputStatic> {
	
	override getName() {
		'Convert Static Value to Input'
	}

	override execute(InputStatic inputStatic) {
		try {
			val container = inputStatic.container
			// Re-add all ports to preserve their order
			container.getNodes(Input).forEach[oldPort |
				if (oldPort === inputStatic) {
					container.addInput(inputStatic)
				}
				else {
					oldPort.reconnectIncoming(oldPort.clone(oldPort.container))
				}
				oldPort.delete
			]
		}
		catch (Exception e) {
			e.printStackTrace
		}
	}
	
}

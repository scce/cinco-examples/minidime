package info.scce.cinco.product.minidime.process.hooks

import de.jabc.cinco.meta.core.ge.style.generator.runtime.api.CModelElement
import graphmodel.ModelElementContainer
import info.scce.cinco.product.minidime.process.ControlFlow
import info.scce.cinco.product.minidime.process.DataFlowSource
import info.scce.cinco.product.minidime.process.DataFlowTarget
import info.scce.cinco.product.minidime.process.helper.EdgeLayoutUtils.Location
import java.util.ArrayList
import java.util.List
import org.eclipse.graphiti.mm.pictograms.FreeFormConnection
import org.eclipse.swt.widgets.Display

import static extension info.scce.cinco.product.minidime.process.helper.EdgeLayoutUtils.*

/**
 * Copied from info.scce.dime.process.hooks.DataFlowTargetPostMove - Complete
 */
class DataFlowTargetPostMove extends ProcessNodePostMove<DataFlowTarget> {
	
	override void postMove(DataFlowTarget dfTarget,
		                   ModelElementContainer sourceContainer,
		                   ModelElementContainer targetContainer,
		                   int x, int y, int deltaX, int deltaY) {
		super.postMove(dfTarget, sourceContainer, targetContainer, x, y, deltaX, deltaY)
		dfTarget.updateLocation
		dfTarget.postProcessEdges(deltaX, deltaY)
	}
	
	def void updateLocation(DataFlowTarget dfTarget) {
		if (dfTarget.container instanceof DataFlowSource) {
			val dfSource = dfTarget.container as DataFlowSource
			var createEdge = true
			var redirect = false
			val outgoing = dfSource.getOutgoing(ControlFlow)
			val controlFlowEdge = outgoing.stream.filter[targetElement.equals(dfTarget)].findAny
			if (!outgoing.empty) {
				createEdge = false
				if (!controlFlowEdge.present) {
					redirect = showConfirmDialog('Conflict', 'Outgoing control flow already exists. Redirect?')
				}
			}
			val x = dfSource.x - (dfTarget.width - dfSource.width) / 2
			val y = dfSource.y + dfSource.height + 40
			dfTarget.moveTo(dfTarget.rootElement, x, y)
			if (createEdge) {
				dfSource.newControlFlow(dfTarget)
			}
			else if (redirect) {
				outgoing.get(0).delete
				dfSource.newControlFlow(dfTarget)
			}
		}
	}
	
	def postProcessEdges(DataFlowTarget dfTarget, int deltaX, int deltaY) {
		var List<FreeFormConnection> movables = new ArrayList
		if (Display.current !== null) {
			// Seems to be an UI-related change,
			// find out which edges have already been moved completely
			movables = (dfTarget as CModelElement).movableEdges
		}
		for (input : dfTarget.inputs) {
			val absLoc = input.absoluteLocation
			val oldLoc = new Location(absLoc.x + 6 - deltaX, absLoc.y + 6 - deltaY)
			for (edge : input.incoming) {
				if (!movables.contains(edge.connection)) {
					val points = edge.bendpoints
					for (i : 0 ..< points.size) {
						val point = points.get(i)
						if (getDistance(oldLoc, Location.from(point)) < 60) {
							edge.moveBendpoint(i, point.x + deltaX, point.y + deltaY)
						}
					}
				}
			}
		}
	}
	
}

package info.scce.cinco.product.minidime.process.hooks

import info.scce.cinco.product.minidime.process.PutToContextSIB

/**
 * Copied from info.scce.dime.process.hooks.PutToContextSIBHook - Complete
 */
class PutToContextSIBPostCreate extends AbstractSIBPostCreate<PutToContextSIB> {
	
	override boolean initialize(PutToContextSIB sib) {
		label = 'PutToContext'
    	addBranch('success')
		return true
	}
	
}

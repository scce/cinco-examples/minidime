package info.scce.cinco.product.minidime.process.helper

import de.jabc.cinco.meta.core.ge.style.generator.runtime.api.CEdge
import de.jabc.cinco.meta.core.ge.style.generator.runtime.api.CModelElement
import de.jabc.cinco.meta.runtime.xapi.WorkbenchExtension
import graphmodel.Edge
import graphmodel.ModelElement
import graphmodel.Node
import java.util.ArrayList
import java.util.Collection
import java.util.List
import java.util.stream.Collectors
import org.eclipse.graphiti.features.IFeature
import org.eclipse.graphiti.features.IFeatureProvider
import org.eclipse.graphiti.features.context.IContext
import org.eclipse.graphiti.features.context.impl.AddBendpointContext
import org.eclipse.graphiti.features.context.impl.MoveBendpointContext
import org.eclipse.graphiti.features.context.impl.RemoveBendpointContext
import org.eclipse.graphiti.mm.algorithms.styles.Point
import org.eclipse.graphiti.mm.pictograms.Anchor
import org.eclipse.graphiti.mm.pictograms.AnchorContainer
import org.eclipse.graphiti.mm.pictograms.ContainerShape
import org.eclipse.graphiti.mm.pictograms.FreeFormConnection
import org.eclipse.graphiti.mm.pictograms.PictogramElement
import org.eclipse.graphiti.ui.services.GraphitiUi

import static java.lang.Math.min
import static java.lang.Math.pow
import static java.lang.Math.sqrt

/**
 * Copied from info.scce.dime.process.helper.EdgeLayoutUtils - Complete
 */
class EdgeLayoutUtils {
	
	static extension WorkbenchExtension = new WorkbenchExtension
	
	public static int EDGE_OFFSET_LEFT = 15
	
	def static List<Location> getManhattanPoints(Node source, Node target) {
		val sLoc = source.absoluteLocation
		val tLoc = target.absoluteLocation
		val x = min(sLoc.x - EDGE_OFFSET_LEFT, tLoc.x - EDGE_OFFSET_LEFT)
		val bps = #[
			new Location(x, sLoc.y + source.height / 2),
			new Location(x, tLoc.y + target.height / 2)
		]
		return bps
	}
	
	def static Location getAbsoluteLocation(Node cnode) {
		val pos = new Location(cnode.x, cnode.y)
		var ccontainer = cnode.container
		while (ccontainer instanceof Node) {
			pos.x += ccontainer.x
			pos.y += ccontainer.y
			ccontainer = ccontainer.container
		}
		return pos
	}
	
	def static FreeFormConnection getConnection(CEdge cedge) {
		val pe = cedge.pictogramElement
		if (pe instanceof FreeFormConnection) {
			return pe
		}
		return null
	}
	
	def static FreeFormConnection getConnection(Edge edge) {
		val pe = edge.pictogramElement
		if (pe instanceof FreeFormConnection) {
			return pe
		}
		return null
	}
	
	def static double getDistance(Location l1, Location l2) {
		return sqrt(pow((l2.x - l1.x), 2) + pow((l2.y - l1.y), 2))
	}
	
	def static PictogramElement[] getSelectedPictograms() {
		return activeDiagramEditor.selectedPictogramElements
	}
	
	def static List<FreeFormConnection> getMovableEdges(CModelElement element) {
		return (element.pictogramElement as PictogramElement).movableEdges
	}
	
	def static List<FreeFormConnection> getMovableEdges(PictogramElement movingPe) {
		val list = new ArrayList
		val anchors = getAnchors(movingPe)
		val selection = selectedPictograms
		if (selection !== null) {
			for (pe : selection) {
				for (toAnchor : getAnchors(pe)) {
					for (con : toAnchor.incomingConnections) {
						if (con instanceof FreeFormConnection && anchors.contains(con.start)) {
							list.add(con as FreeFormConnection)
						}
					}
					for (con : toAnchor.outgoingConnections) {
						if (con instanceof FreeFormConnection && anchors.contains(con.end)) {
							list.add(con as FreeFormConnection);
						}
					}
				}
			}
		}
		return list
	}
	
	def static List<Anchor> getAnchors(PictogramElement pe) {
		val anchors = new ArrayList
		if (pe instanceof AnchorContainer) {
			anchors.addAll(pe.anchors)
		}
		if (pe instanceof ContainerShape) {
			val container = pe as ContainerShape
			val children = container.children
			for (child : children) {
				if (child instanceof ContainerShape) {
					anchors.addAll(getAnchors(child))
				}
				else {
					anchors.addAll(child.anchors)
				}
			}
		}
		return anchors
	}
	
	def static List<Point> getBendpoints(Edge edge) {
		val connection = edge.connection
		if (connection !== null) {
			return connection.bendpoints
		}
		return new ArrayList
	}
	
	def static List<Point> getBendpoints(CEdge cedge) {
		val connection = cedge.connection
		if (connection !== null) {
			return connection.bendpoints
		}
		return new ArrayList
	}
	
	def static void addBendpoint(CEdge cEdge, int x, int y) {
		addBendpoint(cEdge as Edge, x, y)
	}
	
	def static void addBendpoint(Edge edge, int x, int y) {
		val index = getBendpoints(edge).size
		val ctx = new AddBendpointContext(getConnection(edge), x, y, index)
		val fp = getFeatureProvider(edge)
		val ftr = fp.getAddBendpointFeature(ctx)
		executeFeature(ftr, ctx)
	}
	
	def static void moveBendpoint(CEdge cEdge, int bendpointIndex, int x, int y) {
		moveBendpoint(cEdge as Edge, bendpointIndex, x, y)
	}
	
	def static void moveBendpoint(Edge edge, int bendpointIndex, int x, int y) {
		val ctx = new MoveBendpointContext(null)
		ctx.connection = getConnection(edge)
		ctx.bendpointIndex = bendpointIndex
		ctx.setLocation(x, y)
		val fp = edge.featureProvider
		val ftr = fp.getMoveBendpointFeature(ctx)
		executeFeature(ftr, ctx)
	}
	
	def static void removeBendpoint(CEdge cEdge, int bendpointIndex) {
		removeBendpoint(cEdge as Edge, bendpointIndex)
	}
	
	def static void removeBendpoint(Edge edge, int bendpointIndex) {
		val ctx = new RemoveBendpointContext(edge.connection, null)
		ctx.bendpointIndex = bendpointIndex
		val fp = edge.featureProvider
		val ftr = fp.getRemoveBendpointFeature(ctx)
		executeFeature(ftr, ctx)
	}
	
	def static IFeatureProvider getFeatureProvider(ModelElement elm) {
		val diagram = elm.diagram
		return GraphitiUi.extensionManager.createFeatureProvider(diagram)
	}
	
	def static Object executeFeature(IFeature ftr, IContext ctx) {
		val db = ftr.featureProvider.diagramTypeProvider.diagramBehavior
		return db.executeFeature(ftr, ctx)
	}
	
	static class Location {
		
		public int x = 0
		public int y = 0
		
		def static Location from(Point point) {
			return new Location(point.x, point.y)
		}
		
		def static List<Location> from(Collection<Point> points) {
			if (points === null) {
				return new ArrayList
			}
			return points.stream.map[from].collect(Collectors.toList)
		}
		
		new(int x, int y) {
			this.x = x
			this.y = y
		}
	}
	
}
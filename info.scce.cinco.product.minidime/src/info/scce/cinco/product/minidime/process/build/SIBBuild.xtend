package info.scce.cinco.product.minidime.process.build

import de.jabc.cinco.meta.runtime.xapi.ResourceExtension
import de.jabc.cinco.meta.runtime.xapi.WorkbenchExtension
import graphmodel.ModelElement
import info.scce.cinco.product.minidime.mcam.cli.MiniDIMEProcessExecution
import info.scce.cinco.product.minidime.process.Branch
import info.scce.cinco.product.minidime.process.DataFlow
import info.scce.cinco.product.minidime.process.IO
import info.scce.cinco.product.minidime.process.Input
import info.scce.cinco.product.minidime.process.InputStatic
import info.scce.cinco.product.minidime.process.Output
import info.scce.cinco.product.minidime.process.SIB
import info.scce.cinco.product.minidime.process.helper.PortUtils.Sync
import info.scce.cinco.product.minidime.siblibrary.Port
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.jface.dialogs.MessageDialog

import static extension info.scce.cinco.product.minidime.process.helper.PortUtils.*
import info.scce.cinco.product.minidime.process.MiniDIMEProcess

/**
 * Copied from info.scce.dime.process.build.PrimeSIBBuild - Complete
 */
abstract class SIBBuild<T extends SIB, SIBPortRef, BranchRef, BranchPortRef> {
	
	protected extension WorkbenchExtension = new WorkbenchExtension
	protected extension ResourceExtension = new ResourceExtension
	
	protected T sib
	protected SIB cSib
	protected MiniDIMEProcess cModel
	protected BranchCreator branchCreator
	
	new(T sib) {
		this.sib = sib
	}
	
	def EObject getSIBReference(T sib)
	def boolean isSIBReferenceValid(T sib)
	def Iterable<SIBPortRef> getSIBPortReferences(T sib)
	def boolean isSIBPortReference(Input port, SIBPortRef portRef)
	def Iterable<BranchRef> getBranchReferences(T sib)
	def boolean isBranchReference(Branch branch, BranchRef branchRef)
	def String getBranchName(BranchRef branchRef)
	def Iterable<BranchPortRef> getBranchPortReferences(BranchRef branchRef)
	def boolean isBranchPortReference(Output branchPort, BranchPortRef portRef)
	
	def initialize() {
		if(initializeInternal) {
			initializeSIBInternal
			initializeBranchesInternal		
			return true	
		}
		return false
	}
	
	def initializeSIB() {
		if(initializeInternal) {
			initializeSIBInternal		
			return true	
		}
		return false
	}
	
	private def initializeInternal() {
		cModel = sib.rootElement
		cSib = sib
		branchCreator = new InitializationBranchCreator(cSib)
		if (sib.SIBReference === null) {
			letUserKnow('Reference is null', 'Initialization failed. Reference not found!')
			cSib.delete
			return false
		}
		if (!sib.isSIBReferenceValid) {
			val ok = letUserConfirm('Referenced model contains errors', 'Referenced model contains errors! Continue anyway?')
			if (!ok) {
				cSib.delete
				return false
			}
		}
		return true
	}
	
	def update() {
		cModel = sib.rootElement
		cSib = sib
		branchCreator = new UpdateBranchCreator(cSib)
		if (sib.SIBReference === null) {
			letUserKnow('Reference is null', 'Update failed. Reference not found!')
			return
		}
		if (!sib.isSIBReferenceValid) {
			val ok = letUserConfirm('Referenced model contains errors', 'Referenced model contains errors! Continue anyway?')
			if (!ok) {
				return
			}
		}
		updateSIB
		updateBranches
	}
	
	private def initializeSIBInternal() {
		val sync = new Sync<Input,SIBPortRef> => [
			add = [ ref | addInput(ref) ]
		]
		sync.apply(sib.inputs, sib.SIBPortReferences)
	}
	
	def updateSIB() {
		val sync = new Sync<Input,SIBPortRef> => [
			equals = [ input, ref | input.isSIBPortReference(ref) ]
			add    = [        ref | addInput(ref) ]
			update = [ input, ref | input.update(ref) ]
			delete = [ input      | input.delete ]
		]
		sync.apply(sib.inputs, sib.SIBPortReferences)
	}
	
	def addInput(SIBPortRef ref) {
		cSib.addInput(ref)?.postProcessNewSIBPort(ref)
	}
	
	def dispatch void update(Input sibPort, Object ref) {
		val cInput = sibPort
		val cInputNew = cSib.addInput(ref)
		for (cEdge : cInput.incoming.toNewList) {
			cEdge.reconnectTarget(cInputNew)
		}
		cInput.delete
		EcoreUtil.setID(cInputNew, sibPort.id)
	}
	
	def dispatch void update(InputStatic input, Object ref) {
		val staticInputType = switch it:ref {
			ModelElement : primitiveType?.toStaticInputType
			Port: primitiveType?.toStaticInputType
		}
		if (staticInputType?.isInstance(input)) {
			val inputClone = cSib.cloneStaticPort(input)
			EcoreUtil.setID(inputClone, input.id)
		} else {
			cSib.addInput(ref)
		}
		input.delete
	}
	
	def void postProcessNewSIBPort(IO port, Object ref) {
		// Default: do nothing
	}
	
	package def initializeBranchesInternal() {
		val sync = new Sync<Branch,BranchRef> => [
			add = [ ref | addAndUpdateBranch(ref) ]
		]
		sync.apply(sib.getSuccessors(Branch), sib.branchReferences)
	}
	
	def updateBranches() {
		val sync = new Sync<Branch,BranchRef> => [
			equals = [ branch,ref | branch.isBranchReference(ref) ]
			add    = [        ref | addAndUpdateBranch(ref) ]
			update = [ branch,ref | branch.update(ref) ]
			delete = [ branch     | branch.delete ]
		]
		sync.apply(sib.getSuccessors(Branch), sib.branchReferences)
	}
	
	def addAndUpdateBranch(BranchRef ref) {
		addBranch(ref.branchName).update(ref)
	}
	
	def addBranch(String branchName) {
		val newBranch = branchCreator.newBranch
		newBranch.name = branchName
		return newBranch as Branch
	}
	
	def update(Branch branch, BranchRef branchRef) {
		val sync = new Sync<Output,BranchPortRef> => [
			equals = [ port,ref | port.isBranchPortReference(ref) ]
			add    = [      ref | branch.addOutputPort(ref) ]
			update = [ port,ref | port.update(ref, branch) ]
			delete = [ port     | port.delete ]
		]
		sync.apply(branch.outputs, branchRef.branchPortReferences)
	}
	
	def addOutputPort(Branch branch, BranchPortRef ref) {
		branch.addOutput(ref)
	}
	
	def update(Output branchPort, Object portRef, Branch branch) {
		val cOutput = branchPort
		val cOutputNew = branch.addOutput(portRef)
		for (cEdge : cOutput.outgoing.toNewList) {
			reconnect(cEdge as DataFlow, cOutputNew, null)
		}
		cOutput.delete
		EcoreUtil.setID(cOutputNew, branchPort.id)
	}
	
	def delete(Input input) {
		input?.delete
	}
	
	def delete(Branch branch) {
		branch?.delete
	}
	
	def delete(Output output) {
		output?.delete
	}
	
	def boolean hasErrors(MiniDIMEProcess model) {
		val file = model.eResource.file.rawLocation.toFile
		val fe = new MiniDIMEProcessExecution
		val adapter = fe.initApiAdapter(file)
		val check = fe.executeCheckPhase(adapter)
		if (check.hasErrors || check.hasWarnings) {
			println(check)
		}
		return check.hasErrors
	}
	
	def void letUserKnow(String title, String msg) {
		MessageDialog.openInformation(null, title, msg)
	}
	
	def boolean letUserConfirm(String title, String msg) {
		return MessageDialog.openConfirm(null, title, msg)
	}
	
	def <X> toNewList(Iterable<X> list) {
		val newList = newArrayList
		if (list !== null) {
			newList.addAll(list)
		}
		return newList
	}
	
	def getStartSIB(MiniDIMEProcess process) {
		if (process.startSIBs.isEmpty) {
			showError('Referenced process model does not contain a Start SIB')
			return null
		}
		if (process.startSIBs.size > 1) {
			showError('Referenced process model contains multiple Start SIBs')
		}
		return process.startSIBs.head
	}
	
	def showError(String message) {
		showErrorDialog('Operation canceled', message)
	}
	
}
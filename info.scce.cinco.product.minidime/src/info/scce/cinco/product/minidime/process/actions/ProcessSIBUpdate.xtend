package info.scce.cinco.product.minidime.process.actions

import info.scce.cinco.product.minidime.process.ProcessSIB
import info.scce.cinco.product.minidime.process.build.ProcessSIBBuild

/**
 * Copied from info.scce.dime.process.actions.UpdateProcessSIB - Complete
 */
class ProcessSIBUpdate<T extends ProcessSIB> extends SIBUpdate<ProcessSIB> {
	
	override update(ProcessSIB sib) {
		ProcessSIBBuild.update(sib)
	}
	
}
